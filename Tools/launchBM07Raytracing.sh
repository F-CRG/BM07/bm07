python3.8 BM07Raytracing.py --outfile ref.png

python3.8 BM07Raytracing.py --beam_alt -4.0 --outfile BEAM_ALT_-4_0mm.png
python3.8 BM07Raytracing.py --beam_alt -3.0 --outfile BEAM_ALT_-3_0mm.png
python3.8 BM07Raytracing.py --beam_alt -2.0 --outfile BEAM_ALT_-2_0mm.png
python3.8 BM07Raytracing.py --beam_alt -1.0 --outfile BEAM_ALT_-1_0mm.png
python3.8 BM07Raytracing.py --beam_alt  1.0 --outfile BEAM_ALT_+1_0mm.png
python3.8 BM07Raytracing.py --beam_alt  2.0 --outfile BEAM_ALT_+2_0mm.png
python3.8 BM07Raytracing.py --beam_alt  3.0 --outfile BEAM_ALT_+3_0mm.png
python3.8 BM07Raytracing.py --beam_alt  4.0 --outfile BEAM_ALT_+4_0mm.png

python3.8 BM07Raytracing.py --beam_ang -0.000150 --outfile BEAM_ANG_-150microrad.png
python3.8 BM07Raytracing.py --beam_ang -0.000100 --outfile BEAM_ANG_-100microrad.png
python3.8 BM07Raytracing.py --beam_ang -0.000050 --outfile BEAM_ANG_-50microrad.png
python3.8 BM07Raytracing.py --beam_ang -0.000025 --outfile BEAM_ANG_-25microrad.png
python3.8 BM07Raytracing.py --beam_ang  0.000025 --outfile BEAM_ANG_+25microrad.png
python3.8 BM07Raytracing.py --beam_ang  0.000050 --outfile BEAM_ANG_+50microrad.png
python3.8 BM07Raytracing.py --beam_ang  0.000100 --outfile BEAM_ANG_+100microrad.png
python3.8 BM07Raytracing.py --beam_ang  0.000150 --outfile BEAM_ANG_+150microrad.png

python3.8 BM07Raytracing.py --SL0 -2.5 --outfile SL0_-2_5mm.png
python3.8 BM07Raytracing.py --SL0 -2.0 --outfile SL0_-2_0mm.png
python3.8 BM07Raytracing.py --SL0 -1.0 --outfile SL0_-1_0mm.png
python3.8 BM07Raytracing.py --SL0 -0.5 --outfile SL0_-0_5mm.png
python3.8 BM07Raytracing.py --SL0  0.5 --outfile SL0_+0_5mm.png
python3.8 BM07Raytracing.py --SL0  1.0 --outfile SL0_+1_0mm.png
python3.8 BM07Raytracing.py --SL0  2.0 --outfile SL0_+2_0mm.png
python3.8 BM07Raytracing.py --SL0  2.5 --outfile SL0_+2_5mm.png

python3.8 BM07Raytracing.py --M1 -2.5 --outfile M1_-2_5mm.png
python3.8 BM07Raytracing.py --M1 -2.0 --outfile M1_-2_0mm.png
python3.8 BM07Raytracing.py --M1 -1.5 --outfile M1_-1_5mm.png
python3.8 BM07Raytracing.py --M1 -1.0 --outfile M1_-1_0mm.png
python3.8 BM07Raytracing.py --M1  1.0 --outfile M1_+1_0mm.png
python3.8 BM07Raytracing.py --M1  1.5 --outfile M1_+1_5mm.png
python3.8 BM07Raytracing.py --M1  2.0 --outfile M1_+2_0mm.png
python3.8 BM07Raytracing.py --M1  2.5 --outfile M1_+2_5mm.png

python3.8 BM07Raytracing.py --MOVEH -4.0 --outfile MOVEH_-4_0mm.png
python3.8 BM07Raytracing.py --MOVEH -3.5 --outfile MOVEH_-3_0mm.png
python3.8 BM07Raytracing.py --MOVEH -1.5 --outfile MOVEH_-1_5mm.png
python3.8 BM07Raytracing.py --MOVEH  1.5 --outfile MOVEH_+1_5mm.png
python3.8 BM07Raytracing.py --MOVEH  3.0 --outfile MOVEH_+3_0mm.png
python3.8 BM07Raytracing.py --MOVEH  4.0 --outfile MOVEH_+4_0mm.png
