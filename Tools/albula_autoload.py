import sys
sys.path.insert(0,"/opt/pxsoft/albula/vdefault/ubuntu20.04/python/")
import dectris.albula
from tango import DeviceProxy
from time import sleep
from os.path import exists



tango_test = DeviceProxy("//bm07server2:20000/bm07/limaccds/pilatus-6m")

m = dectris.albula.openMainFrame()
s = m.openSubFrame()



old_image_path=""


while True:
    saving_directory = tango_test.read_attribute("saving_directory").value
    saving_prefix = tango_test.read_attribute("saving_prefix").value
    saving_suffix = tango_test.read_attribute("saving_suffix").value
    saving_index_format = tango_test.read_attribute("saving_index_format").value
    saving_next_number = tango_test.read_attribute("saving_next_number").value

    if saving_next_number>1:
        #print("wait for new image")
        pathformat = "/%s/%s%s%s"%(saving_directory,saving_prefix,saving_index_format,saving_suffix)
        image_path = pathformat%(saving_next_number-1)
        if old_image_path != image_path:
            old_image_path = image_path
            for t in range(100):
                print("wait for", image_path)
                if exists(image_path):
                    s.loadFile(image_path)
                    break
                sleep(0.2)
    try:
        cursubframeid = m.sendCommand("subFrames")
    except:
        print("Albula seam closed")
        exit()
    sleep(1)
