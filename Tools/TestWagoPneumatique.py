from tkinter import *
from functools import partial
from pyModbusTCP.client import ModbusClient
import time 

def set_text(e, text):
    e["text"]=text

class wago_test_pneumatique(Frame):

    def __init__(self):
        super().__init__()

        self.initUI()
        self.connected=False
        self.c=None
        self.poll()

    def poll(self):
        self.read_from_wago()
        self.master.update_idletasks()
        self.master.after_idle(self.poll)

    def initUI(self):

        self.master.title("Test WAGO Pneumatique")

        frame1 = Frame(self)
        frame1.pack(fill=X)

        lblIP = Label(frame1, text="IP ou Hostname")
        lblIP.pack(side=LEFT, padx=5, pady=5)
        self.connectbtn = Button(frame1,text="Connecter", command=self.connection)
        self.connectbtn.pack(side=RIGHT, padx=5)
        self.entryPORT = Entry(frame1)
        self.entryPORT.insert(END,"502")
        self.entryPORT.pack(padx=5, side=RIGHT)
        lblPORT = Label(frame1, text="Port")
        lblPORT.pack(side=RIGHT, padx=5, pady=5)
        self.entryIP = Entry(frame1)
        self.entryIP.insert(END,"160.103.127.27")
        self.entryIP.pack(fill=X, padx=5, expand=True)
        
        
        self.frame2 = Frame(self, relief=RAISED, borderwidth=1)
        for i in range(0,8+1+4):
            if i==8:
                weight=5
            else:
                weight=1
            self.frame2.columnconfigure(i, pad=0, weight=weight)
        for i in range(0,1+2+2):
            self.frame2.rowconfigure(i, pad=3)
        
        self.stateOn=[]
        self.stateOff=[]
        for i in range(0,8+1+4):
            if i!=8:
                lblChan = Label(self.frame2, text=str(i))
                lblChan.grid(row=0,column=i, sticky="n")
                self.stateOn.append(Canvas(self.frame2, bg="black", height=10, width=10))
                self.stateOn[-1].grid(row=1,column=i, sticky="n")
                self.stateOff.append(Canvas(self.frame2, bg="black", height=10, width=10))
                self.stateOff[-1].grid(row=2,column=i, sticky="n")
                on_with_arg = partial(self.enable, i)
                btnOn = Button(self.frame2, text="ON", width=5, command=on_with_arg)
                btnOn.grid(row=3,column=i, sticky="n")
                off_with_arg = partial(self.disable, i)
                btnOff = Button(self.frame2, text="OFF", width=5, command=off_with_arg)
                btnOff.grid(row=4,column=i,sticky="n")
                
        self.frame2.pack(fill=X)
        for child in self.frame2.winfo_children():
            child.configure(state='disable')
        
        frame = Frame(self)
        frame.pack(fill=BOTH, expand=True)

        self.pack(fill=BOTH, expand=True)
        
        self.status = Label(self)
        self.status.pack(side=LEFT, padx=5, pady=5, fill=X, expand=True)
        
    def display_enabled(self,channel):
        self.stateOn[channel].config(bg="green")
        self.stateOff[channel].config(bg="black")
        
    def display_disabled(self,channel):
        self.stateOn[channel].config(bg="black")
        self.stateOff[channel].config(bg="red")


    def enable(self,channel):
        try:
            if(self.connected):
                self.c.write_single_coil(channel,1)
        except Exception as e:
            set_text(self.status, e)

    def disable(self,channel):
        try:
            if(self.connected):
                self.c.write_single_coil(channel,0)
        except Exception as e:
            set_text(self.status, e)
            
    def read_from_wago(self):
        try:
            if self.connected:
                values=self.c.read_coils(0,12)
                for i in range(0,len(values)):
                    if values[i]:
                        self.display_enabled(i)
                    else:
                        self.display_disabled(i)
        except Exception as e:
            set_text(self.status, e)


    def connection(self):
        IP=self.entryIP.get()
        PORT=self.entryPORT.get()
        
        if not self.connected:
            try:
                self.c = ModbusClient(IP,int(PORT), timeout=2)
                if not self.c.is_open():
                    if not self.c.open():
                        set_text(self.status, "unable to connect to "+IP+":"+PORT)
                    else:
                        self.connected=True
                else:
                    self.connected=True
            except Exception as e:
                set_text(self.status, e)
                
            if  self.connected:
                set_text(self.status, "Connection OK")
                self.connectbtn['text']="Deconnecter"
                self.entryIP.config(state='disabled')
                self.entryPORT.config(state='disabled')
                for child in self.frame2.winfo_children():
                    child.configure(state='normal')
                
        else:
            self.c.close()
            set_text(self.status, "Deconnection")
            self.connectbtn['text']="Connecter"
            self.entryIP.config(state='normal')
            self.entryPORT.config(state='normal')
            self.connected=False
            for child in self.frame2.winfo_children():
                child.configure(state='disabled')
            for child in self.stateOn:
                child.config(bg="black")
            for child in self.stateOff:
                child.config(bg="black")
                
                
#read_coils
                
def main():

    root = Tk()
    root.geometry("800x170")
    app = wago_test_pneumatique()
    root.mainloop()


if __name__ == '__main__':
    main()