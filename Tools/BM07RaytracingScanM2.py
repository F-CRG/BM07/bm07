from multiprocessing import Pool
from BM07RaytracingClass import BM07Raytracing
import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--SL0', metavar="SL0", type=float, help="shift in mm", default=0.0)
parser.add_argument('--M1', metavar="M1", type=float, help="shift in mm", default=0.0)
parser.add_argument('--MOVEH', metavar="MOVEH", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_alt', metavar="beam_alt", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_ang', metavar="beam_ang", type=float, help="shift in rad", default=0.0)
parser.add_argument('--outfile', metavar="outfile", type=str, help="graph png output name", default="")
args = parser.parse_args()

#fluo3 vs utz bruité
#refUtz = np.array([30.0, 30.040816, 30.081633, 30.122449, 30.163265, 30.204082, 30.244898, 30.285714, 30.326531, 30.367347, 30.408163, 30.44898, 30.489796, 30.530612, 30.571429, 30.612245, 30.653061, 30.693878, 30.734694, 30.77551, 30.816327, 30.857143, 30.897959, 30.938776, 30.979592, 31.020408, 31.061224, 31.102041, 31.142857, 31.183673, 31.22449, 31.265306, 31.306122, 31.346939, 31.387755, 31.428571, 31.469388, 31.510204, 31.55102, 31.591837, 31.632653, 31.673469, 31.714286, 31.755102, 31.795918, 31.836735, 31.877551, 31.918367, 31.959184, 32.0])
#refPix = np.array([265.9497445760883, 252.7151014137031, 281.5387006654282, 328.1874601055426, 283.849631224327, 287.9961803399088, 298.90494151339675, 310.7059742035974, 318.8192082365739, 321.18235654016286, 326.31302538952224, 367.6576129108051, 350.0191479284162, 380.4484284647399, 374.6849834106572, 370.0351801243595, 384.0074151065695, 370.37798359887114, 386.42635529264527, 393.58307370332756, 397.1750500907829, 408.49706904495736, 423.5070653630889, 403.90016599328754, 431.9746929998574, 433.9433730240846, 447.00932935242207, 454.8471275648124, 438.57584663656615, 453.27204814174956, 457.1882849149393, 489.5539210852073, 456.52767825588217, 486.5456840652252, 489.1349086567407, 481.96236872446286, 508.31119267310913, 505.75991074960393, 523.3373154248907, 494.03394220619606, 535.770137722929, 526.2471140318042, 532.667738688419, 557.694319572784, 560.6595168555616, 549.1600884975556, 555.2860483344822, 556.260644613071, 564.3195254209501, 588.145170672925])

#fluo3 vs utz
refUtz = np.array([29.81578947368421, 29.973684210526315, 30.13157894736842, 30.289473684210527, 30.44736842105263, 30.605263157894736, 30.763157894736842, 30.92105263157895, 31.07894736842105, 31.236842105263158, 31.394736842105264, 31.55263157894737, 31.710526315789473, 31.86842105263158, 32.026315789473685, 32.18421052631579])
refPix=np.array([225.07856885862284, 257.92771096909706, 285.6368967018754, 314.2729880074261, 345.5303062996342, 375.06255890323615, 402.4168982485395, 427.3677997963553, 451.6695336823217, 478.87058797399175, 504.2583165345779, 529.2925398672547, 553.1189928235761, 576.5304058166864, 597.1163772936302, 633.1587369356564])

#alpha2 vs fluo3
#refUtz = np.array([0.208, 0.20852631578947367, 0.20905263157894735, 0.20957894736842103, 0.21010526315789474, 0.21063157894736842, 0.2111578947368421, 0.21168421052631578, 0.21221052631578946, 0.21273684210526314, 0.21326315789473685, 0.21378947368421053, 0.2143157894736842, 0.2148421052631579, 0.21536842105263157, 0.21589473684210525, 0.21642105263157896, 0.21694736842105264, 0.21747368421052632, 0.218])
#refPix= np.array([687.6850578135032, 654.9184586684075, 621.1376256748607, 590.2391680047651, 557.121328574367, 523.0694834164194, 489.4204244983938, 458.02920236779363, 424.06374516267476, 390.71085644897926, 359.6339274359316, 325.8529629205913, 291.99579913475435, 258.00587243870234, 226.59366556029394, 193.0062030759781, 159.86719708152464, 127.2107715011972, 86.14939221229258, 50.750123322701064])

#fluo2 vs utz
#refUtz = np.array([29.5, 29.657894736842106, 29.81578947368421, 29.973684210526315, 30.13157894736842, 30.289473684210527, 30.44736842105263, 30.605263157894736, 30.763157894736842, 30.92105263157895, 31.07894736842105, 31.236842105263158, 31.394736842105264, 31.55263157894737, 31.710526315789473, 31.86842105263158, 32.026315789473685])
#refPix = np.array([468.94778460418195, 467.03681280762373, 477.8366874007203, 476.3745815203798, 472.4123462597772, 469.4420954394949, 465.8460429366522, 463.32779233947565, 460.5916366855514, 456.67346096679523, 454.4131583644062, 450.10838468457916, 446.72300933996723, 442.9078120162559, 439.6615570305759, 435.90396698769774, 432.0502646884644])




bm07 = BM07Raytracing()

bm07.APERTURE_SL0 = 0.4
bm07.SHIFT_ALT_SOURCE = args.beam_alt
bm07.SHIFT_ANG_SOURCE = args.beam_ang
bm07.SHIFT_ALT_SL0 = args.SL0
bm07.SHIFT_ALT_M1 = args.M1
bm07.SHIFT_ALT_MOVEH = args.MOVEH
bm07.COURBURE_C2 = -1#0.075E6
bm07.NB_BEAM = 11


P_Source, photon_distribution, angles_distribution = bm07.creation_source()


for courbure in [20000]:
#for courbure in [5, 8, 9, 10, 11, 20000]:
    bm07.COURBURE_M2 = courbure*1E6
    hauteurList = []
    utzList = []
    for shift in np.linspace(-1.5,1.5,20):   
    #for shift in np.linspace(0.208,0.218,20):   
        bm07.SHIFT_UTZ=shift
        #bm07.ALPHA2=shift

        AllObject = bm07.create_geometry()
        AllObject = bm07.create_geometry_add_M2(AllObject, fluo2Up=False)
        nbHit, nbHitWeighted, rayons, rayons_w, hauteurFluo3 = bm07.propagation(AllObject, P_Source, photon_distribution, angles_distribution)
        print(bm07.SHIFT_UTZ, hauteurFluo3, -(537/9.*np.average(hauteurFluo3)))
        # FLUO3
        hauteurList.append(-(537/9.*np.average(hauteurFluo3))+310)
        # FLUO2
        #hauteurList.append(-(537/9.*0.26*(np.average(hauteurFluo3)))+430) #0.26 estimation champ de vue comparatif entre 25mm et 8mm
        # ALPHA2
        #hauteurList.append(-(537/9.*np.average(hauteurFluo3))-3020)
        
        utzList.append(33.01-bm07.SHIFT_UTZ-2)
        #utzList.append(bm07.ALPHA2)

    plt.plot(utzList,hauteurList, label='%.2fkm'%(bm07.COURBURE_M2/1E6))

plt.plot(refUtz, refPix, "o", label="mesure sur BM07")
plt.grid(b=True, which='major', color='k', linestyle='-')
plt.grid(b=True, which='minor', color='r', linestyle='-', alpha=0.2)
plt.minorticks_on()

plt.xlabel("Position UTZ (mm)")
#plt.xlabel("Position ALPHA2 (deg)")
plt.ylabel("Position verticale sur Fluo3 (pixel)")
#plt.title('Hauteur du faisceau sur fluo3 lors du scan UTZ (C2 courbe = %.2fkm)'%(bm07.COURBURE_C2/1E6))
plt.title('Hauteur du faisceau sur fluo3 lors du scan UTZ')
#plt.title('Hauteur du faisceau sur fluo3 lors du scan ALPHA2')
plt.legend(loc='upper center')

if args.outfile!="":
    plt.savefig(args.outfile)
else:
    plt.show()
