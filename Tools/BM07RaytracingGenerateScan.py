from BM07RaytracingClass import BM07Raytracing
from multiprocessing import Pool
import argparse
import sqlite3
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--BEG', metavar="BEG", type=int, default=0)
parser.add_argument('--END', metavar="END", type=int, default=0)
parser.add_argument('--outfile', metavar="outfile", type=str, help="database output name", default="resultat.sql")
args = parser.parse_args()


bm07 = BM07Raytracing()
bm07.NB_BEAM = 1000


#################################################################
BEG = args.BEG #mm
END = args.END #mm
bm07 = BM07Raytracing()
dataX = [-4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]
#################################################################


def scan(ERROR_ALT_SOURCE, ERROR_ANG_SOURCE, ERROR_ALT_SL0, ERROR_ALT_M1, ERROR_ALT_MOVEH):
    bm07.APERTURE_SL0 = 0.4
    bm07.SHIFT_ALT_SOURCE = ERROR_ALT_SOURCE
    bm07.SHIFT_ANG_SOURCE = ERROR_ANG_SOURCE
    bm07.SHIFT_ALT_SL0 = ERROR_ALT_SL0
    bm07.SHIFT_ALT_M1 = ERROR_ALT_M1
    bm07.SHIFT_ALT_MOVEH = ERROR_ALT_MOVEH
    diodeList = []
    diodeListWeighted = []
    P_Source, photon_distribution, angles_distribution = bm07.creation_source()
    TotalPhoton = sum(photon_distribution)
    for shift in dataX:   
        geometry = bm07.create_geometry()
        nbHit, nbHitWeighted, rayons, rayons_w, hauteurFluo3 =  bm07.propagation(geometry, P_Source, photon_distribution, angles_distribution)
        diodeList.append(nbHit)
        diodeListWeighted.append(nbHitWeighted)
    return diodeList, diodeListWeighted, TotalPhoton    
        

conn = sqlite3.connect(args.outfile)
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS simu
             (NB_BEAM real, NB_PHOTONS real, SL0 real, M1 real, MOVEH real, BEAM_ALT real,  BEAM_ANG real, SCAN BLOB, SCANW BLOB);''')



listarg = []
for ERROR_ALT_SOURCE in [0, -0.25, 0.25, -0.5, 0.5, -1, 1, -1.5, 1.5]:
    for ERROR_ANG_SOURCE in [0, -0.00005, 0.00005, -0.0001, 0.0001, -0.00015, 0.00015, -0.00020, 0.00020]:
        for ERROR_ALT_SL0 in [0, -0.5, 0.5, -1, 1, -1.5, 1.5, -2, 2, -2.5, 2.5]:
            for ERROR_ALT_M1 in [0, -0.25, 0.25, -0.5, 0.5, -0.75, 0.75, -1, 1, -1.25, 1.25, -1.5, 1.5, -1.75, 1.75, -2, 2, -2.25, 2.25, -2.5, 2.5, -3, 3,-3.5,3.5,-4,4,-4.5,4.5]:
                for ERROR_ALT_MOVEH in [0, -0.5, 0.5, -1, 1, -1.5, 1.5, -2, 2, -2.5, 2.5, -3, 3, -3.5, 3.5, -4, 4]:
                    listarg.append([ERROR_ALT_SOURCE, ERROR_ANG_SOURCE, ERROR_ALT_SL0, ERROR_ALT_M1, ERROR_ALT_MOVEH])
                    
print(len(listarg))
if END>len(listarg)-1:
    listarg=listarg[BEG:]
else:
    listarg=listarg[BEG:END+1]

with Pool() as p: 
    scan, scanWeighted, NbPhoton =zip(*p.starmap(scan, listarg))

for i in range(0, len(scan)):
    ERROR_ALT_SOURCE=listarg[i][0]
    ERROR_ANG_SOURCE=listarg[i][1]
    ERROR_ALT_SL0=listarg[i][2]
    ERROR_ALT_M1=listarg[i][3]
    ERROR_ALT_MOVEH=listarg[i][4]

    ok=False
    while not ok:
        try:
            c.execute("INSERT INTO simu VALUES (?,?,?,?,?,?,?,?,?);",[bm07.NB_BEAM,
                                                                NbPhoton[i],
                                                                ERROR_ALT_SL0,
                                                                ERROR_ALT_M1,
                                                                ERROR_ALT_MOVEH,
                                                                ERROR_ALT_SOURCE,
                                                                ERROR_ANG_SOURCE,
                                                                pickle.dumps(scan[i]),
                                                                pickle.dumps(scanWeighted[i])])
            conn.commit()
            ok=True
        except sqlite3.OperationalError:
            ok=False

conn.close()

