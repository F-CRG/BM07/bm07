from multiprocessing import Pool
from BM07RaytracingClass import BM07Raytracing
import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--SL0', metavar="SL0", type=float, help="shift in mm", default=0.0)
parser.add_argument('--M1', metavar="M1", type=float, help="shift in mm", default=0.0)
parser.add_argument('--MOVEH', metavar="MOVEH", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_alt', metavar="beam_alt", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_ang', metavar="beam_ang", type=float, help="shift in rad", default=0.0)
parser.add_argument('--outfile', metavar="outfile", type=str, help="graph png output name", default="")
args = parser.parse_args()



bm07 = BM07Raytracing()

bm07.APERTURE_SL0 = 0.4
bm07.SHIFT_ALT_SOURCE = args.beam_alt
bm07.SHIFT_ANG_SOURCE = args.beam_ang
bm07.SHIFT_ALT_SL0 = args.SL0
bm07.SHIFT_ALT_M1 = args.M1
bm07.SHIFT_ALT_MOVEH = args.MOVEH
bm07.NB_BEAM = 100

dataX = [-4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4]
P_Source, photon_distribution, angles_distribution = bm07.creation_source()

diodeList = []
for shift in dataX:   
    AllObject = bm07.create_geometry()
    nbHit, nbHitWeighted, rayons, rayons_w, hauteurFluo3 = bm07.propagation(AllObject, P_Source, photon_distribution, angles_distribution)
    print("SHIFT, NB HIT IN DIODE = ", shift, nbHit)
    diodeList.append(nbHitWeighted)

with Pool() as p: 
    nbHits, nbHitWeighteds, rayonss, rayons_ws = zip(*p.starmap(bm07.propagation, arglist))


plt.plot(dataX, nbHitWeighteds, marker = 'o')
plt.xlabel("position du scan (mm)")
plt.ylabel("Nb hit diode")
if args.outfile!="":
    plt.savefig(args.outfile)
else:
    plt.show()
