from BM07RaytracingClass import BM07Raytracing
from shapely.geometry import Point, LineString, MultiPoint
import matplotlib.pyplot as plt

import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--SL0app', metavar="SL0app", type=float, help="SLO0 aperture in mm", default=6)
parser.add_argument('--SL0', metavar="SL0", type=float, help="shift in mm", default=0.0)
parser.add_argument('--M1', metavar="M1", type=float, help="shift in mm", default=0.0)
parser.add_argument('--MOVEH', metavar="MOVEH", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_alt', metavar="beam_alt", type=float, help="shift in mm", default=0.0)
parser.add_argument('--beam_ang', metavar="beam_ang", type=float, help="shift in rad", default=0.0)
args = parser.parse_args()


bm07 = BM07Raytracing()

bm07.APERTURE_SL0 = args.SL0app
bm07.SHIFT_ALT_SOURCE = args.beam_alt
bm07.SHIFT_ANG_SOURCE = args.beam_ang
bm07.SHIFT_ALT_SL0 = args.SL0
bm07.SHIFT_ALT_M1 = args.M1
bm07.SHIFT_ALT_MOVEH = args.MOVEH
bm07.COURBURE_C2 = -1
bm07.NB_BEAM = 10

AllObject = bm07.create_geometry()
AllObject = bm07.create_geometry_add_M2(AllObject, fluo2Up=False)

P_Source, photon_distribution, angles_distribution = bm07.creation_source()
nbHit, nbHitWeighted, rayons, rayons_w, hauteurFluo3 = bm07.propagation(AllObject, P_Source, photon_distribution, angles_distribution)
print(hauteurFluo3)
rayons_w = rayons_w/max(rayons_w)

cmap=plt.get_cmap("rainbow")


for obj in AllObject:
    x,y = obj.geo.xy
    if obj.reflect:
        col = "b"
    else:
        col = "k"
    plt.plot(x,y,col)


for idx in range(0,len(rayons_w)):
    rayon = rayons[idx]
    full_ray = LineString(rayon)
    x,y = full_ray.xy
    rgba = cmap(rayons_w[idx])
    plt.plot(x,y,color=rgba)

plt.axis('equal')
plt.show()