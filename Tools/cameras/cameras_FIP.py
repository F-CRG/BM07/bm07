from flask import Flask, render_template, request, url_for, flash, redirect, send_file
from werkzeug.exceptions import abort
import PyTango
import numpy
import struct
import urllib3
from waitress import serve
import io
import sqlite3
import os
import gevent
import gevent.lock
from pyModbusTCP.client import ModbusClient
import base64

def get_db_zone():
    db_path = os.path.dirname(__file__)+"/zone.sqlite"
    db_conn = sqlite3.connect(db_path)
    db_cursor = db_conn.cursor()

    sql = 'create table if not exists zone (fluo INT(4) NOT NULL, points text NOT NULL);'
    db_cursor.execute(sql)

    #sql = "INSERT INTO `zone` (`fluo`, `points`) VALUES \
    #        (6, '[[744,205],[744,248],[802,277],[856,259],[871,218],[810,232],[746,207],[748,208],[748,208],[744,205],]'), \
    #        (9, '[[569,423],[569,469],[713,467],[713,413],[568,422],[568,422],[568,422],[569,423],]'), \
    #        (10, '[[518,221],[521,304],[769,310],[768,214],[520,216],[520,215],[519,222],[519,222],[520,220],[520,220],[518,221],]');" 
    #db_cursor.execute(sql)

    db_conn.commit()
    return db_conn



class camera_fip():
    def __init__(self):
        
        self.fluo = [PyTango.DeviceProxy("bm07/fluo/01"),
                     PyTango.DeviceProxy("bm07/fluo/02"),
                     PyTango.DeviceProxy("bm07/fluo/03")]

        self.wbm_avaliable = [False, False]
        self.wbm_tango_path = ["bm07/%s/wbm1","bm07/%s/wbm2"]
        self.wbm_bpm = []
        self.wbm_limaccd = []
        self.wbm_img = []
        self.wbm_Y = []
        self.wbm_run = []

        for i in range(0,len(self.wbm_tango_path)):
            self.wbm_avaliable.append(False)
            self.wbm_img.append(None)
            self.wbm_Y.append(-1)
            self.wbm_run.append(None)
            try:    
                self.wbm_bpm.append(PyTango.DeviceProxy(self.wbm_tango_path[i]%"bpm"))
                self.wbm_limaccd.append(PyTango.DeviceProxy(self.wbm_tango_path[i]%"limaccds"))
                self.wbm_avaliable[i]=True
                print("ici", i)
            except Exception as e:
                print("Error to import ",self.wbm_tango_path[i]%"bpm", self.wbm_tango_path[i]%"limaccds")
                print(e)

        print("self.wbm_avaliable[0]=",self.wbm_avaliable[0])
        print("self.wbm_avaliable[1]=",self.wbm_avaliable[1])
        if (self.wbm_avaliable[0]):
            self.wbm_bpm[0].subscribe_event('bvdata', PyTango.EventType.CHANGE_EVENT, self.decode_bvdata1)
            self.wbm_limaccd[0].subscribe_event('acq_status', PyTango.EventType.CHANGE_EVENT, self.watch_state1)
        if (self.wbm_avaliable[1]):
            self.wbm_bpm[1].subscribe_event('bvdata', PyTango.EventType.CHANGE_EVENT, self.decode_bvdata2)
            self.wbm_limaccd[1].subscribe_event('acq_status', PyTango.EventType.CHANGE_EVENT, self.watch_state2)

        try:
            self.__comm_lock = gevent.lock.RLock()
            self.wago = ModbusClient(host='bm07ebvwago', port=502, auto_open=True, debug=False)
            self.wago_wbm_add = [{"led_on": 514, "led_off": 515, "screen_in": 513, "screen_out": 512},
                                {"led_on": 522, "led_off": 523, "screen_in": 521, "screen_out": 520}]
        except Exception as e:
            print("Error to load wago WBM")
            print(e)
            for i in range(0, len(self.wbm_avaliable)):
                self.wbm_avaliable = False

    def pulse_command(self, name, wbm_idx):
        add = self.wago_wbm_add[wbm_idx][name]
        with self.__comm_lock:
            self.wago.write_single_coil(add, 0)
            gevent.sleep(0.01)
            self.wago.write_single_coil(add, 1)
            gevent.sleep(0.01)
            self.wago.write_single_coil(add, 0)

    def get_wbm_wago_status(self):
        led1 = "Off"
        led2 = "Off"
        screen1 = "Moving"
        screen2 = "Moving"

        if any(self.wbm_avaliable):
            with self.__comm_lock:
                bits = self.wago.read_coils(0, 7)
                #print(bits)

            if bits[2]==True:
                led1 = "On"
        
            if bits[6]==True:
                led2 = "On"
            
            if bits[0] == False and bits[1] == True:
                screen1 = "Out"
            elif bits[0] == True and bits[1] == False:
                screen1 = "In"
                
            if bits[4] == False and bits[5] == True:
                screen2 = "Out"
            elif bits[4] == True and bits[5] == False:
                screen2 = "In"

        return led1, screen1, led2, screen2

    def watch_state1(self, evt):
        if evt.attr_value:
            self.wbm_run[0]=evt.attr_value.value

    def watch_state2(self, evt):
        if evt.attr_value:
            self.wbm_run[1]=evt.attr_value.value


    def start_acq(self, i):
        self.wbm_limaccd[i].saving_mode = 'manual'
        self.wbm_limaccd[i].acq_trigger_mode = 'internal_trigger'
        self.wbm_limaccd[i].acq_mode = 'single'
        self.wbm_limaccd[i].acq_nb_frames = 0
        self.wbm_limaccd[i].acq_expo_time = 0.002

        if self.wbm_bpm[i].State() != PyTango.DevState.ON:
            self.wbm_bpm[i].Start()

        self.wbm_limaccd[0].stopAcq()
        self.wbm_limaccd[1].stopAcq()
        self.wbm_limaccd[i].prepareAcq()
        self.wbm_limaccd[i].startAcq()


    def stop_acq(self):
        for i in [0,1]:
            if self.wbm_limaccd[i].acq_status == "Running":
                self.wbm_limaccd[i].stopAcq()
                gevent.sleep(0.1)
            if self.wbm_bpm[i].State() == PyTango.DevState.ON:
                self.wbm_bpm[i].Stop()
                gevent.sleep(0.1)
 

    def decode_bvdata1(self, evt_bvdata):
        self.decode_bvdata(evt_bvdata,0)

    def decode_bvdata2(self, evt_bvdata):
        self.decode_bvdata(evt_bvdata,1)


    def decode_bvdata(self, evt_bvdata, id_wbm):
        """Callback function from the subscribe_event on bvdata"""

        if not evt_bvdata.attr_value:
            print('No bvdata value returning')
            return

        if not evt_bvdata.attr_value.value:
            print('No bvdata value.value returning')
            return

        try:
            #print(evt_bvdata.attr_value.value)
            self.parse_bvdata(evt_bvdata.attr_value.value, id_wbm)

        except AttributeError as TypeError:
            print ("Subscribe event callback")

    def parse_bvdata(self, bvdata, id_wbm):
        bv_data = bvdata[1]
        HEADER_FORMAT = bvdata[0]
        (timestamp,framenb,
            X,Y,I,maxI,roi_top_x,roi_top_y,
            roi_size_getWidth,roi_size_getHeight,
            fwhm_x,fwhm_y, prof_x, prof_y, jpegData) = struct.unpack(HEADER_FORMAT, bv_data)

        profile_x = numpy.frombuffer(prof_x, dtype=numpy.int64).tolist()
        profile_y = numpy.frombuffer(prof_y, dtype=numpy.int64).tolist()
        #print("refresh id=",id_wbm, X,Y)
        self.wbm_img[id_wbm]=("data:image/jpg;base64,%s"%jpegData.decode("utf-8")).encode()
        self.wbm_Y[id_wbm]=Y
        return {"framenb" : framenb, "X" : X, "Y" : Y, "I" : I, "fwhm_x" : fwhm_x, "fwhm_y" : fwhm_y,  "jpegData": jpegData.decode('utf-8'), "profile_x" : profile_x, "profile_y" : profile_y}

def get_preset(ip):
    try:
        url ='http://bm07recorder.esrf.fr:%d/axis-cgi/com/ptz.cgi?query=presetposcam'%(7000+ip)
        http_pool = urllib3.connection_from_url(url)
        r = http_pool.urlopen('GET',url)
        presets=[]
        for preset in r.data.decode().splitlines():
            presets.append(preset.strip().split('=')[-1])
        if len(presets)>1:
            presets=presets[1:]
        print(presets)
        return presets
    except Exception as e:
        print("Error to get ", url)
        print(e)
        return ""

camera = camera_fip()
app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'

script_dir = os.path.dirname(__file__) 
blank = open("%s/static/blank.jpg"%script_dir, "rb").read()
img_base64 = base64.b64encode(blank)
blank = ("data:image/jpg;base64,%s"%img_base64.decode("utf-8")).encode()
#blank ="data:image/jpg;base64,".encode()+blank

@app.route('/')
def index():
    print("ici")
    preset3 = get_preset(3)
    print(preset3)
    preset5 = get_preset(5)
    preset8 = get_preset(8)
    return render_template('index.html', button3=preset3, button5=preset5, button8=preset8)

@app.route('/detail', methods=['GET'])
def detail():
    idx = int(request.args.get('idx'))
    presets = get_preset(idx)
    return render_template('detail.html', idx=idx, ip=7000+idx, presets=presets)

@app.route('/getzone', methods=['GET'])
def getzone():
    fluo = int(request.args.get('fluo'))
    db = get_db_zone()
    cursor = db.cursor()
    sql = "SELECT points FROM `zone` WHERE fluo IS %d;"%fluo
    cursor.execute(sql)
    records = cursor.fetchall()
    if records is not None:
        print(records)
        return  records[-1][0]
    else:
        return ""
    
@app.route('/setzone', methods=['GET'])
def setzone():
    fluo = int(request.args.get('fluo'))
    points = request.args.get('points')
    db = get_db_zone()
    cursor = db.cursor()
    sql = "INSERT OR REPLACE INTO zone VALUES (%d,\'[%s]\');"%(fluo, points)
    cursor.execute(sql)
    db.commit()
    return ""

@app.route('/move', methods=['GET'])
def move():
    num = request.args.get('num')
    val = request.args.get('val')
    if val is None or num is None:
        print("error with arg")
        return ""
    
    num=int(num)
    val = int(val)
    if num<=3 and num>0:
        if val==1:
            camera.fluo[num-1].command_inout("Open")
        elif val==0:
            camera.fluo[num-1].command_inout("close")
    return ""

@app.route('/start_live', methods=['GET'])
def start_live():
    num = int(request.args.get('num'))
    if num==0:
        camera.pulse_command("screen_in", 0)
        camera.pulse_command("screen_out", 1)
    else:
        camera.pulse_command("screen_out", 0)
        camera.pulse_command("screen_in", 1)
        
    camera.start_acq(num)
    return ""

@app.route('/wbm_status')
def wbm_status():
    led1, screen1, led2, screen2 = camera.get_wbm_wago_status()
    #print("%s %s %d %s %s %d"%(camera.wbm_run[0], screen1, camera.wbm_Y[0], camera.wbm_run[1], screen2,camera.wbm_Y[1]))
    return "%s %s %d %s %s %d"%(camera.wbm_run[0], screen1, camera.wbm_Y[0], camera.wbm_run[1], screen2,camera.wbm_Y[1])

@app.route('/stop_live')
def stop_live():
    camera.pulse_command("screen_out", 0)
    camera.pulse_command("screen_out", 1)
    camera.stop_acq()
    return ""

@app.route('/images/<int:pid>.jpg')
def get_image(pid):
    img_source = blank
    if camera.wbm_avaliable[pid]:
        if camera.wbm_run[pid] == "Running":
            img_source = camera.wbm_img[pid]
        else:
            img_source = blank
    return send_file(
        io.BytesIO(img_source),
        download_name='%s.jpg' % pid,
        mimetype='image/jpeg'
    )

if __name__ == "__main__":
    #debug webserver
    #app.run(host= '0.0.0.0', port=8067)

    #prod webserver
    serve(app, host='0.0.0.0', port=8067)