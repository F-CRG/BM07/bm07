#!/bin/bash

conda activate base

for i in {0..439263..6000}
do
    oarsub -l nodes=1,walltime=10:00:00 --notify="mail:sallazda@esrf.fr" "python3 BM07RaytracingGenerateScan.py --BEG $i --END $((5999+$i)) --outfile out.sql";
done