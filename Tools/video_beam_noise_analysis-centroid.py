#soft pour visualiser le niveau de bruit du faisceau (en vertical)

import numpy as np 
# importing matplotlib modules 
import matplotlib.image as mpimg 
import matplotlib.pyplot as plt 

from skimage import color
from skimage import io
from PIL import Image
import scipy.optimize as opt
from scipy import ndimage

import glob




img2 = []
xvalue = []
file = 'C:\\Users\\ermathie\\Work Folders\\Documents\\FIPSHARE\\Video\\2021_test_vibrations_faisceau\\video_roots_E2\\*.png'
title = file.split('\\')[-2]
#print (title)
for filepath in glob.iglob(file):
    #print(filepath)
    #image = Image.open(filepath, "r")
    img = []
    img=color.rgb2gray(io.imread(filepath))
    imgcrp = img[300:600 , 500:800]
    centroid = ndimage.measurements.center_of_mass(imgcrp)
    #print (centroid)
    xvalue.append(centroid[0]+300)
    print ("-", end =" ")

plt.ylim((-25, 25))
plt.title(title)
plt.plot(xvalue - xvalue[0],"-")

np.savetxt('C:\\Users\\ermathie\\Work Folders\\Documents\\FIPSHARE\\Video\\2021_test_vibrations_faisceau\\centroid\\pic_position_centroid_' + title + ".txt", xvalue, fmt='%.18e', delimiter=' ', newline='\n', header='', footer='', comments='# ', encoding=None)


#-------------------------------- Split here in jupyter... ----------------------------

path = 'C:\\Users\\ermathie\\Work Folders\\Documents\\FIPSHARE\\Video\\2021_test_vibrations_faisceau\\centroid\\*.txt'

#plt.ylim((-25, 25))
plt.figure(0,figsize=(10,7))
plt.title("Beam vertical fluctuations")
shift = 0


for filepath in glob.iglob(path):
    xmax = []
    title = filepath.split('\\')[-1]
    xmax.append(np.genfromtxt(filepath))
    print (title, end = ".     ")
    print ("Ecart Type: %f2"%statistics.pstdev(xmax[0]))
    avg = np.average(xmax[0])
    plt.plot(xmax[0] - avg + shift, label =title, linestyle = "-")
    shift += 10
plt.legend(loc='best')
plt.show()

#frequency analysis
shift = 0
welch = []
plt.figure(0,figsize=(10,7))
plt.title("Beam vertical fluctuations (freq analysis)")
for filepath in glob.iglob(path):
    xmax = []
    title = filepath.split('\\')[-1]
    xmax.append(np.genfromtxt(filepath))
    f, Pwelch_spec = signal.welch(xmax, 24, scaling='spectrum')
    plt.semilogy(f, Pwelch_spec[0,:], label =title)
    shift += 10
    
plt.legend(loc='best')
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD')
plt.grid()
plt.show()



