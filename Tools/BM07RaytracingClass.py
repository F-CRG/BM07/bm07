from multiprocessing import Pool
from shapely.geometry import Point, LineString, MultiPoint
from shapely.affinity import rotate
from shapely.ops import nearest_points, substring
from math import cos, sin, atan, atan2, degrees, radians, pi, exp
import numpy as np
from scipy import stats

NOT_INTER_DISTANCE = 123456789  # distance de nom interception

class objet_optique():
    def __init__(self, name, liste_coord, centre_rot, angle, shift=0, reflect=False):
        self.name = name
        self.geo = LineString(liste_coord)
        self.centre = centre_rot
        self.geo=rotate(self.geo, angle, origin=centre_rot, use_radians=False)
        self.reflect = reflect

    def extendline_from_direction(self, orig, angle, dist):
        Xend = orig.x+dist*cos(angle)
        Yend = orig.y+dist*sin(angle)
        return Point(Xend,Yend)

    def intercept(self, begin, angle):
        end = self.extendline_from_direction(begin, angle, 100000)
        line = LineString([begin, end])
        intersection = line.intersection(self.geo)
        if intersection.is_empty:
            return None, NOT_INTER_DISTANCE
        if not isinstance(intersection, Point):
            intersection = list(intersection)[0]
        return intersection, LineString([begin, intersection]).length
    
    def get_surface_facette(self, intersection):
        ratio_intersection = (intersection.x-self.geo.bounds[0])/(self.geo.bounds[-2]-self.geo.bounds[0])
        nearest_geoms = list(substring(self.geo, ratio_intersection-0.002, ratio_intersection+0.002, normalized=True).coords)
        return Point(nearest_geoms[0]), Point(nearest_geoms[-1])



class BM07Raytracing:

    def __init__(self):
        self.SHIFT_ALT_SOURCE=0
        self.SHIFT_ANG_SOURCE=0
        self.SHIFT_ALT_SL0=0
        self.SHIFT_ALT_M1=0
        self.SHIFT_ALT_MOVEH=0
        self.SHIFT_UTZ=0
        self.APERTURE_SL0=6
        self.NB_BEAM=1000
        self.NRJ = 12.6 #keV
        self.COURBURE_M1 = 14.31E6 #mm -> 14km
        self.COURBURE_M2 = 10.77E6 #mm -> 10km
        self.COURBURE_C2 = -1
        self.ALPHA2 = 0.235 #deg

    def divVertFWHM(self, E): #nfrom ils
        a1=164  #µrad
        a2=245 #µrad
        b1=0.01375 #kev^-1
        b2=0.14418 #kev^-1
        return (a1*exp(-b1*E)+a2*exp(-b2*E))*1e-6 #rad   FWHM

    def divVert(self, E): #from nils
        a1=164  #µrad
        a2=245 #µrad
        b1=0.01375 #kev^-1
        b2=0.14418 #kev^-1
        return (a1*exp(-b1*E)+a2*exp(-b2*E))*1e-6*0.849*2 #rad   FWHM 

    def FWHM_to_sigma(self, FWHM):
        return FWHM/2.355

    def extendline_from_direction(self, orig, angle, dist):
        Xend = orig.x+dist*cos(angle)
        Yend = orig.y+dist*sin(angle)
        return Point(Xend,Yend)

    def angle_reflechi(self, Psource,Pcontact,Psurf1,Psurf2):
        angleSource = atan2(Pcontact.y-Psource.y,Pcontact.x-Psource.x)
        angleSurface = atan2(Psurf2.y-Psurf1.y,Psurf2.x-Psurf1.x)
        #print("angleSource=",degrees(angleSource))
        #print("angleSurface=",degrees(angleSurface))
        return 2*angleSurface-angleSource



    def create_geometry(self, shiftOptics=0.0):
        AllObject = []

        #############################
        #  M1
        #############################

        courbeM1 = self.COURBURE_M1
        lengthM1 = 1280 #mm
        heightM1 = 90 #mm
        P_M1 = Point(29601, shiftOptics+self.SHIFT_ALT_M1) #mm

        #M1 partie reflechissante
        demiAngleArc = atan(0.5*lengthM1/courbeM1)
        listePoint=[]
        altituderef = cos(demiAngleArc)*courbeM1
        for angle in np.linspace(-demiAngleArc, demiAngleArc, 1000):
            listePoint.append(Point(P_M1.x+sin(angle)*courbeM1, P_M1.y+altituderef-cos(angle)*courbeM1))
        AllObject.append(objet_optique("M1_refl", listePoint, P_M1, 0.237,reflect=True))

        #M1 corp non reflechissant
        first=listePoint[0]
        end = listePoint[-1]
        listePoint=[first, Point(first.x, first.y-heightM1),
                    Point(end.x,end.y-heightM1), end]
        AllObject.append(objet_optique("M1_corps", listePoint, P_M1, 0.237,reflect=False))



        #############################
        #  MASQUE M1
        #############################

        P_MasqueM1 = Point(28882, shiftOptics+self.SHIFT_ALT_M1)
        appertureMasqueM1 = 6.8 #mm
        listePoint = [Point(P_MasqueM1.x, appertureMasqueM1), Point(P_MasqueM1.x, appertureMasqueM1+100)]
        AllObject.append(objet_optique("Masque_M1_haut", listePoint, P_M1, 0.237, reflect=False))
        listePoint = [Point(P_MasqueM1.x, 0), Point(P_MasqueM1.x, -100)]
        AllObject.append(objet_optique("Masque_M1_bas", listePoint, P_M1, 0.237, reflect=False))


        #############################
        #  TUNGSTEN
        #############################
        P_TUNGSTEN = Point(31780, 4.9) #mm
        listePoint = [Point(P_TUNGSTEN.x, P_TUNGSTEN.y), Point(P_TUNGSTEN.x, P_TUNGSTEN.y-100)]
        AllObject.append(objet_optique("Bloc_tungsten", listePoint, P_TUNGSTEN, 0, reflect=False))
                        

        #############################
        #  C1
        #############################

        P_C1 = Point(32336, 22.62+shiftOptics+self.SHIFT_ALT_MOVEH) #mm
        lengthC1 = 50 #mm
        heightC1 = 50 #mm

        #C1 partie reflechissante
        listePoint = [Point(P_C1.x-0.5*lengthC1, P_C1.y), Point(P_C1.x+0.5*lengthC1, P_C1.y)]
        AllObject.append(objet_optique("C1_refl", listePoint, P_C1, -8.514, reflect=True))

        #C1 corp non reflechissant
        listePoint = [Point(P_C1.x-0.5*lengthC1, P_C1.y), Point(P_C1.x-0.5*lengthC1, P_C1.y+heightC1),
                    Point(P_C1.x+0.5*lengthC1, P_C1.y+heightC1), Point(P_C1.x+0.5*lengthC1, P_C1.y)]
        AllObject.append(objet_optique("C1_corps", listePoint, P_C1, -8.514, reflect=False))



        #############################
        #  C2
        #############################

        P_C2 = Point(P_C1.x+208.70, P_C1.y-33.01+self.SHIFT_UTZ) #mm
        lengthC2 = 95 #mm
        heightC2 = 50 #mm

        if self.COURBURE_C2 != -1:
            #C2 partie reflechissante version courbé (comme julien)
            courbeC2 = self.COURBURE_C2
            demiAngleArc = atan(0.5*lengthC2/courbeC2)
            listePoint=[]
            altituderef = cos(demiAngleArc)*courbeC2
            for angle in np.linspace(-demiAngleArc, demiAngleArc, 1000):
                listePoint.append(Point(P_C2.x+sin(angle)*courbeC2, P_C2.y+altituderef-cos(angle)*courbeC2))
            AllObject.append(objet_optique("C2_refl", listePoint, P_C1, -8.514,reflect=True))
        else:
            #C2 partie reflechissante
            listePoint = [Point(P_C2.x-0.5*lengthC2, P_C2.y), Point(P_C2.x+0.5*lengthC2, P_C2.y)]
            AllObject.append(objet_optique("C2_refl", listePoint, P_C1, -8.514, reflect=True))

        #C2 corp non reflechissant
        listePoint = [Point(P_C2.x-0.5*lengthC2, P_C2.y), Point(P_C2.x-0.5*lengthC2, P_C2.y-heightC2),
                    Point(P_C2.x+0.5*lengthC2, P_C2.y-heightC2), Point(P_C2.x+0.5*lengthC2, P_C2.y)]
        AllObject.append(objet_optique("C2_corps", listePoint, P_C1, -8.514, reflect=False))
                        
    

        #############################
        #  MASQUE FE
        #############################

        P_MasqueFE = Point(23437,0)
        appertureMasqueFE = 50 #mm normalement 8mm
        listePoint = [Point(P_MasqueFE.x, P_MasqueFE.y+0.5*appertureMasqueFE), 
                        Point(P_MasqueFE.x, P_MasqueFE.y+0.5*appertureMasqueFE+100)]
        AllObject.append(objet_optique("Masque_FE_haut", listePoint, P_MasqueFE, 0, reflect=False))
        listePoint = [Point(P_MasqueFE.x, P_MasqueFE.y-0.5*appertureMasqueFE), 
                        Point(P_MasqueFE.x, P_MasqueFE.y-0.5*appertureMasqueFE-100)]
        AllObject.append(objet_optique("Masque_FE_bas", listePoint, P_MasqueFE, 0, reflect=False))


        #############################
        #  MASQUE BE
        #############################

        P_MasqueBE = Point(30577,-7.1)
        appertureMasqueBE = 30
        listePoint = [Point(P_MasqueBE.x, P_MasqueBE.y+appertureMasqueBE), 
                    Point(P_MasqueBE.x, P_MasqueBE.y+appertureMasqueBE+100)]
        AllObject.append(objet_optique("Masque_BE_haut", listePoint, P_MasqueBE, 0, reflect=False))
        listePoint = [Point(P_MasqueBE.x, P_MasqueBE.y), 
                        Point(P_MasqueBE.x, P_MasqueBE.y-100)]
        AllObject.append(objet_optique("Masque_BE_bas", listePoint, P_MasqueBE, 0, reflect=False))


        #############################
        #  SL0
        #############################

        P_SL0 = Point(26958,shiftOptics+self.SHIFT_ALT_SL0)
        appertureSL0 = self.APERTURE_SL0 #mm
        listePoint = [Point(P_SL0.x, P_SL0.y+0.5*appertureSL0), Point(P_SL0.x, P_SL0.y+0.5*appertureSL0+100)]
        AllObject.append(objet_optique("SL0_haut", listePoint, P_SL0, 0, reflect=False))
        listePoint = [Point(P_SL0.x, P_SL0.y-0.5*appertureSL0), Point(P_SL0.x, P_SL0.y-0.5*appertureSL0-100)]
        AllObject.append(objet_optique("SL0_bas", listePoint, P_SL0, 0, reflect=False))
        return AllObject

    def create_geometry_add_M2(self, AllObject, shiftOptics=0.0, fluo2Up=False):
        #############################
        #  M2
        #############################

        courbeM2 = self.COURBURE_M2#-1E6
        courbeM2b = self.COURBURE_M2
        lengthM2 = 1280 #mm
        heightM2 = 90 #mm
        P_M2 = Point(35067, shiftOptics-20) #mm

        #M2 partie reflechissante
        demiAngleArc1 = atan(0.5*lengthM2/courbeM2)
        demiAngleArc2 = atan(0.5*lengthM2/courbeM2b)
        listePoint=[]
        altituderef1 = cos(demiAngleArc1)*courbeM2
        altituderef2 = cos(demiAngleArc2)*courbeM2b

        for angle in np.linspace(-demiAngleArc1, 0, 500):
            listePoint.append(Point(P_M2.x+sin(angle)*courbeM2, P_M2.y-altituderef1+cos(angle)*courbeM2))
        for angle in np.linspace(0, demiAngleArc2, 500):
            listePoint.append(Point(P_M2.x+sin(angle)*courbeM2b, P_M2.y-altituderef2+cos(angle)*courbeM2b-altituderef1+courbeM2+altituderef2-courbeM2b))
        AllObject.append(objet_optique("M2_refl", listePoint, P_M2, self.ALPHA2,reflect=True))



        #M2 corp non reflechissant
        first=listePoint[0]
        end = listePoint[-1]
        listePoint=[first, Point(first.x, first.y+heightM2),
                    Point(end.x,end.y+heightM2), end]
        AllObject.append(objet_optique("M2_corps", listePoint, P_M2, self.ALPHA2,reflect=False))

        #############################
        #  FLUO 2
        #############################

        if fluo2Up:
            P_FLUO2 = Point(36418, -20)
            listePoint = [Point(P_FLUO2.x, -90), Point(P_FLUO2.x, 90)]
            AllObject.append(objet_optique("FLUO2", listePoint, P_FLUO2, -57.1, reflect=False))


        #############################
        #  FLUO 3
        #############################

        P_FLUO3 = Point(55088, -20)
        listePoint = [Point(P_FLUO3.x, -90), Point(P_FLUO3.x, 90)]
        AllObject.append(objet_optique("FLUO3", listePoint, P_FLUO3, -57.1, reflect=False))

        return AllObject

    def creation_source(self):
        # SOURCE
        P_Source = Point(0, self.SHIFT_ALT_SOURCE)
        beam_sigma = self.FWHM_to_sigma(self.divVertFWHM(self.NRJ))
        angles_distribution = np.linspace(-2*beam_sigma, 2*beam_sigma, self.NB_BEAM)
        photon_distribution = stats.norm.pdf(angles_distribution, 0, beam_sigma)
        angles_distribution+=self.SHIFT_ANG_SOURCE
        return P_Source, photon_distribution, angles_distribution

    def propagation(self, geometry, P_Source, photon_distribution, angles_distribution):
        nbHit=0
        nbHitWeighted=0
        rayons = []
        rayons_w = []
        hauteurFluo = []
        for idxPhoton in range(0, len(angles_distribution)):
            angle = angles_distribution[idxPhoton]
            poids = photon_distribution[idxPhoton]
            geometryCopy = geometry.copy()
            rayon = [P_Source]
            dead = False
            while(not dead):
                liste_intersection=[]
                liste_distance=[]
                for obj in geometryCopy:
                    intersection, distance = obj.intercept(rayon[-1], angle)
                    liste_intersection.append(intersection)
                    liste_distance.append(distance)
                distance_min = min(liste_distance)
                if distance_min == NOT_INTER_DISTANCE:
                    dead=True
                    rayon.append(self.extendline_from_direction(rayon[-1], angle, 33200))
                else:
                    idx = liste_distance.index(distance_min)
                    obj = geometryCopy[idx]
                    intersection = liste_intersection[idx]
                    #print("\trayon hit",obj.name,"at",intersection)
                    if obj.reflect:
                        objSurf_P1, objSurf_P2 = obj.get_surface_facette(intersection)
                        new_angle = self.angle_reflechi(rayon[-1],intersection,objSurf_P1,objSurf_P2)
                        angle=new_angle
                    else:
                        dead=True
                    if obj.name=="C2_refl": #SI C2 touché on compte comme si diode
                        nbHit+=1
                        nbHitWeighted+=poids
                    elif obj.name=="FLUO2" : #SI FLUO3 touché on recup la hauteur
                        hauteurFluo.append(intersection.x-36418)
                    elif obj.name=="FLUO3" : #SI FLUO3 touché on recup la hauteur
                        hauteurFluo.append(intersection.x-55088)
                    rayon.append(intersection)
                    del(geometryCopy[idx])
            rayons.append(rayon)
            rayons_w.append(poids)
        return nbHit, nbHitWeighted, rayons, rayons_w, hauteurFluo




