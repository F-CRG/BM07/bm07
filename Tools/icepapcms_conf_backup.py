import sqlite3
import os 


db_file = "/users/blissadm/.icepapcms/sqlitedb/icepapcms.db"
python_script_path = os.path.dirname(os.path.abspath(__file__))
output_path = "%s/../icepap_conf_xml"%python_script_path
output_old_conf_path = "%s/oldconf"%output_path
if not os.path.exists(output_old_conf_path):
    os.mkdir(output_old_conf_path)

if os.path.isfile(db_file) and os.access(db_file, os.R_OK):
    print("Database file", db_file, "is readable")
else:
    print("Database file", db_file, "is NOT readable")
    exit()

con = sqlite3.connect(db_file)


cur = con.cursor()

cur.execute("SELECT DISTINCT cfg_id FROM cfgparameter ORDER BY cfg_id ASC;")
icepap_conf_list = [x[0] for x in cur.fetchall()]

for conf_id in icepap_conf_list:
    cur.execute("SELECT name, value FROM cfgparameter WHERE cfg_id=%d ORDER BY name ASC;"%conf_id)
    axis = dict(cur.fetchall())

    xml_file_name_path = "%s/%s.xml"%(output_path, axis['IPAPNAME'])
    xml_old_file_name_path = "%s/%s_%d.xml"%(output_old_conf_path, axis['IPAPNAME'], conf_id)

    for old in [True, False]:
        print(xml_file_name_path,"v%d"%conf_id, end="")
        if old:
            xml = open(xml_file_name_path,"w")
        else:
            xml = open(xml_old_file_name_path,"w")
        
        xml.write("<?xml version=\"1.0\" ?>\n")
        xml.write("<Driver>\n")
        for prop, value in axis.items():
            if prop == "ID":
                value = value.split("'")[1]
            xml.write("\t<par name=\"%s\" value=\"%s\"/>\n"%(prop,value))
        xml.write("</Driver>\n")
        xml.close()
        print(" -> OK")
        
