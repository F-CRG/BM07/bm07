from pyModbusTCP.client import ModbusClient
import time 

c = ModbusClient('wcd07g.esrf.fr')



while True:
    # open or reconnect TCP to server
    if not c.is_open():
        if not c.open():
            print("unable to connect to "+SERVER_HOST+":"+str(SERVER_PORT))

    # if open() is ok, read register (modbus function 0x03)
    if c.is_open():
        # read 10 registers at address 0, store result in regs list
        regs = c.read_holding_registers(0, 2)
        # if success display registers
        if regs:
            XFU0 = regs[0] & 0x07
            value0 = regs[0] & 0xFFF8
            XFU1 = regs[1] & 0x07
            value1 = regs[1] & 0xFFF8
            print("%f(%d) %f(%d)"%(value0*0.9765625/1000, XFU0, value1*0.9765625/1000, XFU1))

    # sleep 2s before next polling
    time.sleep(2)
