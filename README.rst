============
BM07 project
============

[![build status](https://gitlab.esrf.fr/BM07/bm07/badges/master/build.svg)](http://BM07.gitlab-pages.esrf.fr/BM07)
[![coverage report](https://gitlab.esrf.fr/BM07/bm07/badges/master/coverage.svg)](http://BM07.gitlab-pages.esrf.fr/bm07/htmlcov)

BM07 software & configuration

Latest documentation from master can be found [here](http://BM07.gitlab-pages.esrf.fr/bm07)
