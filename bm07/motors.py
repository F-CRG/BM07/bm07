from bliss.controllers.motor import CalcController
from bliss.common.logtools import *
from bm07 import inclino_FIP
from math import asin,sin


class bragg(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.inclinoM1 = self.config.get("inclino", inclino_FIP)
        self.slope_source = self.config.get("slope_source", float)
  
    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)
        axis.no_offset = True

    def calc_from_real(self, positions_dict):
        bragg_angle = -self.slope_source + 2*self.inclinoM1.read() + positions_dict["beta"]
        return {"calc_brag": bragg_angle}

    def calc_to_real(self, positions_dict):
        real_pos = self.slope_source - 2*self.inclinoM1.read() + positions_dict["calc_brag"]
        return {"beta": real_pos}

class mirrorup(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.distancedetweenelevator = 650

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)

    def calc_from_real(self, positions_dict):
        mirrorangle = asin((positions_dict["upfr"]-positions_dict["upbk"])/self.distancedetweenelevator)
        mirroraltitude = (positions_dict["upfr"]+positions_dict["upbk"])/2.
        return {"altitude":mirroraltitude, "angle":mirrorangle}

    def calc_to_real(self, positions_dict):
        sinangle = sin(positions_dict["angle"])
        upfr = positions_dict["altitude"]-sinangle*self.distancedetweenelevator/2.
        upbk = positions_dict["altitude"]+sinangle*self.distancedetweenelevator/2.
        return {"upfr":upfr, "upbk":upbk}

class bend_calc(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)

    def calc_from_real(self, positions_dict):
        c2benddiff = positions_dict["bend1"]-positions_dict["bend2"]
        c2bend = (positions_dict["bend1"]+positions_dict["bend2"])/2.
        return {"benddiff":c2benddiff, "bend":c2bend}

    def calc_to_real(self, positions_dict):
        bend2 = positions_dict["bend"]-0.5*positions_dict["benddiff"]
        bend1 = positions_dict["bend"]+0.5*positions_dict["benddiff"]
        return {"bend2":bend2, "bend1":bend1}


