/*************************************************************************/
/*************************************************************************/
/**                                                                     **/
/**                                 GEOFIP                              **/
/**                                                                     **/
/**         program for geometry modelisation on beamline FIP2          **/
/**         author           : J-L Ferrer (jean-luc.ferrer@ibs.fr)      **/
/**         last modification: 05/01/2023                               **/
/**                                                                     **/
/**     To compile: gcc geofip_FIP2_v3.c -o geofip -lm                  **/
/*************************************************************************/
/*************************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>


#define  VERBOSE        2           /* >=1 en mode debug, 0 sinon      */ 
#define  SIZE_STRING	120
#define  Pi             3.141592653
#define  DILAT_C1       -0.00023    /* DL/L de c1 with respect to c2   */
                                    /*    0.0     at Tc1 = 293 K       */
                                    /*   -0.00023 at Tc1 =  75 K       */
                                    /*   -0.00024 at Tc1 = 100 K       */
#define  PARAM_SI	6.271       /* 2d of Si at 20degC, en Ang.     */
                                    /*   Si 111: 6.271                 */
                                    /*   Si 311: 3.274                 */
#define  CUTOFF		0.75        /* default value of the cutoff     */
#define  CUT_ANGLE      0.00563     /* 0.00685 for Pt                  */
                                    /* 0.00563 for Rh                  */

/*-------------------*/
/*       FIP         */
/*-------------------*/

/* #define  X_SOURCE	    0.0     */
/* #define  X_M1	26815.2     */
/* #define  X_BE	27806.5     */
/* #define  X_FILW	28783.0     */
/* #define  X_C1	29764.0     */
/* #define  X_M2	32712.8     */
/* #define  X_FLUO1	31447.0     */
/* #define  X_FLUO2	34112.0     */
/* #define  X_FLUO3	52719.0     */
/* #define  X_SP	54638.0     */

/*-------------------*/
/*       FIP2        */
/*-------------------*/

#define  X_SOURCE        0.00  /* everything shifted -2800.00 */
#define  X_MASK      23437.00  /* masque dans le front end    */
#define  X_CACHE     28882  /* cache a l entree de M1      */
#define  X_M1        29603
#define  X_BE        30577
#define  X_WBM2      31040
#define  X_C1        32336
#define  X_M2        35064.5
#define  X_FLUO1     33550
#define  X_FLUO2     36418
#define  X_FLUO3     55088
#define  X_SP        57157

/*-------------------*/

#define  Z_SOURCE	0.0         /* altitude first mirror           */
#define  SLOPE_SOURCE   0.0         /* assuming z magnet = 0           */
#define  FAN_SOURCE     0.001663    /* div horiz du faisceau, en rad   */
#define  DIV_V_SOURCE   0.0002      /* div vert du faisceau a 10 keV, en rad FWMH */
#define  APERT_V_MASK   8.0         /* ouverture verticale du mask (mm)*/
#define  APERT_V_CACHE  6.8         /* ouverture verticale du cache a l entree de M1 (mm)*/
#define  Z_SAMPLE	-20.        /* altitude second mirror          */
#define  LENGTH_MIRRORS 1300.0      /* longueur des miroirs            */

#define  RATE_Z_MIRRORS	20000.
#define  ORIG_INCL1	0.0         /* -0.0435  0.03344                */
#define  ORIG_INCL2	0.0         /* last modif. 21 Apr 99           */
#define  RATE_INCL	-1.
#define  RATE_M1_MOT	-25151.     /* motor steps per deg.            */
#define  RATE_M2_MOT	-23057.     /* motor steps per deg.            */
#define  ORIG_H		0.0         /* altitude C1 fin de course bas   */
                                    /* crystal1 111: -2.86             */
                                    /* crystal1 311: -3.29             */
#define  RATE_H		2000.       /* steps per mm                    */
#define  ORIG_B		0.0         /* origine codeur                  */
#define  ASYM_B		0.0         /* crystal asymetry                */
                                    /* crystal 111: 0.087              */
                                    /* crystal 111: ?                  */
#define  RATE_B		20000.      /* steps per deg.                  */
#define  RATE_B_MOT	4000.       /* steps per deg.                  */
#define  ORIG_X		134.        /* 130.                            */
#define  RATE_X		-2000.      /* steps per mm                    */
#define  ORIG_Z         18.0        /* gap vertical entre les crystaux */
                                    /* crystal2 peigne: 27.5           */
                                    /* crystal2 311 sur bender: 38.5   */
#define  RATE_Z		-2000.      /* steps per mm                    */

  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/
  /*                                                                   */
  /*                            MAIN FUNCTION                          */
  /*                                                                   */
  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/

main (argc,argv)
	int	argc;
	char	*argv[];

 {
  
  char   file_name[SIZE_STRING],stmp[SIZE_STRING];
  float  z_mask,z_m1,z_be,z_wbm2,z_c1,z_c2,z_m2,z_fluo1,z_fluo2,z_fluo3;
  float  lambda,cutoff,param_c1;
            /* cutoff   : ratio actual angle/critical angle for mirrors*/
            /* param_c1 : param Si C1, including thermal effect (cryo) */
  float  bragg_angle,critic_angle,mirror_angle,mono_angle,equiv_angle;
            /* mirror_angle : angle of incident beam / surface M1      */
            /* equiv_angle  : geom. equiv. angle of M1 with SLOPE = 0. */
  float  br_angle_c1,br_angle_c2,angle_m1,angle_m2;
  float  radius_m1,radius_m2,radius_c2;
  float  l,d,x_c2,x_c2bis,dx,dz,resol;
  float  f1,f2,dist_m1c1,dist_c2m2;
  int    step_H,step_B,step_X,step_Z,nc;
  float  incl_M1,incl_M2,deep_M1,deep_M2;
  struct tm *timeptr;
  char   result[80];
  time_t t;
  FILE *logfile;

  long   error,ltmp,lstep,lstep2,lvoid,status;
  float  fstep;
  char   device[16];
  
  float  H_MASK, L_MASK, z_MASK;  /* hauteur, largeur et altitude du faisceau au niveau du mask du FE              */
  float  H_M1, L_M1, z_M1;        /* hauteur, largeur et altitude du faisceau au niveau de M1                      */
  float  H_BE, L_BE, z_BE;        /* hauteur, largeur et altitude du faisceau au niveau de la fenetre Be           */
  float  H_WBM2, L_WBM2, z_WBM2;        /* hauteur, largeur et altitude du faisceau au niveau du WBM2           */
  float  H_C1, L_C1, z_C1;        /* hauteur, largeur et altitude du faisceau au niveau de C1                      */
  float  H_C2, L_C2, z_C2;        /* hauteur, largeur et altitude du faisceau au niveau de C2                      */
  float  H_M2, L_M2, z_M2;        /* hauteur, largeur et altitude du faisceau au niveau de M2                      */
  float  H_SP, L_SP, z_SP;        /* hauteur, largeur et altitude du faisceau au niveau de l'echantillon (sans KB) */

  float  div_v_source,div_v_mask,div_v_cache,div_v_m1,div_v_min, vert_collected;
  float  footprint_c1,footprint_c2;

  z_mask  = Z_SOURCE;
  z_m1    = Z_SOURCE;
  z_m2    = Z_SAMPLE;
  z_fluo2 = Z_SAMPLE;
  z_fluo3 = Z_SAMPLE;
  lambda  = 1.;
  cutoff  = CUTOFF;
  param_c1= PARAM_SI*(1.+DILAT_C1);
  
  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/

  /* parameters */
  /* ---------- */

  if(strcmp(argv[1],"-n")==0)
   {
    printf("\noutput of offset according to NEMO convention:\n\n");
    printf("z_source           %f\n",Z_SOURCE);
    printf("source_slope       %f\n",SLOPE_SOURCE);
    printf("z_sample           %f\n",Z_SAMPLE);
    printf("m1_incli_offset    %f\n",-1.*ORIG_INCL1);
    printf("m2_incli_offset    %f\n",-1.*ORIG_INCL2);
    printf("moveh_offset       %f\n",ORIG_H);
    printf("mono_enco_offset   %f\n",ORIG_B - ASYM_B);
    printf("utx_offset         %f\n",ORIG_X);
    printf("utz_offset         %f\n",ORIG_Z);
    printf("Si_dilat           %f\n",DILAT_C1);
    printf("\n");
    exit(0);
   }
  
  if(argc<2)
   {
    printf("\nsyntax:\n");
    printf("    geofip wavelength [rejection]\n");
    printf("      compute beamline optic geometry and optionally tune the optic\n");
    printf("        wavelength: wavelength (Angstrom) or atomic symbol (Pt, Se...)\n");
    printf("        rejection : mirror angle / critical angle (<1.) [%.2f]\n",CUTOFF);
    printf("    geofip -i\n");
    printf("      read parameters (beta and alphaM1) and optionally tune the optic\n");
    printf("    geofip -l\n");
    printf("      print parameters in log file\n");
    printf("    geofip -n\n");
    printf("      print offset according to NEMO convention\n\n");
    exit(-1);
   }
        /*-------------------------------------------------------------*/ 
  if(VERBOSE>=2) printf("\n"); 
  if(VERBOSE>=2) printf("-------- Theoretical setting (Si 111, C2 with ribs) ---------\n"); 

  if((strcmp(argv[1],"-i")!=0) && (strcmp(argv[1],"-l")!=0))
   {
    if(argc>=2)
     {
      lambda = 0.;
      if((strcmp(argv[1],"U")==0) || (strcmp(argv[1],"u")==0))
       {lambda = 0.72227; /* LIII edge */}
      if((strcmp(argv[1],"Pb")==0) || (strcmp(argv[1],"PB")==0) || (strcmp(argv[1],"pb")==0)) 
       {lambda = 0.95073; /* LIII edge */}
      if((strcmp(argv[1],"Tl")==0) || (strcmp(argv[1],"TL")==0) || (strcmp(argv[1],"tl")==0)) 
       {lambda = 0.97930; /* LIII edge */}
      if((strcmp(argv[1],"Se")==0) || (strcmp(argv[1],"SE")==0) || (strcmp(argv[1],"se")==0)) 
       {lambda = 0.97974; /*    K edge */}
      if((strcmp(argv[1],"Hg")==0) || (strcmp(argv[1],"HG")==0) || (strcmp(argv[1],"hg")==0)) 
       {lambda = 1.00910; /* LIII edge */}
      if((strcmp(argv[1],"Au")==0) || (strcmp(argv[1],"AU")==0) || (strcmp(argv[1],"au")==0)) 
       {lambda = 1.04000; /* LIII edge */}
      if((strcmp(argv[1],"Pt")==0) || (strcmp(argv[1],"PT")==0) || (strcmp(argv[1],"pt")==0)) 
       {lambda = 1.07230; /* LIII edge */}
      if((strcmp(argv[1],"Ir")==0) || (strcmp(argv[1],"IR")==0) || (strcmp(argv[1],"ir")==0))
       {lambda = 1.10580; /* LIII edge */}
      if((strcmp(argv[1],"Os")==0) || (strcmp(argv[1],"OS")==0) || (strcmp(argv[1],"os")==0))
       {lambda = 1.14080; /* LIII edge */}
      if((strcmp(argv[1],"Re")==0) || (strcmp(argv[1],"RE")==0) || (strcmp(argv[1],"re")==0))
       {lambda = 1.17730; /* LIII edge */}
      if((strcmp(argv[1],"w")==0) || (strcmp(argv[1],"W")==0))
       {lambda = 1.21550; /* LIII edge */}
      if((strcmp(argv[1],"Ta")==0) || (strcmp(argv[1],"TA")==0) || (strcmp(argv[1],"ta")==0))
       {lambda = 1.25530; /* LIII edge */}

      if (lambda<0.5)
       {
        lambda = atof(argv[1]);
        if((lambda<0.4) || (lambda>2.6))
         {
          printf("\n  ERROR: not a valid wavelength\n\n");
          exit(-5);
         }
       }
      if(VERBOSE>=1) printf("lambda = %f Ang",lambda);
     }
    if(argc>=3)
     {
      cutoff = atof(argv[2]);
      if(VERBOSE>=1) printf("       cutoff = %f\n",cutoff);
     }
    else
     {
      if(VERBOSE>=1) printf("\n");
     }
   }
  else
   {
    step_B = lstep;
    /* step_B = atoi(argv[2]); */
    mono_angle = (((step_B*1.0)/RATE_B) + ORIG_B - ASYM_B)*Pi/180.;

    incl_M1 = fstep;
    /* incl_M1= atof(argv[3]); */
    angle_m1 = ((incl_M1/RATE_INCL) - ORIG_INCL1)*Pi/180.;
    mirror_angle = angle_m1 - SLOPE_SOURCE*Pi/180.;
    bragg_angle = mono_angle + 2.*mirror_angle + SLOPE_SOURCE*Pi/180.;
    lambda = param_c1*sin(bragg_angle);
    cutoff = mirror_angle/(CUT_ANGLE*lambda);
    if(VERBOSE>=1) printf("lambda = %f       cutoff = %f\n",lambda,cutoff);
   }
   
  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/

  /* computation: physical parameters */
  /* -------------------------------- */
  
  resol        = lambda/(2.*sin(atan(172.5/219.)/2.));
  br_angle_c1  = asin(lambda/param_c1);
  br_angle_c2  = asin(lambda/PARAM_SI);
  bragg_angle  = br_angle_c1;
  critic_angle = CUT_ANGLE*lambda;
  mirror_angle = cutoff*critic_angle;
  equiv_angle  = mirror_angle + SLOPE_SOURCE*Pi/360.;
  angle_m1     = mirror_angle + SLOPE_SOURCE*Pi/180.;
  angle_m2     = mirror_angle + br_angle_c2 - br_angle_c1 + SLOPE_SOURCE*Pi/360.;
  mono_angle   = bragg_angle - 2.*mirror_angle - SLOPE_SOURCE*Pi/180.;
  radius_m1    = 2.*X_M1/sin(mirror_angle);
  radius_m2    = 2.*(X_SP - X_M2)/sin(angle_m2);
  
  z_be   = (X_BE - X_M1)*tan(2.*equiv_angle) + z_m1;
  z_wbm2 = (X_WBM2 - X_M1)*tan(2.*equiv_angle) + z_m1;
  z_c1   = (X_C1 - X_M1)*tan(2.*equiv_angle) + z_m1;
  l      = (X_M2 - X_M1)/2.;  /*half-distance X between M1 et M2*/
  d      = 2.*l*sin(2.*equiv_angle) + (z_m1 - z_m2)/cos(2.*equiv_angle);  /* distance between mono entering and exiting beams (normal distance) */
  dist_m1c1    = (X_C1 - X_M1)/cos(2.*equiv_angle);
  dist_c2m2    = (2.*l - (X_C1 - X_M1) - d/sin(2.*bragg_angle)*cos(2.*bragg_angle-2.*equiv_angle))/cos(2.*equiv_angle);
  f1           = X_M1 + dist_m1c1 + d/sin(2.*bragg_angle);
  f2           = dist_c2m2 + (X_SP - X_M2);
  radius_c2    = 2.*f1*f2*sin(bragg_angle)/(f1+f2);
  /* looks wrong... : x_c2   = X_C1 + l*sin(2.*equiv_angle)*cos(2.*bragg_angle - 2.*equiv_angle)/(sin(bragg_angle)*cos(bragg_angle));*/
  z_c2   = (x_c2 - X_M2)*tan(2.*angle_m2) + z_m2;
  dx     = d/(2.*sin(bragg_angle)); /* X dist in cristals geometric ref. (Ref rotated by mono_angle)*/
  dz     = d/(2.*cos(bragg_angle)); /* Z dist in cristals geometric ref. (Ref rotated by mono_angle)*/
  x_c2bis = X_C1 + dx * cos(mono_angle) - dz * sin (mono_angle);
  z_fluo1= (X_FLUO1 - X_M2)*tan(2.*angle_m2) + z_m2;
  deep_M1= 1200.*1200./(8.*radius_m1);
  deep_M2= 1200.*1200./(8.*radius_m2);
  
  /* computation: technical parameters */
  /* --------------------------------- */
  
  step_H = (z_c1 - ORIG_H)*RATE_H;
  step_B = (mono_angle*180./Pi - ORIG_B + ASYM_B)*RATE_B;
  step_X = (dx - ORIG_X)*RATE_X;
  step_Z = (dz - ORIG_Z)*RATE_Z;
  incl_M1= (angle_m1*180./Pi + ORIG_INCL1)*RATE_INCL;
  incl_M2= (angle_m2*180./Pi + ORIG_INCL2)*RATE_INCL;
 
  /* output */
  /* ------ */

  if(VERBOSE>=2) printf("  (max resol with Mar345 at 0 deg: %f Ang)\n\n",resol); 
  if(VERBOSE>=2) printf("bragg angle c1 = %6.4g deg\n",br_angle_c1*180./Pi);
  if(VERBOSE>=2) printf("bragg angle c2 = %6.4g deg\n",br_angle_c2*180./Pi);
  if(VERBOSE>=2) printf("mono angle     = %6.4g deg",mono_angle*180./Pi);
  if(VERBOSE>=2) printf("             (%7i pas codeur)\n",step_B);
  if(VERBOSE>=2) printf("critical angle = %6.4g deg\n",critic_angle*180./Pi);
  if(VERBOSE>=2) printf("mirror 1:\n  angle = %6.4g deg",angle_m1*180./Pi);
  if(VERBOSE>=2) printf("                    (%6.4g deg incl. M1)\n",incl_M1);
  if(VERBOSE>=2) printf("  radius= %6.4g km    deep=%4.2g microns\n",radius_m1/1000000.,1000.*deep_M1);
  if(VERBOSE>=2) printf("mirror 2:\n  angle = %6.4g deg",angle_m2*180./Pi);
  if(VERBOSE>=2) printf("                    (%6.4g deg incl. M2)\n",incl_M2);
  if(VERBOSE>=2) printf("  radius= %6.4g km    deep=%4.2g microns\n",radius_m2/1000000.,1000.*deep_M2);
  if(VERBOSE>=2) printf("\n"); 
  /* printf("alt. Be window = %6.4g mm\n",z_be);    */
  /* printf("alt. wires mon.= %6.4g mm\n",z_wbm2);  */
  /* printf("alt. fluor. 1  = %6.4g mm\n",z_fluo1); */
  /* printf("\n");                                  */
  if(VERBOSE>=2) printf("alt. 1st cryst.= %6.4g mm",z_c1);
  if(VERBOSE>=2) printf("              (%7i pas moteur)\n",step_H);
  if(VERBOSE>=2) printf("alt. 2nd cryst.= %6.4g mm\n",z_c2);
  if(VERBOSE>=2) printf("long. dist. between cryst.= %6.4g mm",dx);
  if(VERBOSE>=2) printf("   (%7i pas moteur)\n",step_X);
  if(VERBOSE>=2) printf("vert. dist. between cryst.= %6.4g mm",dz);
  if(VERBOSE>=2) printf("   (%7i pas moteur)\n",step_Z);
  if(VERBOSE>=2) printf("x_c2: %10.10g (mm)\n",x_c2);
  if(VERBOSE>=2) printf("x_c2bis: %10.10g (mm)\n",x_c2bis);

  if(VERBOSE>=2) printf("R 2nd crystal  = %6.4g m\n",radius_c2/1000.);
  if(VERBOSE>=2) printf("-------------------------------------------------------------\n"); 

  if(VERBOSE==0) printf("%6.4g %6.4g %6.4g %6.4g %6.4g\n",lambda,dx,dz,z_c1,radius_c2/1000.);

  /* errors */
  /* ------ */

   if (step_X>0)
   {
    printf("\n  ERROR: this geometry can not be reached (long. dist. between crystals too short)\n\n");
    exit(-1);
   }
  if (step_X>0)
   {
    printf("\n  ERROR: this geometry can not be reached (vert. dist. between crystals too short)\n\n");
    exit(-2);
   }
  
  /* limitation de la taille verticale du faisceau      */
  /* calcul de la divergence verticale, en mrad, FWMH   */
  
  /* divergence verticale de la source, profil gaussien */
  div_v_source = (DIV_V_SOURCE-0.00002)*pow((1.2402/lambda),(-0.66))+0.00002;
  if(VERBOSE>=2) printf("vert divergence of the source  : %6.4g (mrad)\n",1000*div_v_source);

  /* divergence verticale limitee par le mask           */
  div_v_mask = 2.0*atan(APERT_V_MASK/(2.0*X_MASK));
  if(VERBOSE>=2) printf("vert divergence due to the mask: %6.4g (mrad)\n",1000*div_v_mask);

  /* divergence verticale limitee par le cache de M1    */
  div_v_cache = 2.0*atan(APERT_V_CACHE/(2.0*X_CACHE));
  if(VERBOSE>=2) printf("vert divergence due au cache M1: %6.4g (mrad)\n",1000*div_v_cache);

  /* divergence verticale limitee par M1                */
  div_v_m1 = 2.0*atan(LENGTH_MIRRORS * tan(angle_m1)/(2.0*X_M1));
  if(VERBOSE>=2) printf("vert divergence due to M1      : %6.4g (mrad)\n",1000*div_v_m1);

  /* divergence verticale due a l'element le plus limitant */
  div_v_min = fmin(div_v_source,div_v_mask);
  div_v_min = fmin(div_v_min,div_v_cache);
  div_v_min = fmin(div_v_min,div_v_m1);
  vert_collected = div_v_min / div_v_source;
  if(VERBOSE>=2) printf("minimal vert divergence        : %6.4g (mrad)\n",1000*div_v_min);
  if(VERBOSE>=2) printf("part of vertical beam collected: %6.4g (ratio)\n",vert_collected);
  if(VERBOSE>=2) printf("-------------------------------------------------------------\n"); 
  
  /* hauteur, largeur et altitude du faisceau           */
  /* hauteur naturelle de la source                     */
 
  H_MASK = 2.0 * tan(div_v_min/2.0) * X_MASK;
  L_MASK = 2.0 * tan(FAN_SOURCE/2.0) * X_MASK;
  z_MASK = 0;
  /* H_M1 = 2.0 * tan(div_v_min/2.0) * X_M1; */
  H_M1 = 2.0 * tan(div_v_source/2.0) * X_M1;
  L_M1 = 2.0 * X_M1 * tan(FAN_SOURCE/2.0);
  z_M1 = 0;
  H_BE = H_M1;
  L_BE = 2.0 * X_BE * tan(FAN_SOURCE/2.0);
  z_BE = z_be;
  H_WBM2 = H_M1;
  L_WBM2 = 2.0 * X_WBM2 * tan(FAN_SOURCE/2.0);
  z_WBM2 = z_wbm2;
  H_C1 = H_M1;
  L_C1 = 2.0 * X_C1 * tan(FAN_SOURCE/2.0);
  z_C1 = z_c1;
  H_C2 = H_M1;
  L_C2 = 2.0 * x_c2 * tan(FAN_SOURCE/2.0);
  z_C2 = z_c2;
  H_M2 = H_M1;
  L_M2 = L_C2 * (X_SP-X_M2)/(X_SP-x_c2);
  z_M2 = Z_SAMPLE;
  H_SP = 0.0;
  L_SP = 0.0;
  z_SP = Z_SAMPLE;


  if(VERBOSE>=2) printf("Beam height, width and altitude at FE mask: %6.4g %6.4g %6.4g (mm)\n",H_MASK,L_MASK,z_MASK);
  if(VERBOSE>=2) printf("Beam height, width and altitude at M1     : %6.4g %6.4g %6.4g (mm)\n",H_M1,L_M1,z_M1);
  if(VERBOSE>=2) printf("Beam height, width and altitude at BE wind: %6.4g %6.4g %6.4g (mm)\n",H_BE,L_BE,z_BE);
  if(VERBOSE>=2) printf("                 Bottom / top of the beam :%6.4g %6.4g (mm)\n",z_BE-H_BE/2.,z_BE+H_BE/2.);
  if(VERBOSE>=2) printf("Beam height, width and altitude at WBM2: %6.4g %6.4g %6.4g (mm)\n",H_BE,L_BE,z_WBM2);
  if(VERBOSE>=2) printf("Beam height, width and altitude at C1     : %6.4g %6.4g %6.4g (mm)\n",H_C1,L_C1,z_C1);
  if(VERBOSE>=2) printf("Beam height, width and altitude at C2     : %6.4g %6.4g %6.4g (mm)\n",H_C2,L_C2,z_C2);
  if(VERBOSE>=2) printf("Beam height, width and altitude at M2     : %6.4g %6.4g %6.4g (mm)\n",H_M2,L_M2,z_M2);
  if(VERBOSE>=2) printf("Beam height, width and altitude at SP     : %6.4g %6.4g %6.4g (mm)\n",H_SP,L_SP,z_SP);
  if(VERBOSE>=2) printf("-------------------------------------------------------------\n"); 


  /* footprint on crystals                              */
  footprint_c1 = H_C1 / sin(bragg_angle);
  if(VERBOSE>=2) printf("Beam footprint on C1: %6.4g (mm)\n",footprint_c1);
  if(VERBOSE>=2) printf("-------------------------------------------------------------\n"); 
  
  
  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/

  /* save parameters in log file */
  /* --------------------------- */

  if(strcmp(argv[1],"-l")==0)
   {
    logfile = fopen("/database/geofip.log","a");

    time(&t);
    timeptr = localtime(&t);
    strftime (result,80,"%d-%h-%y",timeptr );
    fprintf(logfile,"\n--------------------------------------------------\n");
    fprintf(logfile,"  date: %s ",result);
    strftime (result,80,"%H:%M:%S",timeptr );
    fprintf(logfile,"    time: %s \n",result);
    fprintf(logfile,"  (lambda: %f Ang, cutoff: %f)\n\n",lambda,cutoff);
    printf("\n----------- Actual setting (Si 111, C2 with ribs) -----------\n"); 
    printf("  date: %s ",result);
    printf("    time: %s \n",result);
    printf("  (lambda: %f Ang, cutoff: %f)\n\n",lambda,cutoff);

    fprintf(logfile,"Mirror 1:\n");
    printf("Mirror 1:\n");
    fprintf(logfile,"  elevator 1: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS);
    printf("  elevator 1: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS);
    fprintf(logfile,"  elevator 2: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS);
    printf("  elevator 2: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS);
    fprintf(logfile,"  verins 1-2: %ld steps (angle = %f deg)\n",lstep,(lstep*1.0)/RATE_M1_MOT);
    printf("  verins 1-2: %ld steps (angle = %f deg)\n",lstep,(lstep*1.0)/RATE_M1_MOT);
    fprintf(logfile,"  inclino.  : %f deg (angle = %f)\n",fstep,-1.*(fstep+ORIG_INCL1));
    printf("  inclino.  : %f deg (angle = %f)\n",fstep,-1.*(fstep+ORIG_INCL1));
    fprintf(logfile,"  upstr act.: %ld steps\n",lstep);
    printf("  upstr act.: %ld steps\n",lstep);
    fprintf(logfile,"  downst act: %ld steps\n",lstep);
    printf("  downst act: %ld steps\n",lstep);

    fprintf(logfile,"Monochromator:\n");
    printf("Monochromator:\n");
    fprintf(logfile,"  H    : %ld steps (altitude = %f mm)\n",lstep,ORIG_H+(1.0*lstep)/RATE_H);
    printf("  H    : %ld steps (altitude = %f mm)\n",lstep,ORIG_H+(1.0*lstep)/RATE_H);
    fprintf(logfile,"  beta : %ld steps (angle = %f deg)\n",lstep,(1.0*lstep)/RATE_B_MOT-(ASYM_B));
    printf("  beta : %ld steps (angle = %f deg)\n",lstep,(1.0*lstep)/RATE_B_MOT-(ASYM_B));
    fprintf(logfile,"  coder: %ld steps (angle = %f deg)\n",lstep,ORIG_B-(ASYM_B)+(1.0*lstep)/RATE_B);
    printf("  coder: %ld steps (angle = %f deg)\n",lstep,ORIG_B-(ASYM_B)+(1.0*lstep)/RATE_B);
    fprintf(logfile,"  X    : %ld steps (distance = %f mm)\n",lstep,ORIG_X+(1.0*lstep)/RATE_X);
    printf("  X    : %ld steps (distance = %f mm)\n",lstep,ORIG_X+(1.0*lstep)/RATE_X);
    fprintf(logfile,"  Z    : %ld steps (distance = %f mm)\n",lstep,ORIG_Z+(1.0*lstep)/RATE_Z);
    printf("  Z    : %ld steps (distance = %f mm)\n",lstep,ORIG_Z+(1.0*lstep)/RATE_Z);
    fprintf(logfile,"  khi  : %ld steps\n",lstep);
    printf("  khi  : %ld steps\n",lstep);
    fprintf(logfile,"  omega: %ld steps\n",lstep);
    printf("  omega: %ld steps\n",lstep);
    fprintf(logfile,"  gamma: %ld steps\n",lstep);
    printf("  gamma: %ld steps\n",lstep);
    fprintf(logfile,"  twist: %ld steps\n",lstep);
    printf("  twist: %ld steps\n",lstep);
  
    fprintf(logfile,"Mirror 2:\n");
    printf("Mirror 2:\n");
    fprintf(logfile,"  elevator 1: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS+Z_SAMPLE);
    printf("  elevator 1: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS+Z_SAMPLE);
    fprintf(logfile,"  elevator 2: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS+Z_SAMPLE);
    printf("  elevator 2: %ld steps (altitude = %f mm)\n",lstep,lstep/RATE_Z_MIRRORS+Z_SAMPLE);
    fprintf(logfile,"  verins 1-2: %ld steps (angle = %f deg)\n",lstep,(lstep*1.0)/RATE_M2_MOT);
    printf("  verins 1-2: %ld steps (angle = %f deg)\n",lstep,(lstep*1.0)/RATE_M2_MOT);
    fprintf(logfile,"  inclino.  : %f deg (angle = %f)\n",fstep,-1.*(fstep+ORIG_INCL2));
    printf("  inclino.  : %f deg (angle = %f)\n",fstep,-1.*(fstep+ORIG_INCL2));
    fprintf(logfile,"  upstr act.: %ld steps\n",lstep);
    printf("  upstr act.: %ld steps\n",lstep);
    fprintf(logfile,"  downst act: %ld steps\n",lstep);
    printf("  downst act: %ld steps\n",lstep);

    printf("\n>>>>>>   ACTUAL SETTING SAVED IN /database/geofip.log   <<<<<<\n\n");
    fclose(logfile);
    exit(0);
   }

  /*********************************************************************/
  /*********************************************************************/
  /*********************************************************************/

  exit(0);
  }
