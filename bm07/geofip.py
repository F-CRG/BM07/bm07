#!/bin/python3
# -*- coding: utf-8 -*-
"""
                  GEOFIP
program for geometry modelisation on beamline FIP2 

Created on Thu Nov 21 12:42:54 2019

@author: JL FERRER (C version). Conversion to Python by E. MATHIEU
"""

import sys


##########################################################################
#       ENERGY TABLE FOR EACH COMPONENT
##########################################################################
#    lambda = 12397.64/eV
#    eV = 12397.64/lambda
# http://www.bmsc.washington.edu/scatter/AS_periodic.html
tab_nrj=[
        ("Ag", "K",  0.4859, 25.5140),
        ("Pd", "K",  0.5091, 24.3503),
        ("Rh", "K",  0.5339, 23.2199),
        ("Ru", "K",  0.5605, 22.1172),
        ("U",  "L1", 0.5701, 21.7474),
        ("Tc", "K",  0.5891, 21.0440),
        ("U",  "L2", 0.5918, 20.9476),
        ("Th", "L1", 0.6056, 20.4721),
        ("Mo", "K",  0.6199, 19.9995),
        ("Ac", "L1", 0.6249, 19.8400),
        ("Nb", "K",  0.6530, 18.9856),
        ("Th", "L2", 0.6295, 19.6932),
        ("Ra", "L1", 0.6445, 19.2367),
        ("Ac", "L2", 0.6497, 19.0832),
        ("Fr", "L1", 0.6651, 18.6390),
        ("Ra", "L2", 0.6707, 18.4843),
        ("Rn", "L1", 0.6869, 18.0490),
        ("Zr", "K",  0.6888, 17.9976),
        ("Fr", "L2", 0.6924, 17.9065),
        ("At", "L1", 0.7087, 17.4930),
        ("Rn", "L2", 0.7151, 17.3371),
        ("U",  "L3", 0.7222, 17.1663),
        ("Y",  "K",  0.7276, 17.0384),
        ("Po", "L1", 0.7319, 16.9393),
        ("At", "L2", 0.7386, 16.7847),
        ("Bi", "L1", 0.7565, 16.3875),
        ("Th", "L3", 0.7606, 16.3003),
        ("Po", "L2", 0.7632, 16.2443),
        ("Sr", "K",  0.7698, 16.1046),
        ("Pb", "L1", 0.7817, 15.8608),
        ("Ac", "L3", 0.7812, 15.8710),
        ("Bi", "L2", 0.7891, 15.7111),
        ("Ra", "L3", 0.8027, 15.4444),
        ("Tl", "L1", 0.8078, 15.3467),
        ("Pb", "L2", 0.8156, 15.2000),
        ("Rb", "K",  0.8157, 15.1997),
        ("Fr", "L3", 0.8248, 15.0312),
        ("Hg", "L1", 0.8355, 14.8393),
        ("Tl", "L2", 0.8435, 14.6979),
        ("Rn", "L3", 0.8480, 14.6194),
        ("Au", "L1", 0.8638, 14.3528),
        ("Kr", "K",  0.8654, 14.3256),
        ("At", "L3", 0.8722, 14.2135),
        ("Hg", "L2", 0.8725, 14.2087),
        ("Pt", "L1", 0.8932, 13.8799),
        ("Po", "L3", 0.8975, 13.8138),
        ("Au", "L2", 0.9027, 13.7336),
        ("Br", "K",  0.9201, 13.4737),
        ("Bi", "L3", 0.9239, 13.4186),
        ("Ir", "L1", 0.9239, 13.4185),
        ("Pt", "L2", 0.9341, 13.2726),
        ("Pb", "L3", 0.9511, 13.0352),
        ("Os", "L1", 0.9560, 12.9680),
        ("Ir", "L2", 0.9667, 12.8241),
        ("Tl", "L3", 0.9795, 12.6575),
        ("Se", "K",  0.9794, 12.6578),
        ("Re", "L1", 0.9905, 12.5167),
        ("Os", "L2", 1.0010, 12.3850),
        ("Hg", "L3", 1.0093, 12.2839),
        ("W	", "L1", 1.0246, 12.0998),
        ("Re", "L2", 1.0367, 11.9587),
        ("Au", "L3", 1.0402, 11.9187),
        ("As", "K",  1.0447, 11.8667),
        ("Ta", "L1", 1.0613, 11.6815),
        ("Pt", "L3", 1.0721, 11.5637),
        ("W",  "L2", 1.0739, 11.5440),
        ("Hf", "L1", 1.1000, 11.2707),
        ("Ir", "L3", 1.1054, 11.2152),
        ("Ta", "L2", 1.1133, 11.1361),
        ("Ge", "K",  1.1166, 11.1031),
        ("Lu", "L1", 1.1405, 10.8704),
        ("Os", "L3", 1.1404, 10.8709),
        ("Hf", "L2", 1.1544, 10.7394),
        ("Re", "L3", 1.1768, 10.5353),
        ("Yb", "L1", 1.1823, 10.4864),
        ("Ga", "K",  1.1959, 10.3671),
        ("Lu", "L2", 1.1980, 10.3486),
        ("W",  "L3", 1.2146, 10.2068),
        ("Tm", "L1", 1.2256, 10.1157),
        ("Yb", "L2", 1.2425,  9.9782),
        ("Ta", "L3", 1.2547,  9.8811),
        ("Er", "L1", 1.2714,  9.7513),
        ("Zn", "K",  1.2836,  9.6586),
        ("Tm", "L2", 1.2892,  9.6169),
        ("Hf", "L3", 1.2967,  9.5607),
        ("Ho", "L1", 1.3197,  9.3942),
        ("Er", "L2", 1.3382,  9.2643),
        ("Lu", "L3", 1.3411,  9.2441),
        ("Dy", "L1", 1.3705,  9.0458),
        ("Cu", "K",  1.3808,  8.9789),
        ("Yb", "L3", 1.3862,  8.9436),
        ("Ho", "L2", 1.3902,  8.9178),
        ("Tb", "L1", 1.4237,  8.7080),
        ("Tm", "L3", 1.4336,  8.6480),
        ("Dy", "L2", 1.4448,  8.5806),
        ("Gd", "L1", 1.4802,  8.3756),
        ("Er", "L3", 1.4833,  8.3579),
        ("Ni", "K",  1.4878,  8.3328),
        ("Tb", "L2", 1.5025,  8.2516),
        ("Ho", "L3", 1.5361,  8.0711),
        ("Eu", "L1", 1.5397,  8.0520),
        ("Gd", "L2", 1.5633,  7.9303),
        ("Dy", "L3", 1.5915,  7.7901),
        ("Sm", "L1", 1.6024,  7.7368),
        ("Co", "K",  1.6082,  7.7089),
        ("Eu", "L2", 1.6276,  7.6171),
        ("Tb", "L3", 1.6499,  7.5140),
        ("Pm", "L1", 1.6691,  7.4279),
        ("Sm", "L2", 1.6956,  7.3118),
        ("Gd", "L3", 1.7117,  7.2428),
        ("Nd", "L1", 1.7398,  7.1260),
        ("Fe", "K",  1.7432,  7.1120),
        ("Pm", "L2", 1.7679,  7.0128),
        ("Eu", "L3", 1.7770,  6.9769),
        ("Pr", "L1", 1.8139,  6.8348),
        ("Nd", "L2", 1.8445,  6.7215),
        ("Sm", "L3", 1.8459,  6.7162),
        ("Ce", "L1", 1.8931,  6.5488),
        ("Mn", "K",  1.8960,  6.5390),
        ("Pm", "L3", 1.9193,  6.4593),
        ("Pr", "L2", 1.9250,  6.4404),
        ("La", "L1", 1.9785,  6.2663),
        ("Nd", "L3", 1.9971,  6.2079),
        ("Ce", "L2", 2.0112,  6.1642),
        ("Cr", "K",  2.0700,  5.9892),
        ("Ba", "L1", 2.0701,  5.9888),
        ("Pr", "L3", 2.0786,  5.9643),
        ("La", "L2", 2.1046,  5.8906),
        ("Ce", "L3", 2.1661,  5.7234),
        ("Cs", "L1", 2.1696,  5.7143),
        ("Ba", "L2", 2.2046,  5.6236),
        ("La", "L3", 2.2612,  5.4827),
        ("V",  "K",  2.2685,  5.4651),
        ("Xe", "L1", 2.2736,  5.4528),
        ("Cs", "L2", 2.3133,  5.3594),
        ("Ba", "L3", 2.3628,  5.2470),
        ("I",  "L1", 2.3896,  5.1881),
        ("Xe", "L2", 2.4291,  5.1037),
        ("Cs", "L3", 2.4736,  5.0119),
        ("Ti", "K",  2.4963,  4.9664),
        ("Te", "L1", 2.5101,  4.9392)] 

nrj = type('', (), {})()
for valuelist in tab_nrj:
    setattr(nrj,"_"+str(valuelist[3]).replace(".","_")+"_"+valuelist[0]+"_"+valuelist[1],valuelist[3])
    setattr(nrj,valuelist[0]+"_"+valuelist[1]+"_"+str(valuelist[3]).replace(".","_"),valuelist[3])


##########################################################################
#       GEOFIP FUNCTION
# LAMBDA in angstroem
# VERBOSE >= 2 => full display, 1 => one line output, 0 => silent
##########################################################################
def geofip(LAMBDA_OR_NRJ=0.97974,VERBOSE=2, SLOPE_SOURCE_urad=0):
    import sys
    import math as math
    
    if LAMBDA_OR_NRJ<2.7: #Angstom
        LAMBDA = LAMBDA_OR_NRJ
    else: #kev
        LAMBDA = 12.39764/LAMBDA_OR_NRJ
    #--------------------------------------------------------------------
    #VARIABLES DEFINITION
    #--------------------------------------------------------------------
    DILAT_C1  = -0.00023   # DL/L de c1 with respect to c2   
                           #0.0     at Tc1 = 293 K  
                           #-0.00023 at Tc1 =  75 K 
                           #-0.00024 at Tc1 = 100 K 
    PARAM_SI  = 6.271       # 2d of Si at 20degC, in Ang.
                           #  Si 111: 6.271  
                           #  Si 311: 3.274 
    CUTOFF    = 0.75          # default value of the cutoff
    CUT_ANGLE = 0.00563    # 0.00685 for Pt 
                           # 0.00563 for Rh
    
    
    
    
    
    #--------------------------------------------------------------------             
    #FIP2 PARAMETERS
    #--------------------------------------------------------------------
    X_SOURCE  = 0.00  #everything shifted -2800.00
    X_MASK    = 23437.00  #masque dans le front end 
    X_CACHE   = 28882  #cache a l entree de M1  
    X_M1      = 29603
    X_BE      = 30577
    X_WBM2    = 31040
    X_C1      = 32336
    X_M2      = 35064.5
    X_FLUO1   = 33550
    X_FLUO2   = 36418
    X_FLUO3   = 55088
    X_SP      = 57147.7
    
    #-------------------
    
    
    Z_SOURCE        = 0.0         # altitude first mirror           
    #SLOPE_SOURCE urad is positive when rising from source but in geofip SLOPE_SOURCE sign is reverse
    SLOPE_SOURCE    = -SLOPE_SOURCE_urad*180/math.pi*1E-6          # assuming z magnet = 0     

    FAN_SOURCE      = 0.001663    # div horiz du faisceau, en rad   
    DIV_V_SOURCE    = 0.0002      # div vert du faisceau a 10 keV, en rad FWMH 
    APERT_V_MASK    = 8.0         # ouverture verticale du mask (mm)
    APERT_V_CACHE   = 6.8         # ouverture verticale du cache a l entree de M1 (mm)
    Z_SAMPLE        = -20.        # altitude second mirror          
    LENGTH_MIRRORS  = 1300.0      # longueur des miroirs            
    
    RATE_Z_MIRRORS  = 20000.
    ORIG_INCL1      = 0.0         # -0.0435  0.03344                
    ORIG_INCL2      = 0.0         # last modif. 21 Apr 99           
    RATE_INCL       = -1.
    RATE_M1_MOT     = -25151.     # motor steps per deg.            
    RATE_M2_MOT     = -23057.     # motor steps per deg.            
    ORIG_H          = 0.0         # altitude C1 fin de course bas   
                                        # crystal1 111: -2.86             
                                        # crystal1 311: -3.29             
    RATE_H      = 2000.       # steps per mm                    
    ORIG_B      = 0.0         # origine codeur                  
    ASYM_B      = 0.0         # crystal asymetry                
                                        # crystal 111: 0.087              
                                        # crystal 111: ?                  
    RATE_B      = 20000.      # steps per deg.                  
    RATE_B_MOT  = 4000.       # steps per deg.                  
    ORIG_X      = 134.        # 130.                            
    RATE_X      = -2000.      # steps per mm                    
    ORIG_Z      = 18.0        # gap vertical entre les crystaux 
                                        # crystal2 peigne: 27.5           
                                        # crystal2 311 sur bender: 38.5   
    RATE_Z      = -2000.      # steps per mm           
    
    step_B      = 0.
    fstep       = 0.
    
    '''
    z_mask,z_m1,z_be,z_filw,z_c1,z_c2,z_m2,z_fluo1,z_fluo2,z_fluo3 = float()
    cutoff,param_c1 = float()
    # cutoff   : ratio actual angle/critical angle for mirrors
    # param_c1 : param Si C1, including thermal effect (cryo) 
    bragg_angle,critic_angle,mirror_angle,mono_angle,equiv_angle = float()
    # mirror_angle : angle of incident beam / surface M1      
    # equiv_angle  : geom. equiv. angle of M1 with SLOPE = 0. 
    br_angle_c1,br_angle_c2,angle_m1,angle_m2 = float()
    radius_m1,radius_m2,radius_c2 = float()
    l,d,x_c2,dx,dz,resol = float()
    f1,f2,dist_m1c1,dist_c2m2 = float()
    incl_M1,incl_M2,depth_M1,depth_M2 = float()
    result = str()
    
    error,ltmp,lstep,lstep2,lvoid,status = str()
    fstep = float()
    device = str()
      
    H_MASK, L_MASK, z_MASK = float()  # hauteur, largeur et altitude du faisceau au niveau du mask du FE              
    H_M1, L_M1, z_M1  = float()       # hauteur, largeur et altitude du faisceau au niveau de M1                      
    H_BE, L_BE, z_BE  = float()       # hauteur, largeur et altitude du faisceau au niveau de la fenetre Be           
    H_C1, L_C1, z_C1  = float()       # hauteur, largeur et altitude du faisceau au niveau de C1                      
    H_C2, L_C2, z_C2  = float()       # hauteur, largeur et altitude du faisceau au niveau de C2                      
    H_M2, L_M2, z_M2  = float()       # hauteur, largeur et altitude du faisceau au niveau de M2                      
    H_SP, L_SP, z_SP  = float()       # hauteur, largeur et altitude du faisceau au niveau de l'echantillon (sans KB) 
    
    div_v_source,div_v_mask,div_v_cache,div_v_m1,div_v_min, vert_collected = float()
    footprint_c1,footprint_c2 = float()
    '''
    z_mask  = Z_SOURCE
    z_m1    = Z_SOURCE
    z_m2    = Z_SAMPLE
    z_fluo2 = Z_SAMPLE
    z_fluo3 = Z_SAMPLE
    cutoff  = CUTOFF
    param_c1= PARAM_SI*(1.+DILAT_C1)


    
    #-------------------------------------------------------------------------
    # computation: physical parameters 
    # ------------------------------------------------------------------------
      
    resol        = LAMBDA/(2.*math.sin(math.atan(172.5/219.)/2.))
    br_angle_c1  = math.asin(LAMBDA/param_c1)
    br_angle_c2  = math.asin(LAMBDA/PARAM_SI)
    bragg_angle  = br_angle_c1
    critic_angle = CUT_ANGLE*LAMBDA
    mirror_angle = cutoff*critic_angle
    equiv_angle  = mirror_angle + SLOPE_SOURCE*math.pi/360.
    angle_m1     = mirror_angle + SLOPE_SOURCE*math.pi/180.
    angle_m2     = mirror_angle + br_angle_c2 - br_angle_c1 + SLOPE_SOURCE*math.pi/360.
    mono_angle   = bragg_angle - 2.*mirror_angle - SLOPE_SOURCE*math.pi/180.
    radius_m1    = 2.*X_M1/math.sin(mirror_angle)
    radius_m2    = 2.*(X_SP - X_M2)/math.sin(angle_m2)
    
    z_be   = (X_BE - X_M1)*math.tan(2.*equiv_angle) + z_m1
    z_wbm2 = (X_WBM2 - X_M1)*math.tan(2.*equiv_angle) + z_m1
    z_c1   = (X_C1 - X_M1)*math.tan(2.*equiv_angle) + z_m1
    l      = (X_M2 - X_M1)/2. #half-distance X between M1 et M2
    d      = 2.*l*math.sin(2.*equiv_angle) + (z_m1 - z_m2)/math.cos(2.*equiv_angle) # Normal distance between mono entering and exiting beams
    dist_m1c1    = (X_C1 - X_M1)/math.cos(2.*equiv_angle)
    dist_c2m2    = (2.*l - (X_C1 - X_M1) - d/math.sin(2.*bragg_angle)*math.cos(2.*bragg_angle-2.*equiv_angle))/math.cos(2.*equiv_angle)
    f1           = X_M1 + dist_m1c1 + d/math.sin(2.*bragg_angle)
    f2           = dist_c2m2 + (X_SP - X_M2)
    radius_c2    = 2.*f1*f2*math.sin(bragg_angle)/(f1+f2)

    dx     = d/(2.*math.sin(bragg_angle)) # X dist in cristals geometric ref. (Ref rotated by mono_angle)
    dz     = d/(2.*math.cos(bragg_angle)) # Z dist in cristals geometric ref. (Ref rotated by mono_angle)
    # looks wrong...  x_c2   = X_C1 + l*math.sin(2.*equiv_angle)*math.cos(2.*bragg_angle - 2.*equiv_angle)/(math.sin(bragg_angle)*math.cos(bragg_angle))
    x_c2   =  X_C1 + dx * math.cos(mono_angle) - dz * math.sin (mono_angle)
    z_c2   = (x_c2 - X_M2)*math.tan(2.*angle_m2) + z_m2
    z_fluo1= (X_FLUO1 - X_M2)*math.tan(2.*angle_m2) + z_m2
    depth_M1= 1200.*1200./(8.*radius_m1)
    depth_M2= 1200.*1200./(8.*radius_m2)
    
    # ------------------------------------------------------------------------
    # computation: technical parameters 
    # ------------------------------------------------------------------------
     
    incl_M1= (angle_m1*180./math.pi + ORIG_INCL1)*RATE_INCL
    incl_M2= (angle_m2*180./math.pi + ORIG_INCL2)*RATE_INCL
     
    # output 
    # ------ 
    
    if(VERBOSE>=2): print("(max resol with Mar345 at 0 deg: %f Ang)" % (resol)) 
    if(VERBOSE>=2): print("Slope source =%.1f urad (%f deg)" % (SLOPE_SOURCE_urad,SLOPE_SOURCE)) 
    if(VERBOSE>=2): print("bragg angle c1 = %6.4g deg" % (br_angle_c1*180./math.pi))
    if(VERBOSE>=2): print("bragg angle c2 = %6.4g deg" % (br_angle_c2*180./math.pi))
    if(VERBOSE>=2): print("mono angle     = %6.4g deg" % (mono_angle*180./math.pi))
    if(VERBOSE>=2): print("critical angle = %6.4g deg\n" % (critic_angle*180./math.pi))
    if(VERBOSE>=2): print("mirror 1:  angle = %6.4g deg" % (angle_m1*180./math.pi))
    if(VERBOSE>=2): print("                    (%6.4g deg incl. M1)" % (incl_M1))
    if(VERBOSE>=2): print("           radius= %6.4g km    depth=%4.2g microns\n" % (radius_m1/1000000.,1000.*depth_M1))
    if(VERBOSE>=2): print("mirror 2:  angle = %6.4g deg" % (angle_m2*180./math.pi))
    if(VERBOSE>=2): print("                    (%6.4g deg incl. M2)" % (incl_M2))
    if(VERBOSE>=2): print("           radius= %6.4g km    depth=%4.2g microns\n" % (radius_m2/1000000.,1000.*depth_M2))
    if(VERBOSE>=2): print("") 
    # print("alt. Be window = %6.4g mm" % (z_be)    )
    # print("alt. wires mon.= %6.4g mm" % (z_filw)  )
    # print("alt. fluor. 1  = %6.4g mm" % (z_fluo1) )
    # print("")                                  
    if(VERBOSE>=2): print("alt. 1st cryst.= %6.4g mm" % (z_c1))
    if(VERBOSE>=2): print("alt. 2nd cryst.= %6.4g mm" % (z_c2))
    if(VERBOSE>=2): print("long. dist. between cryst.= %6.4g mm" % (dx))
    if(VERBOSE>=2): print("vert. dist. between cryst.= %6.4g mm" % (dz))
    if(VERBOSE>=2): print("R 2nd crystal  = %6.4g m" % (radius_c2/1000.))
    if(VERBOSE>=2): print("-------------------------------------------------------------") 
    
    if(VERBOSE==1): print("%6.4g %6.4g %6.4g %6.4g %6.4g" % (LAMBDA,dx,dz,z_c1,radius_c2/1000.))
    
      
    # limitation de la taille verticale du faisceau    
    # calcul de la divergence verticale, en mrad, FWMH 
      
    # divergence verticale de la source, profil gaussien 
    div_v_source = (DIV_V_SOURCE-0.00002)*pow((1.2402/LAMBDA),(-0.66))+0.00002
    if(VERBOSE>=2): print("vert divergence of the source  : %6.4g (mrad)" % (1000*div_v_source))
    
    # divergence verticale limitee par le mask     
    div_v_mask = 2.0*math.atan(APERT_V_MASK/(2.0*X_MASK))
    if(VERBOSE>=2): print("vert divergence due to the mask: %6.4g (mrad)" % (1000*div_v_mask))
    
    # divergence verticale limitee par le cache de M1  
    div_v_cache = 2.0*math.atan(APERT_V_CACHE/(2.0*X_CACHE))
    if(VERBOSE>=2): print("vert divergence due au cache M1: %6.4g (mrad)" % (1000*div_v_cache))
    
    # divergence verticale limitee par M1    
    div_v_m1 = 2.0*math.atan(LENGTH_MIRRORS * math.tan(angle_m1)/(2.0*X_M1))
    if(VERBOSE>=2): print("vert divergence due to M1      : %6.4g (mrad)" % (1000*div_v_m1))
    
    # divergence verticale due a l'element le plus limitant 
    div_v_min = min([div_v_source,div_v_mask])
    div_v_min = min([div_v_min,div_v_cache])
    div_v_min = min([div_v_min,div_v_m1])
    vert_collected = div_v_min / div_v_source
    if(VERBOSE>=2): print("minimal vert divergence        : %6.4g (mrad)" % (1000*div_v_min))
    if(VERBOSE>=2): print("part of vertical beam collected: %6.4g (ratio)" % (vert_collected))
    if(VERBOSE>=2): print("-------------------------------------------------------------") 
      
    # hauteur, largeur et altitude du faisceau         
    # hauteur naturelle de la source 
     
    H_MASK = 2.0 * math.tan(div_v_min/2.0) * X_MASK
    L_MASK = 2.0 * math.tan(FAN_SOURCE/2.0) * X_MASK
    z_MASK = 0
    # H_M1 = 2.0 * tan(div_v_min/2.0) * X_M1 
    # H_M1 = 2.0 * tan(div_v_min/2.0) * X_M1; #To be used to get the max height limited by other OE
    H_M1 = 2.0 * math.tan(div_v_source/2.0) * X_M1
    L_M1 = 2.0 * X_M1 * math.tan(FAN_SOURCE/2.0)
    z_M1 = 0
    H_BE = H_M1
    L_BE = 2.0 * X_BE * math.tan(FAN_SOURCE/2.0)
    z_BE = z_be
    H_WBM2 = H_M1
    L_WBM2 = 2.0 * X_WBM2 * math.tan(FAN_SOURCE/2.0)
    z_WBM2 = z_wbm2
    H_C1 = H_M1
    L_C1 = 2.0 * X_C1 * math.tan(FAN_SOURCE/2.0)
    z_C1 = z_c1
    H_C2 = H_M1
    L_C2 = 2.0 * x_c2 * math.tan(FAN_SOURCE/2.0)
    z_C2 = z_c2
    H_M2 = H_M1
    L_M2 = L_C2 * (X_SP-X_M2)/(X_SP-x_c2)
    z_M2 = Z_SAMPLE
    H_SP = 0.0
    L_SP = 0.0
    z_SP = Z_SAMPLE
    
    if(VERBOSE>=2): print("Beam height, width and altitude at FE mask: %6.4g %6.4g %6.4g (mm)" % (H_MASK,L_MASK,z_MASK))
    if(VERBOSE>=2): print("Beam height, width and altitude at M1     : %6.4g %6.4g %6.4g (mm)" % (H_M1,L_M1,z_M1))
    if(VERBOSE>=2): print("Beam height, width and altitude at BE wind: %6.4g %6.4g %6.4g (mm)" % (H_BE,L_BE,z_BE))
    if(VERBOSE>=2): print("          Bottom / top of the beam :%6.4g %6.4g (mm)" % (z_BE-H_BE/2.,z_BE+H_BE/2.))
    if(VERBOSE>=2): print("Beam height, width and altitude at WBM2     : %6.4g %6.4g %6.4g (mm)" % (H_WBM2,L_WBM2,z_WBM2))
    if(VERBOSE>=2): print("Beam height, width and altitude at C1     : %6.4g %6.4g %6.4g (mm)" % (H_C1,L_C1,z_C1))
    if(VERBOSE>=2): print("Beam height, width and altitude at C2     : %6.4g %6.4g %6.4g (mm)" % (H_C2,L_C2,z_C2))
    if(VERBOSE>=2): print("Beam height, width and altitude at M2     : %6.4g %6.4g %6.4g (mm)" % (H_M2,L_M2,z_M2))
    if(VERBOSE>=2): print("Beam height, width and altitude at SP     : %6.4g %6.4g %6.4g (mm)" % (H_SP,L_SP,z_SP))
    if(VERBOSE>=2): print("-------------------------------------------------------------") 
    
    
    # footprint on crystals    
    footprint_c1 = H_C1 / math.sin(bragg_angle)
    if(VERBOSE>=2): print("Beam footprint on C1: %6.4g (mm)" % (footprint_c1))
    if(VERBOSE>=2): print("-------------------------------------------------------------") 
    
    
    #write computed data into a dictionnary and return it
    geofipdata = {
      "lambda": LAMBDA,
      "resol": resol,
      "br_angle_c1": br_angle_c1,
      "br_angle_c2": br_angle_c2,
      "mono_angle": mono_angle,
      "critic_angle": critic_angle, 
      "angle_m1": angle_m1,
      "incl_M1": incl_M1,
      "radius_m1": radius_m1,
      "depth_M1": depth_M1,
      "angle_m2": angle_m2,
      "incl_M2": incl_M2,
      "radius_m2": radius_m2,
      "depth_M2": depth_M2,
      "z_be": z_be,
      "z_wbm2": z_WBM2,
      "z_fluo1": z_fluo1,
      "z_c1": z_c1,
      "z_c2": z_c2,
      "dx": dx,
      "dz": dz,
      "radius_c2": radius_c2,
      "div_v_source": div_v_source,
      "div_v_mask": div_v_mask,
      "div_v_m1": div_v_m1,
      "div_v_min": div_v_min,
      "vert_collected": vert_collected,
      "H_MASK": H_MASK,
      "L_MASK": L_MASK,
      "z_MASK": z_MASK,
      "H_M1": H_M1,
      "L_M1": L_M1,
      "z_M1": z_M1,
      "H_BE": H_BE,
      "L_BE": L_BE,
      "z_BE": z_BE,
      "H_WBM2": H_WBM2,
      "L_WBM2": L_WBM2,
      "z_WBM2": z_WBM2,
      "H_C1": H_C1,
      "L_C1": L_C1,
      "z_C1": z_C1,
      "H_C2": H_C2,
      "L_C2": L_C2,
      "z_C2": z_C2,
      "H_M2": H_M2,
      "L_M2": L_M2,
      "z_M2": z_M2,
      "H_SP": H_SP,
      "L_SP": L_SP,
      "z_SP": z_SP,
      "footprint_c1": footprint_c1,
    }
    return geofipdata
    




##########################################################################
#       MAIN
##########################################################################
def main():  
    import argparse
    parser = argparse.ArgumentParser()    
    parser.add_argument("LAMBDA", metavar='?', help="Provide the wavelength (Å) or the energy (eV or keV) or the element (ex: Se) or the element_shell (ex: Se_k)", type=str)   
    try:
        args = parser.parse_args() # read given arguments
    except:
        print("--------------------------------------------------")
        parser.print_help()
        print("--------------------------------------------------")
        sys.exit(0)
    
    LAMBDA = args.LAMBDA
    if LAMBDA.isdigit():
        # lambda is wavelength or energy
        LAMBDA=float(LAMBDA)
        if LAMBDA > 1000:
            #LAMBDA is eV -> Anglstroem
            LAMBDA /= 12398.56
        elif LAMBDA<2:
            #LAMBDA is Angstroem -> OK
            pass
        else:
            #LAMBDA is keV -> Anglstroem
            LAMBDA /= 12.39856
    else:
        # lambda is an element
        element = LAMBDA.split("_")
        if len(element)==1:
            tab_nrj_search = [i for i in tab_nrj if i[0].upper()==element[0].upper()] 
            if len(tab_nrj_search)!=1:
                print("ERROR You need to specify also the electron shell\nex: W_L1")
                sys.exit(0)
            else:
                LAMBDA=tab_nrj_search[0][3]
        else:
            tab_nrj_search = [i for i in tab_nrj if i[0].upper()==element[0].upper() and i[1].upper()==element[1].upper()] 
            if len(tab_nrj_search)!=1:
                print("ERROR element not found")
                sys.exit(0)
            else:
                LAMBDA=tab_nrj_search[0][3]
                
    print("lambda: %0.5fÅ" % LAMBDA)
    
    geofipdata = geofip(LAMBDA)
    print("*************** GEOFIP COMPUTATION PERFORMED *******************")


if __name__ == "__main__":
    # execute only if run as a script
    main()
    