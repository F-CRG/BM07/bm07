from bliss.controllers.actuator import Actuator
from time import sleep
from gevent.pool import Pool

class Actuator_Multiwago_Interlocked(Actuator):

    def __init__(self, name, config):
        Actuator.__init__(self, name, config)
        self.name = name
        self.controller_state = config.get("controller_state")
        self.in_name = config.get("in_name")
        self.out_name = config.get("out_name")
        self.grob = config.get("grob")
        self.grob_state_ok = config.get("grob_state_ok")
        self.controller_state_interlock = config.get("controller_state_interlock")
        self.controller_state_interlock_ok = config.get("controller_state_interlock_ok")
        self._check = True
        #create dynamic method from in/out name
        setattr(self, self.in_name, self.set_in)
        setattr(self, self.out_name, self.set_out)
        setattr(self, "force_"+self.in_name, self._force_in)
        setattr(self, "force_"+self.out_name, self._force_out)
        setattr(self, "is_"+self.in_name, self._is_in)
        setattr(self, "is_"+self.out_name, self._is_out)

        if name == "detcover" :
            self.pool_stop = False
            pool = Pool(1) 
            pool.spawn(self.thread_survey_interlock_change)

    def __repr__(self):
        txt = ""
        if self.state == "IN":
            txt+= "State: %s\n"%self.in_name.upper()
        elif self.state == "OUT":
            txt+= "State: %s\n"%self.out_name.upper()
        else:
            txt+= "State: %s\n"%self.state
        if self.grob:
            if self.grob.get_state() in self.grob_state_ok:
                txt+="Grob status is good to allow %s movement\n"%self.name
            else:
                txt+="Grob status is NOT good to allow %s movement\n"%self.name
        elif self.controller_state_interlock:
            if (self.controller_state.get(self.controller_state_interlock) == self.controller_state_interlock_ok):
                txt+="%s value is good to allow %s movement\n"%(self.controller_state_interlock,self.name)
            else:
                txt+="%s value is NOT good to allow %s movement\n"%(self.controller_state_interlock,self.name)
        return txt

    def _set_in_out(self,ask_in):
        if (ask_in == False and self._is_in()) or (ask_in == True and self._is_out()):
            if self.grob:
                if not (self.grob.get_state() in self.grob_state_ok):
                    raise Exception(f'Grob is not in the good state : move of %s forbidden'%self.name)
            if self.controller_state_interlock:
                if not(self.controller_state.get(self.controller_state_interlock) == self.controller_state_interlock_ok):
                    raise Exception(f'%s set an interlock : move of %s forbidden'%(self.controller_state_interlock,self.name))
            if ask_in:
                super(Actuator, self).set_in()
            else:
                super(Actuator, self).set_out()


    def set_in(self, timeout=None):
        self._set_in_out(ask_in=True)

    def set_out(self, timeout=None):
        self._set_in_out(ask_in=False)


    def _is_in(self):
        if self.key_in:
            return self.controller_state.get(self.key_in) == 1
        if self.key_out:
            return self.controller_state.get(self.key_out) == 0
        if self.key_cmd:
            return self.controller.get(self.key_cmd) == 1
    
    def _is_out(self):
        if self.key_out:
            return self.controller_state.get(self.key_out) == 1
        if self.key_in:
            return self.controller_state.get(self.key_in) == 0
        if self.key_cmd:
            return self.controller.get(self.key_cmd) == 0

    def _force_in(self):
        super(Actuator,self).set_in()

    def _force_out(self):
        super(Actuator, self).set_out()

    def thread_survey_interlock_change(self):
        oldstate = self.controller_state.get(self.controller_state_interlock)
        while not(self.pool_stop):
            newstate = self.controller_state.get(self.controller_state_interlock)
            if (newstate!= oldstate) and (newstate != self.controller_state_interlock_ok):
                super(Actuator, self).set_in()
            oldstate=newstate
            sleep(2)