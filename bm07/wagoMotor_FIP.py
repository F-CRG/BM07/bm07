from bliss.controllers.motors.wago import WagoMotor
from bliss.controllers.motor import Controller
from bliss.common.utils import rounder
import gevent

class WagoMotor_FIP(WagoMotor, Controller):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
      

    def start_one(self, motion):
        logical_name = motion.axis.config.get("logical_name")
        logical_channel = int(motion.axis.config.get("logical_channel"))
        tolerance = motion.axis.config.get("tolerance", float, 1e-1)
        target_rounded = rounder(tolerance, motion.target_pos)
        self.wago.controller.devwritephys(
            (
                self.wago.controller.devname2key(logical_name),
                logical_channel,
                motion.target_pos,
            )
        )

        with gevent.Timeout(0.5, TimeoutError):
            while rounder(tolerance, self.wago.get(logical_name)) != target_rounded:
                gevent.sleep(0.1)

