#from bliss.controllers.motors.icepap import *
from bliss.comm.tcp import Command
from bliss.controllers.motors.icepap.comm import _command, _ackcommand, _vdata_header
from bliss.common import axis
import gevent
import time

class fastshuttrig(object):
    def __init__(self, name, config, config_objects=None):
        self._config_objects = config_objects
        hostname = config.get("host")
        self.address = config.get("address")
        self.name=name
        self._cnx = Command(hostname, 5000, eol="\n")
        self.acct = config["acct"]
        self.vel = config["vel"]
        self.init()



#****************************  NEW 21 OCT 2024 **************************************   
    def init(self):
         _command(self._cnx, "%d:STOP"%self.address)

    def zero(self):
        _command(self._cnx, "%d:ECAM OFF"%self.address)
        _command(self._cnx, "%d:STOP"%self.address)
        _command(self._cnx, "%d:INFOA LOW NORMAL"%self.address)
    
    def ramp(self, start_pos, end_pos):
        self.init() #usefull to restart the LTRACK mode if any change
        # Define the start_pos and end_pos for level 1 of output 1
        start_step = round(start_pos * 12800) # in encoder steps
        end_step =  round(end_pos* 12800) # in encoder steps
        print("fastshutmot_ramp: start: %d,  end: %d"%(start_step, end_step))
        _command(self._cnx, "%d:INFOA ECAM NORMAL"%self.address) #copy ECAM onto InfoA
        _command(self._cnx, "%d:ECAM ON"%self.address) #start the ECAM signal
        _command(self._cnx, "%d:ECAMDAT ENCIN %d %d 2"%(self.address, start_step, end_step)) #ECAM is On when EncoderIn is between the 2 values (gate)
#****************************  END NEW 21 OCT 2024 **************************************  



    def readpos(self):
        # Read the position
        value = _command(self._cnx, "?ENC ENCIN %d"%self.address)
        return int(value)

    def reset(self):
        self.zero()
        self.set_current_position(0)

    def set_current_position(self, val_deg):
        val_step = round(val_deg*12800)
        _command(self._cnx, "%d:ENC ENCIN %d"%(self.address, val_step))

    def readramp(self):
        # Read the actual ramp range (where the shutter is open)
        ret = _command(self._cnx, "%s:?ECAMDAT"%self.address)
        return ret

    def status(self):
        ret = "ECAM=%s\n"%_command(self._cnx, "%s:?ECAM"%self.address)
        ret += "INFOA=%s\n"%_command(self._cnx, "%s:?INFOA"%self.address)
        ret += "INFOB=%s"%_command(self._cnx, "%s:?INFOB"%self.address)
        print(ret)

    def command(self, cmd):
        return _command(self._cnx, "%d:%s"%(self.address,cmd))


    def force_infoA(self,source):
        _command(self._cnx, "%d:INFOA %s NORMAL"%(self.address,source))

    def force_infoB(self,source):
        _command(self._cnx, "%d:INFOB %s NORMAL"%(self.address,source))

    def set_manual_mode(self):
        self.zero()

    def get_limits(self):
        pass
     
                