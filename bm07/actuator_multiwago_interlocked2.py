from bliss.controllers.actuator import Actuator
from time import sleep
from gevent.pool import Pool


'''
command:
  wago_bliss_obj: $wcd07f
  wago_DO_name: backlight_up_cmd
actuator_inout: {"in":1, "out":0}
in_name: up
out_name: down

#if not state defined -> wago_DO_name state is the state
state:
  wago_bliss_obj:
  wago_DI_name_state_in:
  wago_DI_name_state_out:

interlocks:
  robot:
    bliss_obj: $grob
    ok_states: [1,2]
  wagos:
    bliss_objs: []
    wago_DI_name: []
    ok_states: []
  motors:
    bliss_objs: []
    min_max_values: [(min,max)]
'''

class Actuator_Multiwago_Interlocked(Actuator):

    def __init__(self, name, config):
        self.controller_state = None
        self.grob = None
        self.wagos_interlock = None
        self.motors_interlock = None

        self.__motion_hooks = []

        configActuator= {   "controller" : config["command"]["wago_bliss_obj"],
                            "actuator_cmd" : config["command"]["wago_DO_name"],
                            "actuator_inout": config["actuator_inout"]
                            }
        
        if "state" in config:
            self.controller_state = config["state"]["wago_bliss_obj"]
            configActuator["actuator_state_in"] = config["state"]["wago_DI_name_state_in"]
            configActuator["actuator_state_out"] = config["state"]["wago_DI_name_state_out"]            
        
        Actuator.__init__(self, name, configActuator)
        
        self.name = name
        self.in_name = config.get("in_name")
        self.out_name = config.get("out_name")

        if "interlocks" in config:

            if "robot" in config["interlocks"]:
                self.grob = config["interlocks"]["robot"]["bliss_obj"]
                self.grob_state_ok = config["interlocks"]["robot"]["ok_states"]

            if "wagos" in config["interlocks"]:
                self.wagos_interlock = config["interlocks"]["wagos"]["bliss_objs"]
                self.wagos_interlock_state_ok = config["interlocks"]["wagos"]["ok_states"]
                self.wagos_interlock_DI_name = config["interlocks"]["wagos"]["wago_DI_name"]

            if "motors" in config["interlocks"]:
                self.motors_interlock = config["interlocks"]["motors"]["bliss_objs"]
                self.motors_min_max_ok = config["interlocks"]["motors"]["min_max_values"]

        #import pdb
        #pdb.set_trace()

        self._check = True
        #create dynamic method from in/out name
        setattr(self, self.in_name, self.set_in)
        setattr(self, self.out_name, self.set_out)
        setattr(self, "force_"+self.in_name, self._force_in)
        setattr(self, "force_"+self.out_name, self._force_out)
        setattr(self, "is_"+self.in_name, self._is_in)
        setattr(self, "is_"+self.out_name, self._is_out)

        #thread auto close pour detcover
        if name == "detcover" :
            self.pool_stop = False
            pool = Pool(1) 
            pool.spawn(self.thread_survey_interlock_change)

    @property
    def motion_hooks(self):
        """Registered motion hooks (:obj:`MotionHook`)"""
        return self.__motion_hooks



    def check_robot(self):
        if self.grob:
            if self.grob.get_state() in self.grob_state_ok:
                return "Grob status is good to allow %s movement\n"%self.name, True
            else:
                return "Grob status is NOT good to allow %s movement\n"%self.name, True
        else:
            return "", True

    def check_wagos(self):
        if self.wagos_interlock:
            txt=""
            returnVal = True

            for idx in range(len(self.wagos_interlock)):
                wago = self.wagos_interlock[idx]
                wago_DI_name = self.wagos_interlock_DI_name[idx]
                wago_DI_ok_val = self.wagos_interlock_state_ok[idx]

                if (wago.get(wago_DI_name) == wago_DI_ok_val):
                    txt+="%s value is good to allow %s movement\n"%(wago_DI_name, self.name)
                else:
                    txt+="%s value is NOT good to allow %s movement\n"%(wago_DI_name,self.name)    
                    returnVal=False
            return txt, returnVal
        else:
            return "", True

    def check_motors(self):
        if self.motors_interlock:
            txt=""
            returnVal = True

            for idx in range(len(self.motors_interlock)):
                mot = self.motors_interlock[idx]
                min_max_ok = self.motors_min_max_ok[idx].raw_list
                min_max_ok.sort()

                if (mot.position >= min_max_ok[0] and mot.position <= min_max_ok[1]):
                    txt+="%s position is good to allow %s movement\n"%(mot.name, self.name)
                else:
                    txt+="%s position is NOT good to allow %s movement\n"%(mot.name,self.name)
                    returnVal=False
            return txt, returnVal
        else:
            return "", True

    def __repr__(self):
        txt = ""
        if self.state == "IN":
            txt+= "State: %s\n"%self.in_name.upper()
        elif self.state == "OUT":
            txt+= "State: %s\n"%self.out_name.upper()
        else:
            txt+= "State: %s\n"%self.state

        robot_txt, robot_ok = self.check_robot()
        wagos_txt, wagos_ok = self.check_wagos()
        motors_txt, motors_ok = self.check_motors()
        
        txt += robot_txt + wagos_txt + motors_txt
       
        return txt

    def _set_in_out(self,ask_in):
        if (ask_in == False and self._is_in()) or (ask_in == True and self._is_out()):

            robot_txt, robot_ok = self.check_robot()
            wagos_txt, wagos_ok = self.check_wagos()
            motors_txt, motors_ok = self.check_motors()
            if not robot_ok:
                raise Exception(robot_txt)
            elif not wagos_ok:
                raise Exception(wagos_txt)
            elif not motors_ok:
                raise Exception(motors_txt)

            if ask_in:
                super(Actuator, self).set_in(timeout=10)
            else:
                super(Actuator, self).set_out(timeout=10)

            for hook in self.motion_hooks:
                hook.post_move([])



    def set_in(self, timeout=None):
        self._set_in_out(ask_in=True)

    def set_out(self, timeout=None):
        self._set_in_out(ask_in=False)


    def _is_in(self):
        if self.key_in:
            return self.controller_state.get(self.key_in) == 1
        if self.key_out:
            return self.controller_state.get(self.key_out) == 0
        if self.key_cmd:
            return self.controller.get(self.key_cmd) == 1
    
    def _is_out(self):
        if self.key_out:
            return self.controller_state.get(self.key_out) == 1
        if self.key_in:
            return self.controller_state.get(self.key_in) == 0
        if self.key_cmd:
            return self.controller.get(self.key_cmd) == 0

    def _force_in(self):
        super(Actuator,self).set_in()
        for hook in self.motion_hooks:
            hook.post_move([])

    def _force_out(self):
        super(Actuator, self).set_out()
        for hook in self.motion_hooks:
            hook.post_move([])

    def thread_survey_interlock_change(self):
        wagos_txt, wagos_ok = self.check_wagos()

        oldstate = wagos_ok
        while not(self.pool_stop):
            wagos_txt, wagos_ok = self.check_wagos()
            newstate = wagos_ok
            if (newstate!= oldstate) and (newstate == False):
                super(Actuator, self).set_in()
            oldstate=newstate
            sleep(2)