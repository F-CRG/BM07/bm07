from bliss.comm.tcp import Command
from bliss.controllers.motors.icepap.comm import _command, _ackcommand, _vdata_header
import time
from serial import Serial
import struct
import pycurl
import gevent
from gevent.greenlet import Greenlet



class wago_val_to_influxDB():
    def __init__(self, wagoobj, wagovar, influxstr):
        self.stop = False
        self.wagoobj = wagoobj
        self.wagovar = wagovar
        self.influxstr = influxstr
        print("ici",self.wagoobj.get(self.wagovar))
        
    def start_thread(self):
        greenlet = Greenlet.spawn(self.thread_send_monitor)

    def stop_thread(self):
        self.stop = True


    def thread_send_monitor(self):
        self.stop=False
        while(self.stop == False):
            value = self.wagoobj.get(self.wagovar)
            #print(value)
            datastr=self.influxstr+"="+str(value)
            c = pycurl.Curl()
            c.setopt(c.URL, 'http://vps-e99da8d2.vps.ovh.net:5022/api/v2/write?bucket=FIP_monitor&org=FIP2')
            c.setopt(c.PROXY, 'proxy.esrf.fr:3128')
            c.setopt(c.HTTPHEADER, ["Authorization: Token yP7IXkEYrhkI8FGAWwn3R5-D5C_aD9Ntz7Sw8KPgLiellqy3CZnDfVQz91ceYdiPOU6x8kmPrfJfpI8DBhikFw=="])
            c.setopt(c.SSL_VERIFYPEER, 0)
            c.setopt(c.POST, 1)
            c.setopt(c.CUSTOMREQUEST, 'POST')
            c.setopt(c.POSTFIELDS, datastr)
            c.setopt(c.POSTFIELDSIZE, len(datastr))
            c.perform()
            c.close()
            gevent.sleep(2)
