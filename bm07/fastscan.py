from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.config.static import get_config
from bliss.controllers.tango_attr_as_counter import tango_attr_as_counter
from bliss.common import tango
import termplotlib as tpl
import plotille
import numpy as np
from bliss.shell.standard import umvr, umv
from time import sleep
from math import ceil
import time

def str_tick(min_, max_):
    return '{:.4f}'.format(min_ + (max_ - min_) / 2)

class scan_definition:
    def __init__(self,values):
        self.name = values.get("scan_name")
        self.motor = values.get("motor")
        self.counters = list(map(int, values.get("counters").split(",")))
        self.channel = values.get("channel")
        self.mvr_from = values.get("mvr_from")
        self.mvr_to = values.get("mvr_to")
        self.itm_ms = values.get("integration_time_ms")

    def scan(self):
        self._scan(self)

class fastscan:
    def __init__(self, name, config):
        self._proxy = tango.DeviceProxy(config["pico"])
        self._fluo = config["fluo"]
        self.wago = config["channel_wago"]
        self.relays = config["channel_attr_name"]
        for scan in config["scans"]:
            tempscanobj=scan_definition(scan)
            setattr(tempscanobj, "_scan", self.scan_intmax)
            setattr(self, scan["scan_name"],tempscanobj)

    def start_continuous_mode(self):
        if self._proxy.status() != "In continuous reading..." :
            self._proxy.command_inout("StartContinuous")
            sleep(5)

    def set_channel(self, idx):
        values=[0]*6
        values[idx]=1
        self.wago.set(self.relays,values)

    def get_channel(self):
        return self.wago.get(self.relays)

    def fastreadpico(self, nbpoint, itm=10, channel=3):
        f = open("/tmp/picotest.csv", "w")
        self._proxy.command_inout("Stop")
        self._proxy.ITM=itm
        self._proxy.command_inout("StartN",nbpoint)
        sleep(nbpoint*itm/10000*+2)
        buffer = self._proxy.command_inout("ReadADCBuffer")
        scanresult = []
        for i in range(0,nbpoint*4,4):
            f.write("%d\n"%buffer[i+channel])
            scanresult.append(buffer[i+channel])
        f.close()
        return buffer,scanresult


    def scan_intmax(self, scanobj):
        print("Launch scan for",scanobj.name)
        
        print("Fluo1 down")
        self._fluo.close()

        initialpos = scanobj.motor.position
   
        print("trigger channel set to",scanobj.channel)
        self.set_channel(scanobj.channel)

        print("Prepositionning")
        scanobj.motor.apply_config(reload=True)
        umv(scanobj.motor, initialpos+scanobj.mvr_from)
        
        max_step_per_sec = 1000/(scanobj.itm_ms+2)
        max_velocity = max_step_per_sec / scanobj.motor.steps_per_unit
        velocity = min(max_velocity, scanobj.motor.velocity)
        nbsteptoaccel = ceil(scanobj.motor.steps_per_unit*velocity*velocity/scanobj.motor.acceleration)
        print("Velocity set to", velocity)
        print(nbsteptoaccel, "point at the beging and end will be erase due to accel.")
        scanobj.motor.velocity = velocity

        print("Set pico in intmax mode")
        self._proxy.command_inout("StartIntmax", scanobj.itm_ms*10) #300 = 30ms
        sleep(1)

        print("Scanning")
        umv(scanobj.motor, initialpos+scanobj.mvr_to)
        sleep(1)
        self._proxy.command_inout("Stop")
        sleep(1)
        scanobj.motor.apply_config(reload=True)



        buffer = self._proxy.command_inout("ReadIBuffer")
        self.scanresult = []
        for i in range(nbsteptoaccel*4,len(buffer)-nbsteptoaccel*4,4):
            value=0
            for j in scanobj.counters:
                value+= buffer[i+j]
            self.scanresult.append(value)

        print(self.scanresult)
        maximum = max(self.scanresult)
        index_maximum = np.argmax(self.scanresult)
        avg = np.mean(self.scanresult)

        x = np.linspace(scanobj.mvr_from+initialpos, scanobj.mvr_to+initialpos, len(self.scanresult)+nbsteptoaccel*2)
        x = x[nbsteptoaccel:len(x)-nbsteptoaccel]

        print("maximum =",maximum)
        # print("avg =",avg)
        print("indexmax =", index_maximum)
        # print("len x =", len(x))
        # print("len scanresult =", len(self.scanresult))
        print(x)
        if (maximum-avg)/avg > 0 : #marche pas
            print("Go to maximum",x[index_maximum])
            maximum_to_go = x[index_maximum]
        else:
            print("Return to initial position and speed")
            maximum_to_go = x[int(len(x)/2)]

        #Termplotlib
        #fig = tpl.figure()
        #fig.plot(x, self.scanresult, width=300, height=40)
        #fig.show()
        
        #plotille
        fig = plotille.Figure()
        fig.width = 50
        fig.height = 20
        fig.plot(x, self.scanresult)
        fig.scatter(x, self.scanresult, lc='green', marker='x')
        fig.set_x_limits(min_=x.min(), max_=x.max())
        fig.x_ticks_fkt = str_tick
        print(fig.show(legend=False))



        umv(scanobj.motor, maximum_to_go)


    def derivative(self,y,x):
        var_diff=[]
        var_diff.append(np.gradient(y,x))
        return var_diff

    
