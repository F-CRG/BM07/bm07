from bliss.config import static as static_config
import requests

class ippowerplug(object):

    def __init__(self, name, config, config_objects=None):
        self._config_objects = config_objects
        self.ip = config.get("ip")
        self.auth = requests.auth.HTTPBasicAuth(config.get("login"),config.get("pass"))
        self.url = "http://%s/set.cmd"%self.ip
        self.name = name
        
    def __repr__(self):
        txt= "ip: %s\n"%self.ip
        if self.is_on():
            txt+= "ON\n"
        else:
            txt+= "OFF\n"
        return txt

    def on(self, duration=None):
        if duration:
            requests.get(self.url+"?cmd=setpower+p61=1+p61n=0+t61=%d"%duration, auth= self.auth)
        else:
            requests.get(self.url+"?cmd=setpower+p61=1", auth= self.auth)

    def off(self, duration=None):
        if duration:
            requests.get(self.url+"?cmd=setpower+p61=0+p61n=1+t61=%d"%duration, auth= self.auth)
        else:
            requests.get(self.url+"?cmd=setpower+p61=0", auth= self.auth)

    def is_on(self):
        resp = requests.get(self.url+"?cmd=getpower", auth= self.auth)
        #print(resp.text)
        return "p61=1" in resp.text