from bm07.edge_scan import pico_moyenne
from bliss.config.static import get_config
from bliss.shell.cli.user_dialog import UserInput, UserCheckBox, UserChoice, BlissWizard
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss import global_map
from bliss.setup_globals import *
from matplotlib.gridspec import GridSpec
from bliss.common.standard import mv, mvr
import numpy as np
import datetime
from .geofip import geofip, tab_nrj, nrj
from bliss.physics import trajectory
from bliss.common import axis
from bliss.common.motor_group import TrajectoryGroup
import time
import math
import datetime
from PIL import Image
import requests
import termplotlib as tpl
import urllib.request
import scipy.optimize as opt
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy.interpolate import interp1d
import enlighten
import gevent.event
import gevent


#********************************************************************************
# OPTICAL SCRIPTS
#********************************************************************************



def beammonitorIn():
    print("Beam Monitor go to 0mm")
    umv(beammonitor,0)

def absAluminiumIn():
    print("Beam Monitor go to -66,7mm")
    umv(beammonitor,-66.7)

def absCarbonIn():
    print("Beam Monitor go to -33,8mm")
    umv(beammonitor,-33.8)

def scanM1Bend():
    with open("/tmp_14_days/fip/scanM1Bend.csv","a+") as f:
        f.write("time,actam1,actav1,diode1,diode2,Mach_current\n")
        for bend in range(300000,50000,-20000):
            maintenant = datetime.datetime.now().strftime("%H:%M:%S")
            # if idx%200==0:
            #     fastscan.gamma2_short.scan()
            actam1.move(bend, wait=False)
            actav1.move(bend, wait=False)
            while actam1.is_moving or actav1.is_moving:
                sleep(1)
            bend1pos = actam1.position
            bend2pos = actav1.position
            # print("startT")
            # fastscan._proxy.command_inout("StartT", 10000)
            # sleep(12)
            # print("ReadIbuffer")
            # dioderead = fastscan._proxy.command_inout("ReadIBuffer")
            # f.write("%s,%d,%d,%f,%f\n"%(maintenant,bend1pos,bend2pos,dioderead[2]/dioderead[0],dioderead[3]/dioderead[0]))
            for i in range(0,30):
                current = SR_Current.value
                f.write("%s,%d,%d,%f,%f,%f\n"%(maintenant,bend1pos,bend2pos,OH_I1.value,OH_I2.value,current))
                sleep(0.5)
            f.flush()

def scanSl0(relMin, relMax, step, pics=False):
    sl0vtposArr = []
    PixelSumArr = []
    sl0vtposInit = sl0vt.position
    path = "/tmp/pictures/" #"/users/opd07/Pictures/"
    PixelSumArr = []
    for relpos in np.arange(relMin, relMax, step):
        sl0vt.move(sl0vtposInit+relpos, wait=True)
        sleep(0.2)
        im = Image.open(requests.get("http://bm07recorder.esrf.fr:7011/jpg/image.jpg", stream=True).raw)
        a=np.asarray(im)
        pixelsvalue = a.sum() / 1E5
        PixelSumArr.append(pixelsvalue)
        sl0vtposArr.append(sl0vt.position)
        if pics == True:
            saveMonitorsImage("1000","sl0vt" + str(sl0vt.position))
    sl0vt.move(sl0vtposInit, wait=True) 
       
    fig = tpl.figure()
    fig.plot(sl0vtposArr,PixelSumArr, width=300, height=40)
    fig.show()
    
 
def scanMovehM1(relMin, relMax, step):
    M1posInit = m1up.position
    MovehposInit = moveh.position
    
    M1posArr = []
    PixelSumArr = []
    for relpos in np.append(np.arange(relMin,relMax,step),[relMax]):
        m1up.move(M1posInit+relpos, wait=False)
        moveh.move(MovehposInit+relpos, wait=False)
        while moveh.is_moving or m1up.is_moving:
            sleep(1)
        sleep(1)
        im = Image.open(requests.get("http://bm07recorder.esrf.fr:7010/jpg/image.jpg", stream=True).raw)
        a=np.asarray(im)
        pixelsvalue = a.sum() / 1E5
        print(m1up.position, moveh.position, pixelsvalue)
        M1posArr.append(m1up.position)
        PixelSumArr.append(pixelsvalue)
        
    m1up.move(M1posInit, wait=False)
    moveh.move(MovehposInit, wait=False)  
    while moveh.is_moving or m1up.is_moving:
        sleep(1)
    
    fig = tpl.figure()
    fig.plot(M1posArr,PixelSumArr, width=300, height=40)
    fig.show()

#records fluo3 pictures while bending M2. Useful to have a dynamic reference of bending positions.
def suivi_bend_M2(Min, Max, nbPoint):
    saveMonitorsImage("0001","ref")
    m2BendPos=[]
    for pos in np.linspace(Min, Max, nbPoint):
        m2bend.move(pos, wait=True)
        for i in range(0,10):
            saveMonitorsImage("0001","m2bend_%f_%f"%(pos,i))
            time.sleep(0.3)
        m2BendPos.append(pos)
    print(m2BendPos)

#records fluo3 picture while vertically moving M2. Used to identify if M2 is cylindrically bended or flat. (note: this sis not really working).
def mesure_bend_M2(Min, Max, nbPoint):
    saveMonitorsImage("0001","ref")
    m2upPos=[]
    position=[]
    for pos in np.linspace(Min, Max, nbPoint):
        m2up.move(pos, wait=True)
        value=0
        nbpoint=0
        for i in range(0,10):
            saveMonitorsImage("0001","m2up_%f"%pos)
            urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7006/jpg/image.jpg", "/tmp/pictures/test.jpg")
            resu = analyseImg()
            if resu != -1:
                print(resu)
                value+=resu
                nbpoint+=1
            time.sleep(0.3)
        if nbpoint!=0:
            resu = value/float(nbpoint)
            print(pos, resu)
            m2upPos.append(pos)
            position.append(resu)
    print(m2upPos)
    print(position)
        
        
def mesure_bend_M2_sans_M2(Min, Max, nbPoint):
    saveMonitorsImage("0001","ref")
    utzPos=[]
    position=[]
    for pos in np.linspace(Min, Max, nbPoint):
        mv(utz,pos)
        #fastscan.gamma2_short.scan()
        value=0
        nbpoint=0
        for i in range(0,5):
            saveMonitorsImage("0001","utz_%f"%pos)
            urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7006/jpg/image.jpg", "/tmp/pictures/test.jpg")
            resu = analyseImg()
            if resu != -1:
                print(resu)
                value+=resu
                nbpoint+=1
            time.sleep(0.3)
        if nbpoint!=0:
            resu = value/float(nbpoint)
            print(pos, resu)
            utzPos.append(pos)
            position.append(resu)
    print(utzPos)
    print(position)



###########################################################################
# scan in energy the response of the second crystal = rocking curve:
def rocking_curve_C2(deltaGamma2=0.05, nbPoints=20): #deltaGamma2 in deg of gamma2
    gamma2Start = gamma2.position
    gamma2Steps =  np.linspace(gamma2Start-deltaGamma2/2,gamma2Start+deltaGamma2/2,nbPoints)
    gamma2Val = []

    with open("/tmp_14_days/bm07/c2scan.csv","a+") as f:
        maintenant = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M')
        f.write("date, y, z, Dstop\n")
        for gamma2Pos in gamma2Steps:
            umv(gamma2,gamma2Pos)
            sleep(1)
            # average the diodes after mono:
            Flux = 0
            totalDiode = 0
            for j in range(0,10):
                totalDiode += OH_I1.value+OH_I2.value
                sleep(0.05)
            Flux = totalDiode/10.
            gamma2Val.append(Flux)
            f.write("%s,%.5f,%.2f\n" %(maintenant,gamma2Pos,Flux))
            #print ("Gamma2= " + str(gamma2Pos) + ",  Flux= " + str(Flux))
        umv(gamma2,gamma2Start)
        print (gamma2Steps)
        print (gamma2Val)
        f.close()




def mesure_beam_oscill_fluo3(nbimages,Name):
    positions = []
    for i in range(0,nbimages):
        #saveMonitorsImage("0001","utz_%f"%pos)
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7006/jpg/image.jpg", "/tmp/pictures/test.jpg")
    picpos = analyseImg()      
    positions.append(picpos)
    print (picpos)
    var = np.var(positions)
    print("Vertical Pic Position Variance = %f"%var)


def bend_debend(steps,nbloops): #loop to bend and debend m2. Used for focalisation tests
    for i in range(1,nbloops):
        umvr(m2bend,steps)
        sleep(10)
        umvr(m2bend,-steps)
        sleep(10)


def saveMonitorsImage(cam,string,path): #cam in binary string 000000 to 111111, each character for a camera: wires, fluo1, fluo2, fluo3, OH, sample
    #path = "/data/bm07/inhouse/FILM_DE_FIP/"
    now=datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    if cam[0:1] == "1": # download wire camera picture
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7011/jpg/image.jpg", path + now + "_wires_" + string + ".jpg")
    if cam[1:2] == "1": # download fluo1 camera picture
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7010/jpg/image.jpg", path + now + "_fluo1_" + string + ".jpg")
    if cam[2:3] == "1": # download fluo2 camera picture
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7009/jpg/image.jpg", path + now + "_fluo2_" + string + ".jpg")
    if cam[3:4] == "1": # download fluo3 camera picture
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7006/jpg/image.jpg", path + now + "_fluo3_" + string + ".jpg")
    if cam[4:5] == "1": # download OH camera picture
        urllib.request.urlretrieve("http://bm07recorder.esrf.fr:7008/jpg/image.jpg", path + now + "_OH_" + string + ".jpg")
    if cam[5:6] == "1": # download sample picture
        urllib.request.urlretrieve("http://bm07sample.esrf.fr/jpg/image.jpg", path + now + "_sample_" + string + ".jpg")
    sleep(0.5)


def film_FIP(temps_en_deux_image_sec, duree_total_enregis_sec, path):
    for i in range(0,int(duree_total_enregis_sec/temps_en_deux_image_sec)):
        #safshut.open()
        #frontend.open()
        #fluo3.open()
        #sleep(2)
        saveMonitorsImage("000001","sample",path)
        print('saving image '+str(i))
        #sleep(2)
        #fluo3.close()
        sleep(temps_en_deux_image_sec)

def scanMovehM1Sl0(relMin, relMax, step, pics=False, sourceAngle_uRad=0):
    M1posInit = m1up.position
    MovehposInit = moveh.position
    sl0vtposInit = sl0vt.position
    UtxInit = utx.position
    UtzInit = utz.position
    path = "/tmp/pictures/" #"/users/opd07/Pictures/"
    
    M1posArr = []
    PixelSumArr = []
    for relpos in np.append(np.arange(relMin,relMax,step),[relMax]):

        deltaUtxUtz = delta_utx_utz_fixed_height(relpos, sourceAngle_uRad)
        utx.move(UtxInit+deltaUtxUtz[0], wait=False)
        utz.move(UtzInit+deltaUtxUtz[1], wait=False)
        m1up.move(M1posInit+relpos, wait=False)
        moveh.move(MovehposInit+relpos, wait=False)
        sl0vt.move(sl0vtposInit+relpos, wait=False)
        while moveh.is_moving or m1up.is_moving or sl0vt.is_moving or utx.is_moving or utz.is_moving:
            sleep(0.2)
        sleep(0.2)
        fastscan.gamma2_short.scan()
        PixelSumArr.append(max(fastscan.scanresult))
        # if analyse fluo picture (and/or record it)
        #im = Image.open(requests.get("http://bm07recorder.esrf.fr:7010/jpg/image.jpg", stream=True).raw)
        if pics == True:
            saveMonitorsImage("1100","Z" + str(m1up.position))
            
        #a=np.asarray(im)
        #pixelsvalue = a.sum() / 1E5
        #print(m1up.position, moveh.position, pixelsvalue)
        M1posArr.append(m1up.position)
        #PixelSumArr.append(pixelsvalue)
        #totalDiode = 0
        #for j in range(0,5):
        #    totalDiode += OH_I1.value+OH_I2.value
        #    sleep(0.05)
        #PixelSumArr.append(totalDiode/5.)

        
        
    m1up.move(M1posInit, wait=False)
    moveh.move(MovehposInit, wait=False)  
    sl0vt.move(sl0vtposInit, wait=False)  
    utx.move(UtxInit, wait=False)
    utz.move(UtzInit, wait=False)
    while moveh.is_moving or m1up.is_moving or sl0vt.is_moving or utx.is_moving or utz.is_moving:
        sleep(0.5)
    
    fig = tpl.figure()
    fig.plot(M1posArr,PixelSumArr, width=300, height=40)
    fig.show()
 


def scanMoveh(relMin, relMax, step):
    MovehposInit = moveh.position
    UtxInit = utx.position
    UtzInit = utz.position
    
    MovehposArr = []
    PixelSumArr = []
    for relpos in np.append(np.arange(relMin,relMax,step),[relMax]):
        deltaUtxUtz = delta_utx_utz_fixed_height(relpos)
        utx.move(UtxInit+deltaUtxUtz[0], wait=False)
        utz.move(UtzInit+deltaUtxUtz[1], wait=False)
        moveh.move(MovehposInit+relpos, wait=False)
        while moveh.is_moving or utx.is_moving or utz.is_moving:
            sleep(0.5)
        sleep(0.5)
        #im = Image.open(requests.get("http://bm07recorder.esrf.fr:7010/jpg/image.jpg", stream=True).raw)
        #a=np.asarray(im)
        #pixelsvalue = a.sum() / 1E5
        #print(moveh.position, pixelsvalue)
        MovehposArr.append(moveh.position)
        #PixelSumArr.append(pixelsvalue)
        totalDiode = 0
        for j in range(0,10):
            totalDiode += OH_I1.value+OH_I2.value
            sleep(0.05)
        PixelSumArr.append(totalDiode/10.)

    moveh.move(MovehposInit, wait=False)
    utx.move(UtxInit, wait=False)
    utz.move(UtzInit, wait=False)
    while moveh.is_moving or utx.is_moving or utz.is_moving:
        sleep(0.5)
    
    fig = tpl.figure()
    fig.plot(MovehposArr,PixelSumArr, width=300, height=40)
    fig.show()



# to keep the beam on the same position on dmono during moveh scanning (->C2 position is adjusted)
def delta_utx_utz_fixed_height(relMoveh, sourceAngle_uRad=0):
    braggrad = deg2rad(bragg.position)+sourceAngle_uRad*1E-6
    betarad = deg2rad(beta.position)
    L2 = relMoveh / ( ( tan (braggrad - betarad ) + tan (braggrad + betarad ) ) * cos ( braggrad + betarad ) )
    delta_utz = L2 *sin(braggrad)
    delta_utx = L2 *cos(braggrad)
    return (delta_utx, delta_utz)
 

def md2_wakeup():
    motlist = [omega, kappa, phi, x, y, z, xomega, zomega]
    for sign in [1,-1]:
        for mot in motlist:
            mot.move(mot.position+sign*0.1, wait=False)
        while any(mot.is_moving  for mot in motlist):
            sleep(0.1)

#Oscillate the MD2 during a duration
def md2_oscillation(total_move_duration_in_s=60,velocity_deg_per_s=10,angle=360):
    original_velocity = omega.velocity
    omega.velocity = velocity_deg_per_s
    startingtime = time.time()
    while ((time.time()-startingtime) < total_move_duration_in_s):
        print ("Omega oscillation. Remaining time (s) = " + str(total_move_duration_in_s - int(time.time()-startingtime)))
        omega.move(omega.position+angle, wait=True)
        omega.move(omega.position-angle, wait=True)
    omega.velocity = original_velocity


def flux_from_diode_val(diode_mA, NRJ_keV = 12.65, Si_um = 300, Air_cm = 6, Al_um = 30):
    Si_density = 2.35 #g/cm3
    Air_density = 1.23E-3 #g/cm3
    Al_density = 2.7 #g/cm3
    
    CoefSI = 1.6E-19*NRJ_keV*1000*(1-exp(-exp(0.0133*(log(NRJ_keV))**3-0.1934*(log(NRJ_keV))**2-2.2757*(log(NRJ_keV))+9.6049)*Si_um*0.0001*Si_density))
    CoefAir = exp(-exp(0.009*(log(NRJ_keV))**3-0.1332*(log(NRJ_keV))**2-2.6392*(log(NRJ_keV))+8.2371)*Air_cm*Air_density)
    CoefAl = exp(-exp(0.1376*(log(NRJ_keV))**3-1.2533*(log(NRJ_keV))**2-2.2957*(log(NRJ_keV))-0.9172)*Al_um*0.0001*Al_density)
    
    flux = 3.62*diode_mA*0.001/CoefSI/CoefAir/CoefAl
    
    print("%e ph.s"%flux)
    return "{:e}".format(flux)


def nrjcal(nelem):    
#    local pas_mot;
#    pas_mot=1./4000.;
 
    import time
    energyTab = {
	"AU": 11.9208,
    "BR": 13.4698,
    "CO": 7.7093,
    "CU": 8.9800, # pmax =  0.015 , pmx2 =  0.05 ,
    "FE": 7.1109, #pmax =  0.015 ,  pmx2 =  0.04 ,
    "GD": 7.2429,
    "HG": 12.2858,
    "HO": 8.0672,
    "NI": 8.3314, #pmax =  0.005 , pmx2 =  0.04 ,
    "PB": 13.0401,
    "PT": 11.5617, #pmax =  0.03 , pmx2 =  0.08 ,
    "RE": 10.5306,
    "SE": 12.6540,
    "TA": 9.8762,
    "U": 17.1660, #pmax =  0.005 , pmx2 =  0.017 ,
    "ZN": 9.6600,
    "YB": 8.9436,
    "EU": 6.9830,
    "TB": 7.5151,
    "RB": 15.2018,
    "KR": 14.3239
	}
    
    
    wavelengthTab = {}
    for key,value in energyTab.items():
        wavelengthTab.update({key:(6.62606957e-34*299792458/1.602176565E-19)/1.00001501e-7/value})

    nitx = 5 
    # valeurs generales :
    pmax =  0.02 
    pmx2 =  0.06 
    # voir aussi valeurs particulieres pour U, Ni, Fe
    
    nbpas=150
    
    #parametres de mscan:
    #moteur, nb.de lissages, 1/2 domaine de scan, moniteur 1 2 ou 1/2), fichier

    print("\n\n********************************************\n")
    print("***             nrjcal                  ***\n")
    print("***  reglage automatique du zero mono   ***\n") 
    print("********************************************\n\n") 
    print("\nfiltres disponibles AU BR CU CO FE GD NI PB PT SE TA U ZN\n\n")
  
    ene=-1
    duree=1.

    #epai_att1_initiale=att_readwidth();   A refaire en bliss
  
    #parametres concernant l element#


    targetEnergy=energyTab[nelem]
    targetWavelength=wavelengthTab[nelem]
    if targetEnergy==0:
      print("\nUnknown filter\n")
      print("\n**** nrjcal stopped ****\n")
      print("\n")
  
    print("************************\n")
    print(" seuil absorption %s\n",nelem)
    print("************************\n")
    print("\nselected lambda %8.5f energy %8.5f\n\n", targetWavelength, targetEnergy)
    print("Filter is assumed to be in place...\n") 
    
    
    """ Calcul des pas de rotation mono pour 150 points. Remplacer par procédure BLISS
    
    #position angulaire theorique du seuil
    monot=Wavelength_to_MonoAngle(wavelengthTab[nelem]);
  
    #position initiale
    posi=mono_angle_enco();
    lami=getwl();
 
    #l increment de position est fonction de l angle
    dpos=0.000075*monot;
    if(dpos<pas_mot)
    {
      dpos=pas_mot;
    }else{
      ipos = int(dpos/pas_mot) ;
      epos = 1. - pas_mot*ipos/dpos ;
      if(epos<0.5) ipos=ipos+1.;
      dpos = ipos*pas_mot ;
    }

    local nbpas, dmono, mono1, mono2, ecar1, ecar2;
    #nbpas=70.;
    nbpas=150.;
    dmono=0.5*nbpas*dpos;
    mono1=monot-dmono;
    mono2=monot+dmono;
    printf("scan %10.5f %10.5f %10.5f %8.0f \n\n", mono1, monot, mono2, nbpas);
    ecar1=fabs(posi-mono1);
    ecar2=fabs(posi-mono2);
    ecart=ecar1;
    if(ecar2>ecar1) ecart=ecar2 ;
    if(ecart>1.) 
    {
      printf("Mono position initial = %f\n", posi);
      printf("ecart de position trop grand - stop \n\n");
      return ;
    }
    """
  
    #scan en energie

    #positionnement initial
    """
    pos=mono1; #donner position départ du scan
    mono_mva(pos) ;
    pos=mono_getpos();
    """

    #comptage et test initial
    dstop_in() ######### Corriger pour BLISS  #########
    bstop_down() ######### Corriger pour BLISS  #########
    #att_setwidth(calcAttValTh(wavelengthTab[nelem])+1.2)
    #calc_diode_bg(3.);
    pico_read_diode() ######### Corriger pour BLISS  #########
    print("dstop=%d\n", diode["dstop"])
    print("dmono=%d\n", diode["dmono"])
    if diode["dstop"]<10 :
      print("CRITICAL ERROR !\nDSTOP moniteur very low - pas de faisceau ?\nEXIT\n")
      bstop_in()
      dstop_out()
      #att_setwidth(epai_att1_initiale);
      #mono_mva(posi);
      #return ;
    
    initCountDstop=diode["dstop"]
 
    dataTab = [[],[],[]]
    #Fait le scan en position sur mono et mesure les diodes d0 et dstop pour calculer leur rapport
#    data_grp(1, 0, 0);
#    data_grp(1, nbpas, 2)
    for nb in nbpas:
      pos+=dpos; # ! gérer index position
      beam_set()#mvr mono dpos
      pico_read_diode()
      dataTab[0].append(mono_angle_enco())
      #dataTab[0][nb]=mono_angle_enco()+dpos*nb;
      dataTab[1].append(diode["dstop"])
      dataTab[2].append(diode["dmono"])
      if(diode["dmono"]!=0):
          rap=diode["dstop"]/diode["dmono"]
      else:
          rap=1
      if nb==0 : ref=10000./rap
      rap=ref*rap 
      dataTab[3][nb]=rap  
#      data_put(1, nb, 0, dataTab[0][nb])
#      data_put(1, nb, 1, dataTab[3][nb])
      print("mono angle %f  --> dstop=%f dmono=%f rapport=%f\n", dataTab[0][nb], diode["dstop"], diode["dmono"], dataTab[3][nb])
      #array_plot(1)  créer graphe

    #calcul fenetre de lissage
    hamminWin = []
    hamminWinSum=0.
    for ii in range(0,6,1): 
      hamminWin[ii]=0.54-0.46*cos(2*math.pi*ii/7.)
      hamminWinSum+=hamminWin[ii]

    #double application fenetre de lissage
#    data_grp(1, 0, 0)
#    data_grp(1, nbpas, 2)
    for nb in range (0,nbpas,1):
      dataTabLiss[nb]=0
      nbptLiss=0
      for ii in range(0,6,1):
        if nb-3+ii>=0 and nb-3+ii<nbpas :
          dataTabLiss[nb]+=hamminWin[ii]*dataTab[3][nb-3+ii]
          nbptLiss+=hamminWin[ii]
      if nbptLiss != 0 : dataTabLiss[nb]/=nbptLiss
#      data_put(1, nb, 0, dataTab[0][nb])
#      data_put(1, nb, 1, dataTabLiss[nb])
#      array_plot(1);  Ajouter graphe
    

    #calcul de la derivée
    #on commence a 6 et on fini 6 avant la fin parce que les lissages sont mauvais au bord.

    for nb in range(6,nbpas-6,1) : 
       Yderive[nb-6]=(dataTabLiss[nb]-dataTabLiss[nb-1])/(dataTab[0][nb]-dataTab[0][nb-1])
       Xderive[nb-6]=(dataTab[0][nb]+dataTab[0][nb-1])/2
    #recuper la position du maximum de la derive
    PosMax= Xderive[Yderive.index(max(Yderive))]
    print("Mono angle where the transition takes place: %f\n", PosMax)

    
    ##########################
    #Corriger le mono du delta
    ########################
    

    dstop_out() ########## Corriger pour BLISS  #########





def opensh():
    fastshutmot.open_manual()

def closesh():
    fastshutmot.close_manual()


def intmax():
    detcover.force_close()
    fluo3.close()
    bstop1.down()
    dstop_up_down.up()
    safshut.open()
    fastshutmot.open_manual()
    transmission.set(0.5)

    fastscan.gamma2_short.scan()
    slits_sample.A850()
    fastscan.alpha2_dstop.scan()
    slits_sample.A10008()
    fastscan.khi2_dstop.scan()

    fastscan.gamma2_short.scan()
    slits_sample.A850()
    fastscan.alpha2_dstop.scan()

    fastshutmot.close_manual()
    transmission.set(100)
    slits_sample.A250()




def takespectra(path="/data/bm07/inhouse/opd07/20220408/RAW_DATA/", dtime=20, transmi=10):
        mca_in_out.insert()
        transmission.set(transmi)
        #mca.reset()
        print(mca.calib_c)
        mca.clear_spectrum()
        mca.set_presets(fname=path, ctime=dtime)
        opensh()
        mca.start_acq()    
        time.sleep(dtime+1)
        #mca.stop_acq()
        closesh()
        mca.read_raw_data(save_data=True)
        mca_in_out.remove()



def derivative(y,x):
    var_diff=[]
    var_diff.append(np.gradient(y,x))
    return var_diff



def pico_scan(seconde):
        import tango
        beam_H_width=0.5
        beam_V_Height=0.5
        nbP=10
        
        #self._proxy.command_inout("dstop_up_down.force_up")
        dstop_up_down.force_up()
        umv(sl2ht,0.5*beam_H_width+0.1)
        umv(sl2vt,0.5*beam_V_Height+0.1)
        intensitiesX=[]
        intensitiesZ=[]
        sl2ha.move(2*beam_H_width)
        sl2va.move(2*beam_V_Height)
        for i in range (nbP):
            
            sl2ha.move(2*beam_H_width-((i)*beam_H_width*2/nbP),wait=True) 
            while sl2ha.is_moving()=="True" :
                print(sl2ha.value())
            time.sleep(0.1)
            Ds,mono=pico_moyenne(seconde)
            intensitiesX.append(Ds/mono)
            sl2ha.move(2*beam_H_width)
        for i in range (nbP):
     
            sl2va.move(beam_V_Height-((i)*beam_V_Height/nbP),wait=True) 
            while sl2va.is_moving()=="True" :
                print(sl2va.value())
            time.sleep(0.1)
            Ds,mono=pico_moyenne(seconde)
            intensitiesZ.append(Ds/mono)
        
        d_move=np.linspace(0,1,nbP)
        dx=derivative(intensitiesX,d_move)
        dz=derivative(intensitiesZ,d_move)
        xv,zv=np.meshgrid(dx,dz)
        P_xz=xv*zv
        x=np.linspace(0,beam_H_width,nbP)
        z=np.linspace(0,beam_V_Height,nbP)
        dstop_up_down.force_down()
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.plot_surface(x, z, P_xz)
        plt.xlabel('x')
        plt.ylabel('z')
        plt.show()

        
def beam_profile_narrow_slits(Avg_time_in_s=0.1,scan_width=1,scan_height=1,nb_points=50,nb_lectures_pico=5,beam_transmission=0.01) :
    beam_H_width=scan_width
    beam_V_Height=scan_height
    
    nbP=nb_points #number
    nbM=nb_lectures_pico #
    
    bstop1.down()
    dstop_up_down.up()
    transmission.set(beam_transmission)
    fastshutmot.open_manual()

    umv(sl2ht,0)
    umv(sl2vt,0)
    intensitiesY=[]
    intensitiesZ=[]
    
    dataYstd=[]
    dataZstd=[]
    dataYmean=[]
    dataZmean=[]
    
    posY=[]
    posZ=[]
    
    umv(sl2va,beam_V_Height)
    umv(sl2ha,0.1)
    umv(sl2ht,-beam_H_width/2)
    for i in range (nbP+1):
        for j in range (nbM):
            intensitiesY.append(pico_moyenne(Avg_time_in_s)[0])
            
        dataYstd.append(np.std(intensitiesY))
        dataYmean.append(np.mean(intensitiesY))
        posY.append(sl2ht.position)
        umvr(sl2ht,beam_H_width/nbP)
        time.sleep(0.1)
        intensitiesY=[]
    print(dataYmean) 
    umv(sl2ht,0)  
    umv(sl2ha,beam_H_width)


    umv(sl2va,0.1)
    umv(sl2vt,-beam_V_Height/2)
    for i in range (nbP+1):
        for j in range (nbM):
            intensitiesZ.append(pico_moyenne(Avg_time_in_s)[0] )
        dataZstd.append(np.std(intensitiesZ))
        dataZmean.append(np.mean(intensitiesZ))
        posZ.append(sl2vt.position)
        umvr(sl2vt,beam_V_Height/nbP)
        time.sleep(0.1)
        intensitiesZ=[]

    fastshutmot.close_manual()

    umv(sl2vt,0) 
    umv(sl2va,beam_V_Height)

    
    posY=np.array(posY)
    posZ=np.array(posZ)
    dataYmean = np.array(dataYmean)
    dataZmean = np.array(dataZmean)
    dataYmean=dataYmean.reshape(nb_points+1)
    dataZmean=dataZmean.reshape(nb_points+1)
    posY=posY.reshape(nb_points+1)
    posZ=posZ.reshape(nb_points+1)
    dataYmean=dataYmean.reshape(nb_points+1)
    dataZmean=dataZmean.reshape(nb_points+1)
    plt.title('horizontal profile')
    plt.plot(posY,dataYmean, label='power')  
    plt.legend()
    
    plt.figure()
    plt.title('Vertical profile')
    plt.plot(posZ,dataZmean, label='power')
    plt.legend()


    y_c,z_c=np.meshgrid(dataYmean,dataZmean)
    Local_intensity=y_c*z_c

    fig = plt.figure()
    gs=GridSpec(nrows=2,ncols=2)
    ax0=fig.add_subplot(gs[0,0])
    ax0=plt.contourf(posY,posZ,Local_intensity,cmap='plasma',figsize=(20,10), levels = 1000)
    # ax0=plt.axis('scaled')
    ax0=plt.colorbar()
    ax0=plt.title('Beam Caustic')
    ax0=plt.xlabel('y')
    ax0=plt.ylabel('z')

    ax1=plt.subplot(gs[1,0])
    ax1=plt.plot(posY,dataYmean)
    ax1=plt.title('Y')

    ax2=fig.add_subplot(gs[0,1])
    ax2=plt.plot(dataZmean,posZ)
    ax2=plt.title('Z')
    plt.show()
        
def beam_profile(Avg_time_in_s=0.1,scan_width=1,scan_height=1,nb_points=50,nb_lectures_pico=5,beam_transmission=0.001) :
    beam_H_width=scan_width
    beam_V_Height=scan_height
    
    nbP=nb_points #number
    nbM=nb_lectures_pico #
    
    bstop1.down()
    dstop_up_down.up()
    transmission.set(beam_transmission)
    fastshutmot.open_manual()
    umv(sl2ht,0)
    umv(sl2vt,0)
    intensitiesY=[]
    intensitiesZ=[]
    
    dataYstd=[]
    dataZstd=[]
    dataYmean=[]
    dataZmean=[]
    
    posY=[]
    posZ=[]
    
    sl2ha.move(beam_H_width)
    sl2va.move(beam_V_Height)
    
    for i in range (nbP+1):
        umv(sl2ht,i*(beam_H_width/nbP))
        posY.append((sl2ht.position)-(scan_width/2))

        time.sleep(0.1)
        
        for j in range (nbM):
            intensitiesY.append(pico_moyenne(Avg_time_in_s)[0])
            
        dataYstd.append(np.std(intensitiesY))
        dataYmean.append(np.mean(intensitiesY))
        
        intensitiesY=[]
    print(dataYmean) 
    umv(sl2ht,0)  
    for i in range (nbP+1):
        umv(sl2vt,i*(beam_V_Height/nbP))
        posZ.append((sl2vt.position)-(scan_height/2))

        time.sleep(0.1)
        for j in range (nbM):
            intensitiesZ.append(pico_moyenne(Avg_time_in_s)[0] )
            
        dataZstd.append(np.std(intensitiesZ))
        dataZmean.append(np.mean(intensitiesZ))
        intensitiesZ=[]
    print(dataZmean)
    print(posZ)
    umv(sl2vt,0) 

    fastshutmot.close_manual()

    der_meanY=derivative(dataYmean,posY)
    der_stdY=derivative(dataYstd,posY)   
    der_meanZ=derivative(dataZmean,posZ)
    der_stdZ=derivative(dataZstd,posZ)      
    
   

    der_meanY=np.array(der_meanY)
    der_meanZ=np.array(der_meanZ)
    der_stdY=np.array(der_stdY)
    der_stdZ=np.array(der_stdZ)
    plot_errY_low=(der_meanY-der_stdY)
    plot_errY_high=(der_meanY+der_stdY)
    
    plot_errZ_low=(der_meanZ-der_stdZ)
    plot_errZ_high=(der_meanZ+der_stdZ)
    
    posY=np.array(posY)
    posZ=np.array(posZ)
    posY=posY.reshape(nb_points+1)
    posZ=posZ.reshape(nb_points+1)
    der_meanY=der_meanY.reshape(nb_points+1)
    der_meanZ=der_meanZ.reshape(nb_points+1)
    plot_errY_low=plot_errY_low.reshape(nb_points+1)   
    plot_errZ_high=plot_errZ_high.reshape(nb_points+1)
    plot_errY_high=plot_errY_high.reshape(nb_points+1)   
    plot_errZ_low=plot_errZ_low.reshape(nb_points+1)   
    print(dataYmean)
    print(dataZmean)
    print(der_meanY)
    print(der_meanZ)
    print(type(posY))
    print(type(der_meanY))
    plt.figure()
    plt.title('horizontal profile')
    plt.plot(posY,der_meanY, label='power')
    plt.plot(posY,plot_errY_high, label='std deviation low')    
    plt.plot(posY,plot_errY_low, label='std deviation high')    
    plt.legend()
    
    plt.figure()
    plt.title('Vertical profile')
    plt.plot(posZ,der_meanZ, label='power')
    plt.plot(posZ,plot_errZ_high, label='std deviation low')    
    plt.plot(posZ,plot_errZ_low, label='std deviation high')  
    plt.legend()


    y_c,z_c=np.meshgrid(der_meanY,der_meanZ)
    Local_intensity=y_c*z_c

    fig = plt.figure()
    gs=GridSpec(nrows=2,ncols=2)
    ax0=fig.add_subplot(gs[0,0])
    ax0=plt.contourf(posY,posZ,Local_intensity,cmap='plasma',figsize=(20,10),  levels = 1000)
    # ax0=plt.axis('scaled')
    ax0=plt.colorbar()
    cbar.ax0.locator_params(nbins=5)
    ax0=plt.title('Beam Caustic')
    ax0=plt.xlabel('y')
    ax0=plt.ylabel('z')

    ax1=plt.subplot(gs[1,0])
    ax1=plt.plot(-posY,der_meanY)
    ax1=plt.title('Y')

    ax2=fig.add_subplot(gs[0,1])
    ax2=plt.plot(-der_meanZ,posZ)
    ax2=plt.title('Z')
    plt.show()
               
               
#Scans the beam with a 5um pinhole on the gonio head. Save the data to a csv file in /tmp_14_days/bm07/              
def beam_profile_pinhole( width=1 , height=1 , nb_points=20, Avg_time_in_s=0.1 , nb_lectures_pico=5, beam_transmission=10, filename="profile") :

    import tango

    T_int=Avg_time_in_s*1000
    i1 = tango.AttributeProxy('bm07/pico/oh/i3')
    pico = tango.DeviceProxy('bm07/pico/oh')
    oldvalue = i1.get_poll_period()
    i1.poll(int(T_int))
    #initialisation des variables
    nbPH=nb_points
    nbPV=nb_points
    nbM=nb_lectures_pico 
    nbP=int(nbPH*nbPV)
 
    datastd=[]
    datamean=[]
    lastnp=[]

    posY=[]
    posZ=[]
    
    #lecture position initiale gonio
    y_init = y.position
    z_init = zomega.position

    #creation liste des positions Y et Z
    posY=np.linspace(-width/2,width/2,nbPH)
    posZ=np.linspace(-height/2,height/2,nbPV)

    rangee=0 

    bstop1.down()
    dstop_up_down.up()
    transmission.set(beam_transmission)
    fastshutmot.open_manual()

    sleep(1)
    
    with open("/tmp_14_days/bm07/beam_"+filename+".csv","a+") as f:
        maintenant = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M')
        f.write("date, y, z, Dstop\n")
        #boucles de scan des positions a mesurer
        for valZ in posZ:  
            mv(zomega,z_init+valZ)  
            print (z_init+valZ)
            for valY in posY:
                val=[]  
                mv(y,y_init+valY)
                print(y_init+valY)
                for k in range (nbM):
                    #val.append(pico_moyenne(Avg_time_in_s)[0])
                    val.append(i1.read().value)
                    sleep(Avg_time_in_s+0.01)
                datastd.append(np.std(val))
                datamean.append(np.mean(val))
                f.write("%s,%.2f,%.2f,%.2f\n" %(maintenant,y.position,z.position,np.mean(val)))
                print("y: % .2f" %y.position, "z: % .2f" %z.position, "Dstop: % .2f" %np.mean(val))
        f.close()
    fastshutmot.close_manual()


    # retour du gonio au centre
    umv(zomega,z_init)  
    umv(y,y_init) 
    
    
    print(datamean) 

    datamean=np.array(datamean)
    datastd=np.array(datastd)

    lastnp=datamean.reshape(nbPH,nbPV)
    datastd=datastd.reshape( nbPH,nbPV)

    #plot_err_low=(datamean-datastd)
    #plot_err_high=(datamean+datastd)
    posY=np.array(posY)
    posZ=np.array(posZ)
    
    ver1=[]
    hor1=[]

    #PROFILES
    #Get profiles at the center of the beam: find position of the strongest pixel
    indiceMax = np.unravel_index(lastnp.argmax(), lastnp.shape)
    '''
    #create vertical profile for plot
    for i in range (lastnp.shape[0]):
        ver1.append(np.sum(lastnp[i,0:]))

    #create horizontal profile for plot   
    for j in range(lastnp.shape[1]):   
        hor1.append(np.sum(lastnp[0:,j]))
    '''   
                    
    
    i1.poll(oldvalue)
    
        
    #PLOT THE RESULT IN A 2D GRAPH
    fig=plt.figure()
    gs=GridSpec(nrows=2,ncols=2)

    ax0=fig.add_subplot(gs[0,0])
    #ax0=plt.contourf(posY,posZ,lastnp,cmap='plasma',figsize=(20,10),levels=20)
    ax0=plt.imshow(lastnp,
                origin='lower',
                extent=[np.min(posY), np.max(posY), np.min(posZ), np.max(posZ)])
    ax0=plt.title('Beam Intensity at sample [position in mm] \n and profile at max position')

    ax0=plt.xlabel('y')
    ax0=plt.ylabel('z')
    ax0=plt.colorbar()
    # draw a vertical line where value is max
    xLine = [posY[indiceMax[1]], posY[indiceMax[1]]]
    yLine = [ np.min(posZ),  np.max(posZ)]
    ax0=plt.plot(xLine, yLine, color="white", linewidth=1)


    # draw a horizontal line where value is max
    yLine = [posZ[indiceMax[0]], posZ[indiceMax[0]]]
    xLine = [ np.min(posY),  np.max(posY)]
    ax0=plt.plot(xLine, yLine, color="white", linewidth=1)


    ax1=fig.add_subplot(gs[1,0])
    #ax1=plt.plot(posY,hor1)
    #ax1=plt.plot(posY,lastnp[indiceMax[0],:])
    ax1=plt.plot(posY,np.mean(lastnp,0))
    plt.title('Y')


    ax2=fig.add_subplot(gs[0,1])
    #ax2=plt.plot(lastnp[:,indiceMax[1]],posZ)
    ax2=plt.plot(posY,np.mean(lastnp,1))
    plt.title('Z')
    plt.show()
    
    
    #fig= plt.figure()    
    #plt.contourf(posY,posZ, datastd, cmap='plasma',figsize=(20,10),levels=20)
    #plt.title('Standard deviation Heatmap')        

def fastshut_cycling (nbcycles = 100):
    for i in range(nbcycles-1):
        fastshutmot.open_manual()
        sleep(0.2)
        # saveMonitorsImage('000001',str(i),'/tmp_14_days/fip/')
        fastshutmot.close_manual()
        sleep(0.2)
   

def reset_motors_diffracto():
    kappa.reset_pos()
    omega.sync_hard()
    phi.sync_hard()
    x.sync_hard()
    y.sync_hard()
    z.sync_hard()
    xomega.sync_hard()
    zomega.sync_hard()


def TTL_delay(ttl1_pulse_duration = 1,ttl1_time_btw_pulses = 30, ttl2_delay = 0.3 , ttl2_pulse_duration = 0.3, repeat_nb = 100):
    TTL5V_1.move(0)
    TTL5V_2.move(0)
    TTL1_start_time = 0
    TTL2_start_time = 0

    if (ttl2_delay + ttl2_pulse_duration) > ttl1_time_btw_pulses:
        print("TTL2 parameters cannot make a cycle longer than the first TTL total cycle")
        return

    finish_trig_1_first = False
    if ttl2_delay > ttl1_time_btw_pulses:
        finish_trig_1_first = True

    for i in range(repeat_nb):
        TTL5V_1.move(1) #start TTL1
        TTL1_start_time = round(time.time() * 1000) #time in millisecond

        if finish_trig_1_first:
            sleep(ttl1_pulse_duration)
            TTL5V_1.move(0)
            time.sleep(ttl2_delay - ttl1_pulse_duration)
            TTL5V_2.move(1)
            TTL2_start_time = round(time.time() * 1000) #time in millisecond
        else:
            time.sleep(ttl2_delay)
            TTL5V_2.move(1)
            TTL2_start_time = round(time.time() * 1000) #time in millisecond
        
        with gevent.Timeout(round(ttl1_time_btw_pulses),False):
            while True:
                current_time = round(time.time() * 1000)
                if current_time > (TTL1_start_time + ttl1_pulse_duration):
                    TTL5V_1.move(0)
                if current_time > (TTL2_start_time + ttl2_pulse_duration):
                    TTL5V_2.move(0)
        print (i+1)
        
def beam_align_using_yag():
    slitbox.beam_align_using_yag()   

def flux_using_Dyag():
    slitbox.flux_using_Dyag()




