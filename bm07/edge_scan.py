
from bliss import global_map
from bliss.setup_globals import *
from bliss.common.standard import mv, mvr
import numpy as np
import time
import subprocess


edgeTab = [
            {"energy":6208.0,  "chooch_args":"-e Nd -a L3", "e_min":5000 , "e_max":5000, "atomic_num":60 },  
            {"energy":6459.0,  "chooch_args":"-e Pm -a L3", "e_min":5000 , "e_max":5800, "atomic_num":61 },  
            {"energy":6537.4,  "chooch_args":"-e Mn -a K" , "e_min":5500 , "e_max":6100, "atomic_num":25 },  
            {"energy":6717.0,  "chooch_args":"-e Sm -a L3", "e_min":5300 , "e_max":5800, "atomic_num":62 },  
            {"energy":6980.3,  "chooch_args":"-e Eu -a L3", "e_min":5600 , "e_max":6000, "atomic_num":63 },  
            {"energy":7110.9,  "chooch_args":"-e Fe -a K" , "e_min":6000 , "e_max":6600, "atomic_num":26 },  
            {"energy":7242.9,  "chooch_args":"-e Gd -a L3", "e_min":5700 , "e_max":6200, "atomic_num":64 },  
            {"energy":7515.1,  "chooch_args":"-e Tb -a L3", "e_min":6000 , "e_max":6400, "atomic_num":65 },  
            {"energy":7709.3,  "chooch_args":"-e Co -a K" , "e_min":6500 , "e_max":7200, "atomic_num":27 },  
            {"energy":7789.4,  "chooch_args":"-e Dy -a L3", "e_min":6200 , "e_max":6600, "atomic_num":66 },  
            {"energy":8067.2,  "chooch_args":"-e Ho -a L3", "e_min":6500 , "e_max":6900, "atomic_num":67 },  
            {"energy":8331.4,  "chooch_args":"-e Ni -a K" , "e_min":7000 , "e_max":7800, "atomic_num":28 },  
            {"energy":8357.0,  "chooch_args":"-e Er -a L3", "e_min":6700 , "e_max":7200, "atomic_num":68 },  
            {"energy":8649.1,  "chooch_args":"-e Tm -a L3", "e_min":6900 , "e_max":7300, "atomic_num":69 },  
            {"energy":8943.6,  "chooch_args":"-e Yb -a L3", "e_min":7100 , "e_max":7600, "atomic_num":70 },  
            {"energy":8980.0,  "chooch_args":"-e Cu -a K" , "e_min":7800 , "e_max":8400, "atomic_num":29 },  
            {"energy":9248.5,  "chooch_args":"-e Lu -a L3", "e_min":7400 , "e_max":7900, "atomic_num":71 },  
            {"energy":9557.2,  "chooch_args":"-e Hf -a L3", "e_min":7600 , "e_max":8100, "atomic_num":72 },  
            {"energy":9660.0,  "chooch_args":"-e Zn -a K" , "e_min":8200 , "e_max":9000, "atomic_num":30 },  
            {"energy":9876.2,  "chooch_args":"-e Ta -a L3", "e_min":7900 , "e_max":8300, "atomic_num":73 },  
            {"energy":10199.6, "chooch_args":"-e W  -a L3", "e_min":8000 , "e_max":8600, "atomic_num":74 },  
            {"energy":10367.7, "chooch_args":"-e Ga -a K" , "e_min":9000 , "e_max":9500, "atomic_num":31 },  
            {"energy":10530.6, "chooch_args":"-e Re -a L3", "e_min":8300 , "e_max":8800, "atomic_num":75 },  
            {"energy":10867.5, "chooch_args":"-e Os -a L3", "e_min":8600 , "e_max":9100, "atomic_num":76 },  
            {"energy":11103.2, "chooch_args":"-e Ge -a K" , "e_min":9600 , "e_max":10000, "atomic_num":32},  
            {"energy":11211.5, "chooch_args":"-e Ir -a L3", "e_min":8900 , "e_max":9300, "atomic_num":77 },  
            {"energy":11561.7, "chooch_args":"-e Pt -a L3", "e_min":9100 , "e_max":9700, "atomic_num":78 },  
            {"energy":11863.8, "chooch_args":"-e As -a K" , "e_min":10200, "e_max":10800, "atomic_num":33},  
            {"energy":11920.8, "chooch_args":"-e Au -a L3", "e_min":9500 , "e_max":9900, "atomic_num":79 },  
            {"energy":12285.8, "chooch_args":"-e Hg -a L3", "e_min":9650 , "e_max":10000, "atomic_num":80},  
            {"energy":12654.0, "chooch_args":"-e Se -a K" , "e_min":11000, "e_max":11500, "atomic_num":34},  
            {"energy":12659.7, "chooch_args":"-e Tl -a L3", "e_min":9900 , "e_max":10400, "atomic_num":81},  
            {"energy":13426.1, "chooch_args":"-e Bi -a L3", "e_min":10500, "e_max":11000, "atomic_num":83},  
            {"energy":13040.1, "chooch_args":"-e Pb -a L3", "e_min":10100, "e_max":10700, "atomic_num":82},  
            {"energy":13469.8, "chooch_args":"-e Br -a K" , "e_min":11500, "e_max":12300, "atomic_num":35},  
            {"energy":14323.9, "chooch_args":"-e Kr -a K" , "e_min":12300, "e_max":12800, "atomic_num":36},  
            {"energy":14352.8, "chooch_args":"-e Au -a L1", "e_min":12000, "e_max":12400, "atomic_num":79},  
            {"energy":15201.8, "chooch_args":"-e Rb -a K" , "e_min":13000, "e_max":13600, "atomic_num":37},  
            {"energy":16106.5, "chooch_args":"-e Sr -a K" , "e_min":13900, "e_max":14200, "atomic_num":38},  
            {"energy":17037.7, "chooch_args":"-e Y  -a K" , "e_min":14500, "e_max":15200, "atomic_num":39},  
            {"energy":17166.0, "chooch_args":"-e U  -a L3", "e_min":13300, "e_max":13800, "atomic_num":92},  
            {"energy":17998.1, "chooch_args":"-e Zr -a K" , "e_min":15500, "e_max":15900, "atomic_num":40},
            {"energy":18986.2, "chooch_args":"-e Nb -a K" , "e_min":16200, "e_max":16900, "atomic_num":41},
            {"energy":19999.5, "chooch_args":"-e Mo -a K" , "e_min":17300, "e_max":17600, "atomic_num":42}
        ]

#def nrjcal_vYo(eMin, eMax, eStep, tMoy):
#    import numpy as np
#    txt="energy beta dstop dmono\n"
#    for e in np.arange(eMin,eMax,eStep):
#        energy.move(e)
#        counts = pico_moyenne(tMoy)
#        txt+="%f %f %f %f\n"%(energy.position, beta.position, counts[0], counts[1])
#        print(txt)


def pico_moyenne(seconde):
    import tango
    pico = tango.DeviceProxy('bm07/pico/oh')
    pico.StartT(seconde*1000)
    time.sleep(seconde+0.5)
    dstopMoyenne = pico.ReadIBuffer()[4]/pico.ReadIBuffer()[1]
    dMono1Moyenne = pico.ReadIBuffer()[2]/pico.ReadIBuffer()[1]
    dMono2Moyenne = pico.ReadIBuffer()[3]/pico.ReadIBuffer()[1]
    return [dstopMoyenne, 0.5*(dMono1Moyenne+dMono2Moyenne)]


def nrjcal_vYo2(eMin, eMax, eStep, tMoy):
    mca.start_acq(300) #(mca.times['real_t_preset']=tMoy
    mca.set_roi(11.5,13.5,channel=1,element="Se",atomic_nb=34)
    mca.sl.write_readline(b"$CT 3000\r")
    txt="energy beta p0 dstop dmono\n"
    for e in np.arange(eMin,eMax,eStep):
        energy.move(e)
        #mca.start_acq()
        counts = pico_moyenne(tMoy)
        #resp = mca.sl.write_readline(b"$GR 1\r")
        #p0 = int(resp[3:])
        p0=mca.read_chrate(1)
        txt+="%f %f %f %f %f\n"%(energy.position, beta.position, p0, counts[0], counts[1])
        print(txt)
    mca.stop_acq()

def nrjcal_edge_chooch(energy_start_rel_eV=45, energy_end_rel_eV=55, nb_point=50):
    energy_tab = np.array([])
    chooch_args_tab = np.array([])
    edge_min_tab = np.array([])
    edge_max_tab = np.array([])
    atomic_num_tab = np.array([])
    for values in edgeTab:
        energy_tab = np.append(energy_tab, values["energy"])
        chooch_args_tab = np.append(chooch_args_tab, values["chooch_args"])
        edge_min_tab = np.append(edge_min_tab, values["e_min"])
        edge_max_tab = np.append(edge_max_tab, values["e_max"])
        atomic_num_tab = np.append(atomic_num_tab, values["atomic_num"])
    
    cur_nrj_keV = energy.position
    idx = np.abs(energy_tab - cur_nrj_keV*1000).argmin()
    #idx=2
    chooch_args = chooch_args_tab[idx]
    edge_min = edge_min_tab[idx]
    edge_max = edge_max_tab[idx]
    atomic_num = atomic_num_tab[idx]
    element = chooch_args.split()[1]
    print("Energy=",cur_nrj_keV, "element=",element, " atomic_num=",atomic_num, " chooch=",chooch_args, " edge_min=",edge_min , " edge_max=",edge_max)


    nrj_min_keV = cur_nrj_keV-energy_start_rel_eV/1000.
    nrj_max_keV = cur_nrj_keV+energy_end_rel_eV/1000.
    mca.start_acq(1000)
    mca.set_roi(edge_min/1000.,edge_max/1000., channel=1, element=element, atomic_nb=atomic_num)
    mca.sl.write_readline(b"$CT 3000\r")
    txt="energy beta p0 dstop dmono\n"
    fchooch = open("/tmp/chooch.txt", "w")
    fchooch.write("chooch\n")
    fchooch.write("%d\n"%nb_point)
    for e in np.linspace(nrj_min_keV, nrj_max_keV, nb_point):
        energy.move(e)
        #mca.start_acq()
        counts = pico_moyenne(3)
        #resp = mca.sl.write_readline(b"$GR 1\r")
        #p0 = int(resp[3:])
        p0=mca.read_chrate(1)
        txt+="%f %f %f %f\n"%(energy.position, beta.position, p0, counts[1])
        print(txt)
        fchooch.write("%f %f\n"%(energy.position*1000,p0/counts[1]))
        fchooch.flush()
    mca.stop_acq()
    fchooch.close()


    process = subprocess.Popen("chooch %s -g /tmp/chooch.png /tmp/chooch.txt"%chooch_args, shell=True, stdout=subprocess.PIPE)
    process.wait()

    energy.move(cur_nrj_keV)

