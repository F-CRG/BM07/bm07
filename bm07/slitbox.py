from gevent import socket, ssl, spawn
from gevent.event import Event
from gevent.server import StreamServer
from functools import partial
from gevent.pool import Pool
from bliss.comm.modbus import ModbusTCP
from time import sleep
from bliss.common import tango
import untangle
import gevent
import time
from bliss.common.motor_group import Group
from bliss.config.static import get_config
#from bm07.optical_scripts import *

import numpy as np




class slitbox:
    def __init__(self, name, config):
        self.name = name
        self.sl1va = config["sl1va"]
        self.sl1ha = config["sl1ha"]
        self.transmission = config["transmission"]
        self.fluo1 = config["fluo1"]
        self.fluo2 = config["fluo2"]
        self.fluo3 = config["fluo3"]
        self.sbyag = config["sbyag"]
        self.bstop1 = config["bstop1"]
        self.fastshutmot = config["fastshutmot"]
        self.sbcam = config["sbcam"]
        self.alpha2 = config["alpha2"]
        self.khi2 = config["khi2"]
        self.detcover = config["detcover"]
        self.Dyag = config["Dyag"]
        

    def beam_align_using_yag(self, xaligned = 523, yaligned = 419):
        # xaligned = 523 #pixel position when beam aligned ###passer de 520 a 523 par lucas le 07/11/24 
        # yaligned = 419 #pixel position when beam aligned
        khi2coef = 0.000037 #degrees / pixel on sbcam
        alpha2coef = 5.26E-06 #degrees / pixel on sbcam

        # test if detcover is closed or beamstop is up. if one of them, we cancel the operation
        if (self.detcover.is_close() == False) and (self.bstop1.is_in == False):
            print("There is nothing to stop the beam in front of the detector. No beamstop nor detcover!")
        else:
            sl1va_orig = self.sl1va.position
            sl1ha_orig = self.sl1ha.position
            #umv(sl1va,3)
            #umv(sl1ha,3)
            transmission_orig = self.transmission.get()
            self.transmission.set(10)

            self.fluo1.close()
            self.fluo2.close()
            self.fluo3.close()
            self.sbyag.open()
            time.sleep(2)
            self.fastshutmot.open_manual()

            #adjust sbcam parameters:
            self.sbcam.rotation = 'NONE'
            self.sbcam.exposure = 0.0002
            while self.sbcam.raw_read()[1] < 4000 and self.sbcam.exposure < 1:
                self.sbcam.exposure = self.sbcam.exposure * 2
                #print (sbcam.raw_read()[1])
                #print (sbcam.exposure )
            if self.sbcam.exposure > 1:
                print('sbcam cannot see any light!')
                self.fastshutmot.close_manual()
                self.sbyag.close()
                self.sl1va.move(sl1va_orig)
                self.sl1ha.move(sl1ha_orig)
                return None
            
            #ALIGN THE BEAM, 3 ITERATIONS:
            for i in range(3):
                time.sleep(0.5)
                #read the position of the beam
                xpos = self.sbcam.raw_read()[2]
                ypos = self.sbcam.raw_read()[3]
                #stop if the beam is too far
                if abs(xpos - xaligned) > 100 or abs(ypos - yaligned) > 100 :
                    print ('Beam too far to be automatically aligned')
                    self.fastshutmot.close_manual()
                    self.sbyag.close()
                    self.sl1va.move(sl1va_orig)
                    self.sl1ha.move(sl1ha_orig)
                    return None
                #adjust the beam position
                xmove = (xaligned-xpos)*alpha2coef
                ymove = (yaligned-ypos)*khi2coef
                if abs(xaligned-xpos) < 2 and abs(yaligned-ypos) < 2:
                    break # the alignement is sufficient, we leave the loop
                if xmove < 0.005: #we don't want to move too far...
                    self.alpha2.rmove(xmove)
                    print('move alpha2: '+str(xmove))
                else:
                    print('alpha2 movement required is too large!')
                    self.fastshutmot.close_manual()
                    self.sbyag.close()
                    self.sl1va.move(sl1va_orig)
                    self.sl1ha.move(sl1ha_orig)
                    return None
                if ymove < 0.05: #we don't want to move too far...
                    self.khi2.rmove(ymove)
                    print('move khi2: '+str(ymove))
                else:
                    print('khi2 movement required is too large!')
                    self.fastshutmot.close_manual()
                    self.sbyag.close()
                    self.sl1va.move(sl1va_orig)
                    self.sl1ha.move(sl1ha_orig)
                    return None
            
            print('The beam should now be aligned')
            self.fastshutmot.close_manual()
            self.sbyag.close()
            self.sl1va.move(sl1va_orig)
            self.sl1ha.move(sl1ha_orig)
            self.transmission.set(transmission_orig)


    def flux_using_Dyag(self,average_nb = 10):
        # test if detcover is closed or beamstop is up. if one of them, we cancel the operation
        if (self.detcover.is_close() == False) and (self.bstop1.is_in == False):
            print("There is nothing to stop the beam in front of the detector. No beamstop nor detcover!")
        else:
            transmission_orig = self.transmission.get()
            self.transmission.set(100)
            self.sbyag.open()
            time.sleep(2)
            self.fastshutmot.open_manual()

            flux_tab = []
            for i in range(average_nb):
                flux_tab.append(self.Dyag.value)
                time.sleep(1)

            print (flux_tab)
            flux = np.mean(flux_tab)
            #print('Flux: %d pA'%flux)
            self.fastshutmot.close_manual()
            self.sbyag.close()
            self.transmission.set(transmission_orig)
            raise KeyError('Flux: %d pA'%flux)