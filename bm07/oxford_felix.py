#from bliss.controllers.motors.icepap import *
from bliss.comm.tcp import Command
from bliss.controllers.motors.icepap.comm import _command, _ackcommand, _vdata_header
import time
from bliss.common import tango

class oxford_felix(object):
    def __init__(self, name, config, config_objects=None):
        self._config_objects = config_objects
        self.proxy = tango.DeviceProxy(config["tango_proxy"])
        self.name=name
    #     self.proxy.subscribe_event("Temp", tango.EventType.CHANGE_EVENT, self.tango_call_back, [])
        
    # def tango_call_back(self, ev):
    #     name = ev.attr_value.name
    #     value = ev.attr_value.value
    #     if name == "Temp".lower():
    #         print(value)

    def read_T(self):                                      # lis et affiche la temperature
        return self.proxy.read_T()

    def gas_temp(self):
        return self.read_T()

    def read_gas_temperature(self):
        return self.read_T()

    def read_Status(self):                                      # read and returns status
        '''Ramp, /* = 0: Current phase is a Ramp */
        Cool, /* = 1: Current phase is a Cool */
        Plat, /* = 2: Current phase is a Plat */
        Hold, /* = 3: Current phase is a Hold */
        End, /* = 4: Current phase is an End */
        Purge, /* = 5: Current phase is a Purge */
        DeletePhase, /* = 6: Internal use only */
        LoadProgram, /* = 7: Internal use only */
        SaveProgram, /* = 8: Internal use only */
        Soak, /* = 9: Part of the Purge phase */
        Wait /* = 10: Part of Ramp/Wait */'''
        return self.proxy.read_Status()

    def ramp(self,rate_Kph_upto_350, T_cible_K):
        self.proxy.ramp([rate_Kph_upto_350, T_cible_K])


    def ramp_up_down (self,rate_Kph_upto_350,T_cible_K,hold_time_in_s=1): 
        temp_init = round(self.read_T())
        print ("Origin temperature: "+str(temp_init))
        print("Starting first ramp")
        self.ramp(rate_Kph_upto_350, T_cible_K)

        #WAIT END OF RAMP
        i=0
        while self.read_Status() != 3:
            if i%60 == 0: 
                print ("Ramp 1...")
            time.sleep(1)
            i+=1

        #WAIT the requested time
        print("Holding (s): "+str(hold_time_in_s))
        time.sleep(hold_time_in_s)

        #START RETURN RAMP
        print("Starting return ramp")
        self.ramp(rate_Kph_upto_350, temp_init)
        
        #WAIT T final
        i=0
        while self.read_Status() != 3:
            if i%60 == 0: 
                print ("Ramp back...")
            time.sleep(1)
            i+=1
        print("Cryostream back to initial temperature")


    def cool (self):                                   #Set la temperature a 100K
        self.ramp(self.default_rate,self.Tcool)
        
        
        
    def heat(self):                                    #Chauffe a T ambiante
        self.ramp(self.default_rate,self.Theat)

    def dry(self):                                      # chauffe a 350K
        self.ramp(self.default_rate,self.Tdry)
       
    def stop_dry_and_cool(self):
        self.proxy.stop_dry_and_cool()

    def dry_and_cool(self):                   #seche la canne attends 2 heures puis refroidi
      self.proxy.dry_and_cool()

    
      