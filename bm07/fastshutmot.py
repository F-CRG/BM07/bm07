#from bliss.controllers.motors.icepap import *
from bliss.comm.tcp import Command
from bliss.controllers.motors.icepap.comm import _command, _ackcommand, _vdata_header
from bliss.common import axis
import gevent
import time

class fastshutmot(object):
    def __init__(self, name, config, config_objects=None):
        self._config_objects = config_objects
        hostname = config.get("host")
        self.address = config.get("address")
        self.name=name
        self._cnx = Command(hostname, 5000, eol="\n")
        self.posOpen = config["posOpen"]
        self.posClose = config["posClose"]
        self.acct = config["acct"]
        self.vel = config["vel"]
        self.init()



#****************************  NEW 05 NOV 2024 **************************************   
    def init(self):
         _command(self._cnx, "%d:STOP"%self.address)
         _command(self._cnx, "%d:LISTDAT CLEAR"%self.address)
         _command(self._cnx, "%d:LISTDAT CYCLIC %d %d 2"%(self.address,self.posOpen,self.posClose))
         _command(self._cnx, "%d:ACCTIME %f"%(self.address,self.acct))
         _command(self._cnx, "%d:VELOCITY %f"%(self.address,self.vel))

    def zero(self):
        _command(self._cnx, "%d:ECAM OFF"%self.address)
        _command(self._cnx, "%d:STOP"%self.address)
        _command(self._cnx, "%d:INFOA LOW NORMAL"%self.address)
    
    def ramp(self, start_pos, end_pos):
        self.init() #usefull to restart the LTRACK mode if any change
        # Define the start_pos and end_pos for level 1 of output 1
        start_step = round(start_pos * 12800) # in encoder steps
        end_step =  round(end_pos* 12800) # in encoder steps
        print("fastshutmot_ramp: start: %d,  end: %d"%(start_step, end_step))
        # _command(self._cnx, "%d:INFOA ECAM NORMAL"%self.address) #copy ECAM onto InfoA
        # _command(self._cnx, "%d:ECAM ON"%self.address) #start the ECAM signal
        # _command(self._cnx, "%d:ECAMDAT ENCIN %d %d 2"%(self.address, start_step, end_step)) #ECAM is On when EncoderIn is between the 2 values (gate)
        _command(self._cnx, "%d:POS INPOS 1"%self.address) #Init the current position if necessary (closed)
        _command(self._cnx, "%d:LTRACK INPOS"%self.address) #Start the Tracking of InPos (connected to InfoA, which is a copy of ECAM). When high = Pos 0, when low = pos 0
#****************************  END NEW 21 OCT 2024 **************************************  




# # ****************************  VERSION BEFORE 21 OCT 2024 **************************************  
#     def init(self):
#          _command(self._cnx, "%d:STOP"%self.address)
#          _command(self._cnx, "%d:LISTDAT CLEAR"%self.address)
#          _command(self._cnx, "%d:LISTDAT %d %d 2"%(self.address,self.posOpen,self.posClose))
#          _command(self._cnx, "%d:ACCTIME %f"%(self.address,self.acct))
#          _command(self._cnx, "%d:VELOCITY %f"%(self.address,self.vel))

#     def zero(self):
#         _command(self._cnx, "%d:ECAM OFF"%self.address)
#         _command(self._cnx, "%d:STOP"%self.address)
#         _command(self._cnx, "%d:INFOA LOW NORMAL"%self.address)
    
#     def ramp(self, start_pos, end_pos):
#         # Define the start_pos and end_pos for level 1 of output 1
#         start_step = round(start_pos * 12800) # in encoder steps
#         end_step =  round(end_pos* 12800) # in encoder steps
#         print("fastshutmot_ramp: start: %d,  end: %d"%(start_step, end_step))
#         _command(self._cnx, "%d:INFOA ECAM NORMAL"%self.address)
#         _command(self._cnx, "%d:ECAM ON"%self.address)
#         _command(self._cnx, "%d:ECAMDAT ENCIN %d %d 2"%(self.address, start_step, end_step))
#         curpos = self.readpos() 
#         if curpos >start_step and curpos < end_step:
#             _command(self._cnx, "%d:ECAM HIGH"%self.address)
#             _command(self._cnx, "%d:POS INPOS 1"%self.address)
#         else:
#             _command(self._cnx, "%d:ECAM LOW"%self.address)
#             _command(self._cnx, "%d:POS INPOS 0"%self.address)
#         _command(self._cnx, "%d:LTRACK INPOS"%self.address)
# # ****************************  END VERSION BEFORE 21 OCT 2024 **************************************  




    def readpos(self):
        # Read the position
        value = _command(self._cnx, "?ENC ENCIN %d"%self.address)
        return int(value)

    def reset(self):
        self.zero()
        self.set_current_position(0)

    def set_current_position(self, val_deg):
        val_step = round(val_deg*12800)
        _command(self._cnx, "%d:ENC ENCIN %d"%(self.address, val_step))

    def readramp(self):
        # Read the actual ramp range (where the shutter is open)
        ret = _command(self._cnx, "%s:?ECAMDAT"%self.address)
        return ret

    def status(self):
        ret = "ECAM=%s\n"%_command(self._cnx, "%s:?ECAM"%self.address)
        ret += "INFOA=%s\n"%_command(self._cnx, "%s:?INFOA"%self.address)
        ret += "INFOB=%s"%_command(self._cnx, "%s:?INFOB"%self.address)
        print(ret)

    def state(self):
        return int(_command(self._cnx, "%d:?POS"%(self.address)))==self.posOpen

    def command(self, cmd):
        return _command(self._cnx, "%d:%s"%(self.address,cmd))

    def open_manual(self):
        _command(self._cnx, "%d:MOVE %d"%(self.address,self.posOpen))

    def close_manual(self):
        _command(self._cnx, "%d:MOVE %d"%(self.address,self.posClose))

    def force_infoA(self,source):
        _command(self._cnx, "%d:INFOA %s NORMAL"%(self.address,source))

    def force_infoB(self,source):
        _command(self._cnx, "%d:INFOB %s NORMAL"%(self.address,source))

    def set_manual_mode(self):
        self.zero()

    def get_limits(self):
        pass

    def homing(self):
        self.init()
        _command(self._cnx, "%d:HOMESRC Home"%(self.address))
        _command(self._cnx, "%d:HOMEVEL 1000"%(self.address))
        homepos = -999
        #look for the home signal
        _command(self._cnx, "%d:HOME +1"%(self.address))
        notok=True
        with gevent.Timeout(30,False):
            while (_command(self._cnx, "%d:?HOMESTAT"%(self.address)) == "MOVING +1"):
                time.sleep(0.5)
                #print ("searching home signal...")
            notok = False
            homepos = _command(self._cnx, "%d:?POS"%(self.address))
        if (notok == True):
            print("fastshutmot homing failed")
        else:
            # print ("fastshutmot home position found = %s"%(homepos))
            print ("Fastshutmot home position reset")
            _command(self._cnx, "%d:POS 922"%(self.address))
            _command(self._cnx, "%d:MOVE %d"%(self.address,self.posClose))
     
                