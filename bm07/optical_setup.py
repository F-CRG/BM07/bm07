from bm07.edge_scan import pico_moyenne
from bliss.config.static import get_config
from bliss.shell.cli.user_dialog import UserInput, UserCheckBox, UserChoice, BlissWizard
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss import global_map
from bliss.setup_globals import *
from matplotlib.gridspec import GridSpec
from bliss.common.standard import mv, mvr
import numpy as np
import datetime
from .geofip import geofip, tab_nrj, nrj
from bliss.physics import trajectory
from bliss.common import axis
from bliss.common.motor_group import TrajectoryGroup
import time
import math
import datetime
from PIL import Image
import requests
import termplotlib as tpl
import urllib.request
import scipy.optimize as opt
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy.interpolate import interp1d
import enlighten
import gevent.event
import gevent


CRED = '\033[91m'
CGRE = '\033[92m'
CEND = '\033[0m'

setattr(energy,'move_to',type('', (), {})())
for valuelist in tab_nrj:
    setattr(energy.move_to,"_"+str(valuelist[3]).replace(".","_")+"_"+valuelist[0]+"_"+valuelist[1],valuelist[3])
    #setattr(nrj,valuelist[0]+"_"+valuelist[1]+"_"+str(valuelist[3]).replace(".","_"),valuelist[3])

def gauss(x, p): # p[0]==mean, p[1]==stdev
    return 1.0/(p[1]*np.sqrt(2*np.pi))*np.exp(-(x-p[0])**2/(2*p[1]**2))


def analyseImg():
    name="/tmp/pictures/test.jpg"
    X = np.arange(0,800,1)
    img = Image.open(name, "r")
    imgcrp = img.crop((700, 0, 1100, 800))
    pix = np.asarray(imgcrp)
    pix2 = np.where(pix>50,pix,0)
    projection = np.sum(np.sum(pix2, axis = 1), axis=1)    
    projection = np.true_divide(projection, np.sum(projection))
    return ndimage.center_of_mass(projection)[0]

def change_energy(value):
    mv(energy,value)

def find_energy_in_conf(nrj_target, force=False):                                                                                                                               
    cc = get_config()                                                                                                                                                           
    cc.reload()                                                                                                                                                                 
    optical_conf = cc.get("optical_setup")                    
    optical_conf_selected = dict(filter(lambda entry: entry[1]!= 'interpolate', optical_conf["selected"].items()))                                                                                                             
    list_selected_str = list(optical_conf_selected.keys())                                                                                                                   
    date_selected =list(optical_conf_selected.values())                                                                                                                      
    list_selected_dict = {float(i):i for i in list_selected_str}                                                                                                                
    list_selected_float = list(list_selected_dict.keys())                                                                                                                       
    list_selected_float.sort()                                                                                                                                                  
                                                                                                                                                                                
    #pas extrapolation on ne peux pas aller au dela de la plage d'energie deja alignée                                                                                          
    if nrj_target<list_selected_float[0]:                                                                                                                                       
        nrj_target=list_selected_float[0]                                                                                                                                       
    elif nrj_target>list_selected_float[-1]:                                                                                                                                    
        nrj_target=list_selected_float[-1]                                                                                                                                      
     
    #est-ce une energie pile poile calibrée
    if nrj_target in list_selected_float:
        idx = list_selected_str.index(list_selected_dict[nrj_target])
        confdata = dict(optical_conf.get(list_selected_str[idx]).get(date_selected[idx]))
        geofipdata = geofip(nrj_target, VERBOSE=0)
        if "alpha1" not in confdata:
            confdata.update({"alpha1":round((geofipdata.get("angle_m1")*180./math.pi), 4)})
        if "beta" not in confdata:
            confdata.update({"beta":round((geofipdata.get("mono_angle")*180./math.pi), 4)})
        return confdata
    #sinon interpolation
    else:
        search1 = min(enumerate(list_selected_float), key=lambda x:abs(nrj_target-x[1]))
        if nrj_target>list_selected_float[search1[0]]:
            search2 = (search1[0]+1, list_selected_float[search1[0]+1])
        else:
            search2 = (search1[0]-1, list_selected_float[search1[0]-1])
         
        idx1 = list_selected_str.index(list_selected_dict[list_selected_float[search1[0]]])
        idx2 = list_selected_str.index(list_selected_dict[list_selected_float[search2[0]]])
        confdata1 = dict(optical_conf.get(list_selected_str[idx1]).get(date_selected[idx1]))
        confdata2 = dict(optical_conf.get(list_selected_str[idx2]).get(date_selected[idx2]))
        geofipdata1 = geofip(search1[1], VERBOSE=0)
        geofipdata2 = geofip(search2[1], VERBOSE=0)
        if "alpha1" not in confdata1:
            confdata1.update({"alpha1":round((geofipdata1.get("angle_m1")*180./math.pi), 4)})
        if "alpha1" not in confdata2:
            confdata2.update({"alpha1":round((geofipdata2.get("angle_m1")*180./math.pi), 4)})
        if "beta" not in confdata1:
            confdata1.update({"beta":round((geofipdata1.get("mono_angle")*180./math.pi), 4)})
        if "beta" not in confdata2:
            confdata2.update({"beta":round((geofipdata2.get("mono_angle")*180./math.pi), 4)})

        if confdata1.keys() != confdata2.keys():
            print("ERROR : Some motors doesn't not exist in one the other configuration use for interpolation")
            print(set(confdata1.keys()) ^ set(confdata2.keys()))
            print("Use \"force\" argument to go further")
            if not force:
                return
        print(confdata1)
        print(confdata2)
        condataInter = {}
        nrj_arr=np.array([search1[1], search2[1]])
        for mot in confdata1.keys():
            if mot in confdata2.keys() and mot != "comment":
                mot_arr=np.array([confdata1[mot],confdata2[mot]])
                f = interp1d(nrj_arr, mot_arr)
                condataInter.update({mot:float(f(nrj_target))})
        return condataInter


def energy_calc_and_set_offset(current_real_energy_in_keV, force=False):    
    cc = get_config()                                                                                                                                                           
    cc.reload()                                                                                                                                                                 
    offset_conf = cc.get("offset_beta")   
    today_str = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M')

    if current_real_energy_in_keV <5 or current_real_energy_in_keV > 22:
        print("ATTENTION L'ENERGIE DOIT ETRE EN keV")
    else:
        if not force and abs(energy.position - current_real_energy_in_keV) > 500 :
            print("Warning the new energy is very different use the option force=True to overright this safety")
        else:
            energy.position = current_real_energy_in_keV
            offset_conf[today_str] = {"offset":beta.offset}
            offset_conf.save()




def beam_set(nrj_target, mono_only=False, force=False):                                                                                                                         
    final_pos_dict_with_enc = find_energy_in_conf(nrj_target, force=force)     
    final_pos_dict = dict(filter(lambda entry: "_enc" not in entry[0], final_pos_dict_with_enc.items()))   

    if final_pos_dict is None:                                                                                                                                                  
        return                                                                                                                                                                  
                                                                                                                                                                                
    mono_motor_name_list = ["beta", "utx", "utz", "moveh"]                                                                                                                      
                                                                                                                                                                                
    #get usefull object motor                                                                                                                                                   
    if mono_only:                                                                                                                                                               
        motor_name_list = mono_motor_name_list                                                                                                                                  
    else:                                                                                                                                                                       
        motor_name_list = list(final_pos_dict.keys())                                                                                                                           
                                                                                                                                                                                
    motor_list_traj = []                                                                                                                                                        
    motor_list = []                                                                                                                                                             
    for axis in global_map.get_axes_iter():                                                                                                                                     
        if axis.name in mono_motor_name_list:                                                                                                                                   
            motor_list_traj.append(axis)                                                                                                                                        
        if axis.name in motor_name_list:                                                                                                                                        
            motor_list.append(axis)                                                                                                                                             
                                                                                                                                                                                
    #comput the time need for the slowest motor to reach target                                                                                                                 
    time_slowest = 0                                                                                                                                                            
    slowest_axis = None                                                                                                                                                         
    for motor in motor_list_traj:      
        motor.sync_hard()
        motor.apply_config(reload=True)
        distance = abs(motor.position-final_pos_dict.get(motor.name))
        Xvmax = 0.5*motor.velocity*motor.velocity/motor.acceleration
        if distance <= Xvmax*2:
            moving_time = 2*sqrt(0.5*distance/motor.acceleration)
        else:
            moving_time = 2*motor.velocity/motor.acceleration + sqrt(2*(distance-2*Xvmax)/motor.acceleration)
        print("%-10s velocity=%f distance=%f time_to_move=%f"%(motor.name, motor.velocity,distance, moving_time))
        if moving_time > time_slowest:
            time_slowest=moving_time
            slowest_axis = motor
    print("%s is the slowest axis all others follow its speed"%slowest_axis.name)
    
    #change velocity for motor in traj
    for motor in motor_list_traj:
        distance = abs(motor.position-final_pos_dict.get(motor.name))
        #print(motor.name, "vel_opti=",distance/time_slowest, "vel_min=",motor.velocity*0.01, "vel_max=",motor.velocity)
        velocity = min(max(distance/time_slowest, motor.velocity*0.01),motor.velocity)
        motor.velocity=velocity

    #move motor to reach their target all at the same time (good speed)                                                                                                         
    motor_need_moving_list = []
    for motor in motor_list:
        motor.move(final_pos_dict.get(motor.name), wait=False)
        motor_need_moving_list.append(motor)

    #verif si un axe bouge pas et qui n'est pas à la position final stop de tout   
    with enlighten.Manager() as manager:
        progressbar = {}
        for motor in motor_need_moving_list:
            progressbar[motor]={"bar":manager.counter(
                                            total=100, 
                                            desc='%s %f->%f'%(motor.name, motor.position, final_pos_dict.get(motor.name)),
                                            bar_format=u'{desc}{desc_pad}{percentage:3.0f}%|{bar}',
                                            counter_format=u'{desc}{desc_pad}DONE'),
                                "init":motor.position,
                                "final":final_pos_dict.get(motor.name)}
        ok = True
        motorfail = None                                                                                                                                                                                                                                                                                                    
        while ok:                                                                           
            try:
                ok = False
                for motor in motor_need_moving_list:
                    if motor.is_moving:
                        ok = True
                        progressbar[motor]["bar"].count=(100.*(motor.position-progressbar[motor]["init"])/(progressbar[motor]["final"]-progressbar[motor]["init"]))
                    else:
                        distance = abs(motor.position-final_pos_dict.get(motor.name))
                        if distance>motor.tolerance:
                            ok = False
                            motorfail=motor
                        else:
                            progressbar[motor]["bar"].update(100)
                    progressbar[motor]["bar"].refresh()        
                sleep(0.5)
            except KeyboardInterrupt:
                for motor in motor_need_moving_list:
                    motor.stop()
                break
            except Exception as e:
                print(CRED+"\n!!! Emergency stop - error or keyboard interupt !!!\n"+CEND)
                print(e)
                mono_stop()
                break
  
    
    if not(motorfail is None):
        print(CRED+"\n!!! Emergency stop - %s can not move !!!\n"%(motorfail.name)+CEND)
        for motor in motor_need_moving_list:
            motor.stop()
    else:
        beta.apply_config(reload=True)
        umv(energy,nrj_target)

    for motor in motor_list:
        motor.apply_config(reload=True)

    # if "gamma2_enc" in final_pos_dict_with_enc.keys():
    #     for repeat in range(0,8):
    #         mvr(gamma2, float(final_pos_dict_with_enc['gamma2_enc'])-gamma2_enc.read())
    # if "khi2_enc" in final_pos_dict_with_enc.keys():
    #     for repeat in range(0,8):
    #         mvr(khi2, float(final_pos_dict_with_enc['khi2_enc'])-khi2_enc.read())
    


def mono_stop():
    utx.stop()
    utz.stop()
    beta.stop()
    gamma2.stop()
    omega2.stop()
    khi2.stop()
    moveh.stop()
    
def geofip_fill_beam_save():
    import math
    
    cc = get_config()
    cc.reload()
    optical_conf = cc.get("optical_setup")

    for nrj in tab_nrj:
        resu_geofip = geofip(nrj[2], VERBOSE=0)
        dict_pos_mot = {
                "alpha1":   round((resu_geofip.get("angle_m1")*180./math.pi), 4),
                "beta":     round((resu_geofip.get("mono_angle")*180./math.pi), 4),
                "utx":      round(resu_geofip.get("dx"), 4),
                "utz":      round(resu_geofip.get("dz"), 4),
                "moveh":    round(resu_geofip.get("z_c1"), 4),
                "alpha2":   round((resu_geofip.get("angle_m2")*180./math.pi), 4)
            }
        if(optical_conf.get(str(nrj[3])) is None):
            new_conf = { "geofip": dict_pos_mot }
        else:
            new_conf = optical_conf.get(str(nrj[3]))
            new_conf["geofip"] = dict_pos_mot
            new_conf.save()
            
        optical_conf[str(nrj[3])]= new_conf
    optical_conf.save()


def beam_save(comment="No comment"):                                                                                                                                                                    
    current_nrj = energy.position                                                                                                                                                                                                                                                                                                                 
    cc = get_config()                                                                                                                                                           
    cc.reload()                                                                                                                                                                 
    optical_conf = cc.get("optical_setup")                                                                                                                                      
    list_nrj_setup = list(optical_conf.keys())                                                                                                                                  
    list_nrj_setup.remove("name")
    if "selected" in list_nrj_setup:
        list_nrj_setup.remove("selected")
    list_nrj_setup = np.array([float(i) for i in list_nrj_setup])
    list_nrj_setup.sort()
    tab_nrj_kev = np.array([i[3] for i in tab_nrj])

     
     
    #Search for the closest idx
    tab_nrj_kev = np.array([i[3] for i in tab_nrj])
    idx = (np.abs(tab_nrj_kev-(energy.position/1000.))).argmin()


    tab_nrj_choice = [(str(i), "%6s %6s"%(str(i),"-".join(tab_nrj[np.argwhere(tab_nrj_kev==i)[0][0]][0:2]))) for i in list_nrj_setup]
    defval=0
    for idx in range(len(tab_nrj_choice)):
        if abs(float(tab_nrj_choice[idx][0])-current_nrj)<0.005:
            defval=idx

    dlg3 = UserChoice(label="Your are currently at %.4fkeV - Choose the energy for saving the current motors positions"%(energy.position), values=tab_nrj_choice, defval=defval)
    new_nrg_to_save = BlissDialog( [ [ dlg3 ] ], title='Beam save').show()
    if new_nrg_to_save is not False:
        new_nrg_to_save = list(new_nrg_to_save.values())[0]
         
        cc = get_config()
        cc.reload()
        optical_conf = cc.get("optical_setup")
        dict_pos_mot = {"comment":comment[0:50],
                        "omega2":omega2.position,
                        "gamma2":gamma2.position,
                        "khi2":khi2.position,
                        "utx":utx.position,
                        "utz":utz.position,
                        "moveh":moveh.position,
                        "alpha2":alpha2.position,
                        "actam1":actam1.position,
                        "actav1":actav1.position,
                        "actam2":actam2.position,
                        "actav2":actav2.position,
                        "c2bendbk":c2bendbk.position,
                        "c2bendfr":c2bendfr.position,
                        "khi2_enc":khi2_enc.read(),
                        "gamma2_enc":gamma2_enc.read()
                        }
         
        today_str = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M')
         
        if(optical_conf.get(new_nrg_to_save) is None):
            new_conf = { today_str: dict_pos_mot }
        else:
            new_conf = optical_conf.get(new_nrg_to_save)
            new_conf[today_str] = dict_pos_mot
            new_conf.save()
             


def beam_get_setup(nrj):
    
    # kevification de nrj
    if nrj > 1000:
        nrj /= 1000
    elif nrj<2:
        nrj *= 12.39856
    
    
    cc = get_config()
    cc.reload()
    optical_conf = cc.get("optical_setup")
    list_nrj_setup = list(optical_conf.keys())
    list_nrj_setup.remove("name")
    list_nrj_setup.remove("selected")
    list_nrj_setup = np.array([float(i) for i in list_nrj_setup])
    list_nrj_setup.sort()
    
    
    # search idx for interpolation
    idx = (np.abs(list_nrj_setup-nrj)).argmin()
    if list_nrj_setup[idx] >= nrj :
        idxmin = idx-1
        idxmax = idx
    else:
        idxmin = idx
        idxmax = idx+1
    if idxmin < 0:
        idxmin = 0
    if idxmax >= len(list_nrj_setup):
        idxmax = len(list_nrj_setup)-1
    
    print(nrj, "is between", list_nrj_setup[idxmin], "and", list_nrj_setup[idxmax])
    
    list_nrj_setup[idxmin]
    
    
def beam_select_setup():
    
    cc = get_config()
    cc.reload()
    current_nrj = energy.position
    optical_conf = cc.get("optical_setup")
    list_nrj_setup = list(optical_conf.keys())
    list_nrj_setup.remove("name")
    if "selected" in list_nrj_setup:
        list_nrj_setup.remove("selected")
    list_nrj_setup = np.array([float(i) for i in list_nrj_setup])
    list_nrj_setup.sort()
    tab_nrj_kev = np.array([i[3] for i in tab_nrj])

    tab_nrj_choice = [(str(i), "%6s %6s"%(str(i),"-".join(tab_nrj[np.argwhere(tab_nrj_kev==i)[0][0]][0:2]))) for i in list_nrj_setup]
    selected_conf = optical_conf.get("selected")
    defval=0
    for idx in range(len(tab_nrj_choice)):
        if abs(float(tab_nrj_choice[idx][0])-current_nrj)<0.005:
            defval=idx
        if selected_conf is None:
            tab_nrj_choice[idx]= (tab_nrj_choice[idx][0], tab_nrj_choice[idx][1]+"   (interpolate)")
        else:
            current_selected_conf = selected_conf.get(tab_nrj_choice[idx][0])
            if current_selected_conf is None:
                tab_nrj_choice[idx]= (tab_nrj_choice[idx][0], tab_nrj_choice[idx][1]+"   (interpolate)")
            else:
                current_selected_conf_readable = current_selected_conf[:10]+' '+current_selected_conf[11:].replace("-",":")  
                tab_nrj_choice[idx]= (tab_nrj_choice[idx][0], tab_nrj_choice[idx][1]+"   ("+current_selected_conf_readable+")")
        
    dlg3 = UserChoice(label="Choose an available energy :", values=tab_nrj_choice, defval=defval)
    wanted_nrj = BlissDialog( [ [ dlg3 ] ], title='Beam configuration editor', ok_text="Next").show()

    if wanted_nrj is not False:   
        wanted_nrj = list(wanted_nrj.values())[0]
        wanted_nrj_all_values = optical_conf.get(str(wanted_nrj))
        nrj_conf = list(wanted_nrj_all_values)
        nrj_conf.sort(reverse=True)
        defval=0
        tab_nrj_conf_choice=[("interpolate", "interpolate")]
        for conf in nrj_conf:
            if wanted_nrj in selected_conf.keys():
                if conf in selected_conf[wanted_nrj]:
                    defval=nrj_conf.index(conf)+1
            if conf[0:2]=="20":
                commentaire = wanted_nrj_all_values.get(conf).get("comment")
                if not commentaire:
                    commentaire = "no comment"                                
                conf_readable = conf[:10]+' '+conf[11:].replace("-",":")+' ('+commentaire+')'
                tab_nrj_conf_choice.append((str(conf), conf_readable))
            else:
                tab_nrj_conf_choice.append((str(conf), str(conf)))
        dlg3 = UserChoice(label="Choose an available energy :", values=tab_nrj_conf_choice, defval=defval)
        wanted_conf = BlissDialog( [ [ dlg3 ] ], title='Beam configuration editor', ok_text="Select").show()
        if wanted_conf is not False:   
            wanted_conf = list(wanted_conf.values())[0]
            if(selected_conf is None):
                optical_conf["selected"]= { wanted_nrj: wanted_conf }
            else:
                selected_conf[wanted_nrj] = wanted_conf
                selected_conf.save()
            optical_conf.save()







