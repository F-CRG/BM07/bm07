from bliss.common import tango
from time import sleep

def flash_ionic_E1_M1(t_on_in_s, t_off_in_s, t_wait_before_pening_onn_in_s):
    penE1 = tango.DeviceProxy("bm07/pen/11")
    penM1 = tango.DeviceProxy("bm07/pen/21")
    ionicM1_1 = tango.DeviceProxy("bm07/dual-vip/21")
    ionicM1_2 = tango.DeviceProxy("bm07/dual-vip/22")
    ionicE1 = tango.DeviceProxy("bm07/dual-vip/11")
    
    while 1:
        try:
            ionicM1_1.on()
            ionicE1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_on_in_s)

        try:
            ionicM1_1.off()
            ionicE1.off()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))
        
        sleep(t_wait_before_pening_onn_in_s)

        try:
            penE1.on()
            penM1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_off_in_s-t_wait_before_pening_onn_in_s)

        try:
            ionicM1_2.on()
            ionicE1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_on_in_s)

        try:
            ionicM1_2.off()
            ionicE1.off()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))
        
        sleep(t_wait_before_pening_onn_in_s)

        try:
            penE1.on()
            penM1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_off_in_s-t_wait_before_pening_onn_in_s)


def flash_ionic_E1(t_on_in_s, t_off_in_s, t_wait_before_pening_onn_in_s):
    penE1 = tango.DeviceProxy("bm07/pen/11")
    ionicE1 = tango.DeviceProxy("bm07/dual-vip/11")
    
    while 1:
        try:
            ionicE1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_on_in_s)

        try:
            ionicE1.off()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))
        
        sleep(t_wait_before_pening_onn_in_s)

        try:
            penE1.on()
        except Exception as e:
            print('Failed to upload to ftp: '+ str(e))

        sleep(t_off_in_s-t_wait_before_pening_onn_in_s)



