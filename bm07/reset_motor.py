from bliss.common.axis import Axis
import time


def reset_pos_neg(mot):
    mot.hw_limit(-1,wait=True)
    time.sleep(1)
    if mot.hw_state.LIMNEG:
        mot.dial = 0
        mot.position=float(mot.config.get("offset"))
        print("Limit- reach")
        print("set",mot.name,"dial set at 0 and offset at",mot.config.get("offset"))
        print("move to 0")
        mot.move(0,wait=True)
    else:
        print("Fail")

def reset_pos_pos(mot):
    mot.hw_limit(1,wait=True)
    time.sleep(1)
    if mot.hw_state.LIMPOS:
        mot.dial = 0
        mot.position=float(mot.config.get("offset"))
        print("Limit+ reach")
        print("set",mot.name,"dial set at 0 and offset at",mot.config.get("offset"))
        print("move to 0")
        mot.move(0,wait=True)
    else:
        print("Fail")
        
def reset_pos(self):
    if self.name=="omega":
        self.home()
    elif self.config.get("ref") == 'lim-':
        print("Search for lim-")
        reset_pos_neg(self)            
    else:
        print("Search for lim+")
        reset_pos_pos(self)
       
setattr(Axis,'reset_pos',reset_pos)
