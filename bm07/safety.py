from bliss.common.hook import MotionHook
from bliss import global_map
from bliss.setup_globals import *
from bm07.optical_setup import *



class GonioSafety(MotionHook):
    class SafetyError(Exception):
        pass

    def __init__(self):
        super(GonioSafety, self).__init__()
    

    def pre_move(self, motion_list):
        for motion in motion_list:
            target_pos = motion.target_pos/motion.axis.steps_per_unit
            if motion.axis.disabled:
                raise self.SafetyError(
                    f'Cannot move : {motion.axis.name} is disabled')
            if motion.axis.name == "omega" and abs(target_pos-omega.position)>2 and kappa.position > 45:
                raise self.SafetyError(f'Cannot move omega : possible collision due to kappa')
            # if motion.axis.name in ["omega", "y", "x", "z", "xomega", "zomega", "kappa"] and bstop2.state == 'IN':
            #     raise self.SafetyError(f'Cannot move : possible collision due to bstop2')
            if motion.axis.name == "kappa" and (
                                                  not (omega.position > 120 and omega.position < 140) 
                                                  and 
                                                  not (omega.position > -10 and omega.position < 10) 
                                                  ):
                raise self.SafetyError(f'Cannot move : possible collision due to omega')





     # 2023-01-04 Removed by ERM as seems useless now       
    # def pre_move(self, motion_list):
    #     md2.proxy.Flex_TransfertStateIsOn = False
    #     for motion in motion_list:
    #         target_pos = motion.target_pos/motion.axis.steps_per_unit
    #         if motion.axis.disabled:
    #             raise self.SafetyError(
    #                 f'Cannot move : {motion.axis.name} is disabled')
    #         if motion.axis.name in md2.load_motlist.name:
    #             busy_dict.append(motion.axis.name)
    #             md2.proxy.md2_motor_busy = True
    #         if motion.axis.name == "omega" and abs(target_pos-omega.position)>2 and kappa.position > 10:
    #             raise self.SafetyError(f'Cannot move omega : possible collision due to kappa')
    #         if  motion.axis.name in ["omega", "y", "x", "z", "xomega", "zomega", "kappa"] and bstop2.state == 'IN':
    #             raise self.SafetyError(f'Cannot move : possible collision due to bstop2')
    #         if motion.axis.name == "kappa" and (
    #                                             not (omega.position > 120 and omega.position < 140) 
    #                                             and 
    #                                             not (omega.position > -10 and omega.position < 10) 
    #                                             ):
    #             raise self.SafetyError(f'Cannot move : possible collision due to omega')


    # def post_move(self, motion_list):
    #     for motion in motion_list:
    #         if motion.axis.name in self.busy_dict.keys():
    #             self.busy_dict.pop(motion.axis.name)
    #             #print("ici                           " , motion.axis.name)

    #     if len(self.busy_dict)==0:
    #         md2.proxy.md2_motor_busy = False
    #         md2.proxy.Flex_TransfertStateIsOn = md2.is_in_load_position()
    #     else:
    #         md2.proxy.md2_motor_busy = True
    #         md2.proxy.Flex_TransfertStateIsOn = False


    # 2023-01-04 Removed by ERM as seems useless now
# class GroupGonioSafety(MotionHook):
#     class SafetyError(Exception):
#         pass

#     def __init__(self):
#         super(GroupGonioSafety, self).__init__()
    
        
#     def pre_move(self, motion_list):
#         print("pre_move  GroupGonioSafety")

    # def post_move(self, motion_list):
    #     print("post_move  GroupGonioSafety")


class EnergieSafety(MotionHook):
    class SafetyError(Exception):
        pass

    def __init__(self):
        super(EnergieSafety, self).__init__()

    def init(self):
        pass

    def pre_move(self, motion_list):
        for motion in motion_list:
            if motion.axis.disabled:
                raise self.SafetyError(
                    f'Cannot move : {motion.axis.name} is disabled')
            if motion.axis.name == "energy":
                curpos = motion.axis.position
                targetpos = motion.user_target_pos
                print("Energy : current %.4f, wanted %.4f"%(curpos,targetpos))
                if abs(targetpos-curpos)>=0.2:
                    print('Cannot only beta : full energy set launched')
                    #beam_set(targetpos)
                    return

           
def close_FE_on_C1_high_temp(temp_limit=-50):
    C1_temp = tango.DeviceProxy("bm07/temp/mono_c1")
    while C1_temp.value < temp_limit:
        sleep(10)
    frontend.close()

def open_FE_on_C1_low_temp(temp_limit=-120):
    C1_temp = tango.DeviceProxy("bm07/temp/mono_c1")
    while C1_temp.value > temp_limit:
        sleep(10)
    frontend.open()


class DetectorSafety(MotionHook):
    class SafetyError(Exception):
        pass

    def __init__(self):
        super(DetectorSafety, self).__init__()
    

    def pre_move(self, motion_list):
        for motion in motion_list:
            print ("in detfwd hook)")
            target_pos = motion.target_pos/motion.axis.steps_per_unit
            if motion.axis.disabled:
                raise self.SafetyError(f'Cannot move : {motion.axis.name} is disabled')
            if motion.axis.name == "detfwd" and wagoflex.get("RobotParked") == 0 :
                raise self.SafetyError(f'Cannot move detector as robot is not parked')

#GroupGonioSafetyHook = GroupGonioSafety()
# load_motors = Group(*md2.load_motlist)
#load_motors.motion_hooks.append(GroupGonioSafetyHook)

# GonioSafetyHook = GonioSafety()
# omega.motion_hooks.append(GonioSafetyHook)
# kappa.motion_hooks.append(GonioSafetyHook)
# x.motion_hooks.append(GonioSafetyHook)
# y.motion_hooks.append(GonioSafetyHook)
# z.motion_hooks.append(GonioSafetyHook)
# detfwd.motion_hooks.append(GonioSafetyHook)
# xomega.motion_hooks.append(GonioSafetyHook)
# zomega.motion_hooks.append(GonioSafetyHook)
# detcover.motion_hooks.append(GonioSafetyHook)
# cryolong.motion_hooks.append(GonioSafetyHook)
# bstop1.motion_hooks.append(GonioSafetyHook)
# dstop_up_down.motion_hooks.append(GonioSafetyHook)


GonioSafetyHook = GonioSafety()
omega.motion_hooks.append(GonioSafetyHook)
kappa.motion_hooks.append(GonioSafetyHook)
x.motion_hooks.append(GonioSafetyHook)
y.motion_hooks.append(GonioSafetyHook)
z.motion_hooks.append(GonioSafetyHook)
xomega.motion_hooks.append(GonioSafetyHook)
zomega.motion_hooks.append(GonioSafetyHook)



EnergieSafetyHook = EnergieSafety()
energy.motion_hooks.append(EnergieSafetyHook)

DetectorSafetyHook = DetectorSafety()
detfwd.motion_hooks.append(DetectorSafetyHook)

