"""
Inclinometer in 4-20mA control by a 750-473 card
yml configuration example:
- controller:
  plugin: bliss
  name: inclino_m1
  package: bm07.inclino_FIP
  class: inclino_FIP
  wago: $wcd07g
  logical_device : inclinoNew1
  sign: -1
  offset: 0
  calib_A: 0.1244509839
  calib_B: -1.504475499
"""

from bliss.common.logtools import *
import time

class inclino_FIP:
    def __init__(self, name, config):
        self.name = name
        self.wago = config["wago"]
        self.logical_device = config["logical_device"]
        self.sign = config["sign"]
        self.offset = config["offset"]
        self.A = config["calib_A"]
        self.B = config["calib_B"]
        self.channel = self.wago.controller.devname2key(self.logical_device)

    def get_I_corrected(self):
        return round(0.000976562728506*(self.wago.controller.client_read_input_registers(self.channel,"H")& 0xFFF8)-4.81352318359996E-6,6)

    def read(self):
        log_debug(self, "Inclino read()")
        Icorrected = self.get_I_corrected()
        return round(self.sign*(self.A*Icorrected+self.B)+self.offset,6)

    def __info__(self):
        register = self.wago.controller.client_read_input_registers(self.channel,"H")
        X = self.wago.controller.client_read_input_registers(self.channel,"H") & 0x04
        F = self.wago.controller.client_read_input_registers(self.channel,"H") & 0x02
        U = self.wago.controller.client_read_input_registers(self.channel,"H") & 0x01
        raw = self.wago.controller.client_read_input_registers(self.channel,"H") & 0xFFF8
        info_string = f"'{self.name}` Inclino_FIP from wago:\n"
        info_string += f"  Wago = {self.wago.name}\n"
        info_string += f"  Logical device = {self.logical_device}\n"
        info_string += f"  Calib = {self.A}*I + {self.B}\n"
        info_string += f'  Sign = {self.sign}\n'
        info_string += f'  Offset = {self.offset}\n'
        info_string += f"  register value = {register}\n"
        info_string += f"  X (Sensor supply short circuit) = {X}\n"
        info_string += f"  F (Input short circuit or wire break) = {F}\n"
        info_string += f"  U (Input overflow) = {U}\n"
        info_string += f"  raw value = {raw}\n"
        info_string += f"  I corrected = {self.get_I_corrected()} mA\n"
        info_string += f"  value = {self.read()} deg\n"


        return info_string