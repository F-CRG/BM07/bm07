from gevent import socket, ssl, spawn
from gevent.event import Event
from gevent.server import StreamServer
from functools import partial
from gevent.pool import Pool
from bliss.comm.modbus import ModbusTCP
from time import sleep
from bliss.common import tango
import untangle
import gevent
import time
from bliss.common.motor_group import Group
from bliss.config.static import get_config
#from bm07.optical_scripts import *

from numpy import random




class MD2_exporter_FIP:
    def __init__(self, name, config):
        self.name = name
        self.proxy = tango.DeviceProxy(config["tango_proxy"])
        try :
            self.robotModbus = ModbusTCP(url=config["robot_modbusURL"])
            self.set_airOk(1)
            self.set_LN2Ok(0)
            #self.set_LN2Level(0)
        except:
            print("failed to connect", config["robot_modbusURL"])

        self._old_shortpos = None
        bliss_config = get_config()
        self._wcd07f = bliss_config.get("wcd07f")
        self.omega = config["omega"]
        self.xomega = config["xomega"]
        self.zomega = config["zomega"]
        self.x = config["x"]
        self.y = config["y"]
        self.z = config["z"]
        self.kappa = config["kappa"]
        self.phi = config["phi"]
        self.detfwd = config["detfwd"]

        possible_motlist =  [   self.omega,
                                self.xomega,
                                self.zomega,
                                self.x,
                                self.y,
                                self.z,
                                self.kappa,
                                self.phi,
                                self.detfwd]

                                        
        self.detcover = config["detcover"]
        self.bstop1 = config["bstop1"]
        #self.bstop2 = config["bstop2"]
        self.backlight = config["backlight"]
        # self.cryoshort = config["cryoshort"]
        # self.cryolong = config["cryolong"]
        self.cryomove = config["cryomove"]
        self.cryoicepap = config["cryoicepap"]
        self.movecryoreq = False
        self.detfwd = config["detfwd"]
        self.mca = config["mca"]
        self.dstop = config["dstop"]
        self.actuatorlist = [   self.detcover,
                                self.bstop1,
                                #self.bstop2,
                                self.backlight,
                                # self.cryoshort,
                                # self.cryolong,
                                # self.cryomove,
                                self.mca,
                                self.dstop ]

        goniogeo = untangle.parse(config["xml_geo"])
        self.load_pos = {}
        self.collect_pos = {}
        self.load_motlist = []
        self.collect_motlist = []
        for device in goniogeo.config.load.children:
            self.load_pos[device._name] = device.cdata
            found=False
            for mot in possible_motlist:
                if mot.name == device._name:
                    self.load_motlist.append(mot)
                    found = True
            for act in self.actuatorlist:
                if act.name == device._name:
                    found = True
            if not found:
                print("Error :", device._name, "is not in the list of motors to adjust")
                return

        for device in goniogeo.config.collect.children:
            self.collect_pos[device._name] = device.cdata
            found=False
            for mot in possible_motlist:
                if mot.name == device._name:
                    self.collect_motlist.append(mot)
                    found = True
            for act in self.actuatorlist:
                if act.name == device._name:
                    found = True
            if not found:
                print("Error :", device._name, "is not in list of motors to adjust")
                return

        gevent.spawn(self.checkLoadPosition)
        gevent.spawn(self.moveCryoShort)
        self.event_id = 0

        self.is_init = True
        self.is_initcryomove = True
        self.proxy.subscribe_event("LoadStateReq", tango.EventType.CHANGE_EVENT, self.tango_call_back_to_chan, [])
        self.proxy.subscribe_event("UnloadStateReq", tango.EventType.CHANGE_EVENT, self.tango_call_back_to_chan, [])
        self.event_id = self.proxy.subscribe_event("MoveCryoBackReq", tango.EventType.CHANGE_EVENT, self.askmoveCryoShort, [])
        

        

        #gevent.spawn(self.callback_manuel)

    # def callback_manuel(self):
    #     old_LoadStateReq   = self.proxy.loadstatereq
    #     old_UnloadStateReq = self.proxy.unloadstatereq
    #     while True:
    #         if self.proxy.loadstatereq != old_LoadStateReq:
    #             value = self.proxy.loadstatereq
    #             if value == True:
    #                 print("Ask MD2 in load position")
    #                 self.move_in_load_position()
    #             else:
    #                 print("Ask MD2 in collect position")
    #                 self.move_in_collect_position()
    #             old_LoadStateReq   = self.proxy.loadstatereq

    #         elif self.proxy.unloadstatereq != old_UnloadStateReq:
    #             value = self.proxy.unloadstatereq
    #             if value == True:
    #                 print("Ask MD2 in load position")
    #                 self.move_in_load_position()
    #             else:
    #                 print("Ask MD2 in collect position")
    #                 self.move_in_collect_position()
    #             old_UnloadStateReq = self.proxy.unloadstatereq
    #         gevent.sleep(0.5)


    def tango_call_back_to_chan(self, ev):
        if self.is_init:
            self.is_init = False
        else:
            name = ev.attr_value.name
            value = ev.attr_value.value
            #print("tango_call_back_to_chan", name, value)
            if name == "LoadStateReq".lower() or name == "UnLoadStateReq".lower():
                if value is True:
                    print("Ask MD2 in load position")
                    self.move_in_load_position()
                else:
                    print("Load or Unload request removed")
                    if ((self.proxy.LoadStateReq == False) and (self.proxy.UnloadStateReq == False)):
                        print("Ask MD2 in collect position")
                        self.move_in_collect_position()

    def _is_in_position(self, posdict, motlist, verbose=False):
        motOk = self._motors_are_in_position(posdict, motlist, verbose=verbose)
        actOk = self._actuators_are_in_position(posdict, verbose=verbose)
        if verbose:
            print("motOk=",motOk,  "actOk=",actOk)
        return  motOk and actOk


    def is_in_load_position(self, verbose=False):
        return self._is_in_position(self.load_pos, self.load_motlist, verbose=verbose)
 
    def is_in_collect_position(self, verbose=False):
        return self._is_in_position(self.collect_pos, self.collect_motlist, verbose=verbose)


    def _motors_are_in_position(self, posdict, motlist, verbose=False):
        all_position = True             
        for mot in motlist:
            if mot.name in posdict.keys():
                if abs(mot.position-float(posdict[mot.name]))>0.1:
                    if verbose:
                        print(mot.name, "not in position for loading (",mot.position, "!=",posdict[mot.name], ")")
                    all_position = False
        return all_position

    def motors_are_in_load_position(self, verbose=False):
        return self._motors_are_in_position(self.load_pos, self.load_motlist, verbose=verbose)

    def motors_are_in_collect_position(self, verbose=False):
        return self._motors_are_in_position(self.collect_pos, self.collect_motlist, verbose=verbose)

    def _actuators_are_in_position(self, posdict, verbose=False):
        all_position = True      
        for actuat in self.actuatorlist:
            if actuat.name in posdict.keys():
                # if (actuat.name == "cryomove"):
                #     if (actuat.position == posdict[actuat.name]):
                #         pass
                #     else:
                #         if verbose:
                #             print(actuat.name, "not in position for loading (",'%s'%posdict[actuat.name], "is False )")
                #         all_position = False
                # else:
                if getattr(actuat, 'is_%s'%posdict[actuat.name])() == False:
                    if verbose:
                        print(actuat.name, "not in position for loading (",'is_%s'%posdict[actuat.name], "is False )")
                    all_position = False
        return all_position

    def actuators_are_in_load_position(self, verbose=False):
        return self._actuators_are_in_position(self.load_pos, verbose=verbose)

    def actuators_are_in_collect_position(self, verbose=False):
        return self._actuators_are_in_position(self.collect_pos, verbose=verbose)


    def _move_motors_in_position(self, posdict, motlist):
        print("moving motors in position")
        self.omega.sync_hard()
        for mot in motlist:
            if mot.name in posdict.keys():
                target = posdict[mot.name]
                if mot.name=="kappa" and target==0 :
                    print("reset kappa pos")
                    mot.reset_pos()
                else:
                    mot.sync_hard()
                    print("moving motor: ", mot.name)
                    mot.move(target,wait=False)
                
        self.proxy.md2_motor_busy=True        
        notok=True
        with gevent.Timeout(60,False):
            while notok:
                allok=True
                for mot in motlist:
                    if mot.is_moving:
                        allok=False
                notok=not(allok)
                time.sleep(1)
                    
        if notok :
            return False
        else:
            self.proxy.md2_motor_busy=False
            return self._motors_are_in_position(posdict, motlist)

    def _move_motors_in_load_position(self):
        return self._move_motors_in_position(self.load_pos, self.load_motlist)

    def _move_motors_in_collect_position(self):
        return self._move_motors_in_position(self.collect_pos, self.collect_motlist)

    def _move_actuators_in_position(self, posdict):
        for actuat in self.actuatorlist:
            if actuat.name in posdict.keys(): 
                # if actuat.name == "cryomove" :
                #     target = posdict[actuat.name]
                #     if (actuat.position == posdict[actuat.name]):
                #         pass
                #     else:     
                #         try:
                #             getattr(actuat, '%s'%target)()
                #         except:
                #             print("Error when moving", actuat.name)
                # else:
                target = posdict[actuat.name]
                if getattr(actuat, 'is_%s'%target)() == False:       
                    try:
                        getattr(actuat, '%s'%target)()
                    except:
                        print("Error when moving", actuat.name)
        return self._actuators_are_in_position(posdict)


    def _move_actuators_in_load_position(self):
        return self._move_actuators_in_position(self.load_pos)

    def _move_actuators_in_collect_position(self):
        return self._move_actuators_in_position(self.collect_pos)


    def move_in_load_position(self):
        return self._move_motors_in_load_position() and self._move_actuators_in_load_position()

    def move_in_collect_position(self):
        return self._move_motors_in_collect_position() and self._move_actuators_in_collect_position()


    def set_LN2Level(self, pourcent):
        self.robotModbus.write_register(24, "H", int(290*pourcent))
        if pourcent < 5:
            self.set_robotModbus_val(46,0)
        else:
            self.set_robotModbus_val(46,1)
        if pourcent > 100:
            self.set_robotModbus_val(45,1)
        else:
            self.set_robotModbus_val(45,0)
        

    def set_airOk(self, value):
        self.set_robotModbus_val(55,value)

    def set_LN2Ok(self, value):
        self.set_robotModbus_val(56,value)

    def set_robotModbus_val(self,add, value):
        return self.robotModbus.write_coil(add, value)

    def get_robotModbus_val(self, add):
        return self.robotModbus.read_coils(add, 1)[0]

    def checkLoadPosition(self):
        load_motors = Group(*self.load_motlist)
        collect_motors = Group(*self.collect_motlist)

        oldmoving = None
        oldcheckactuator = None
        while True:
            moving = load_motors.is_moving or collect_motors.is_moving

            if moving != oldmoving:
                if moving:
                    self.proxy.Flex_TransfertStateIsOn = False
                    self.proxy.md2_motor_busy = True
                    print ("MD2 moving...")
                    #print ("is in load position? ", self.is_in_load_position())
                else:
                    self.proxy.md2_motor_busy = False
                oldmoving = moving
                
            checkactuator = self.is_in_load_position()
            self.proxy.Flex_TransfertStateIsOn = checkactuator
            if checkactuator != oldcheckactuator :
                if checkactuator: 
                    print ("MD2 ready, ..............   IN load position")
                    # fastshutmot.homing() #good opportunity to do a quick homing of the fastshutter while the detector is closed
                else:
                    print ("MD2 ready, ..............   NOT in load position")
                oldcheckactuator = checkactuator
            gevent.sleep(0.5)

    def askmoveCryoShort(self, ev):
        self.movecryoreq = True
        #print ("--------------------->", ev)

    def moveCryoShort(self):

        while True:
            if self.movecryoreq == True:
                #print ("MoveCryoBackReq changed status")
                #name = ev.attr_value.name
                #value = ev.attr_value.value
                #print ("event ID: %s"%self.event_id)
                #if name == "MoveCryoBackReq".lower():
                #if value is True:
                if self.proxy.MoveCryoBackReq == True:
                    if ("READY" not in self.cryoicepap.state.current_states() or self.cryoicepap.is_moving):
                        print(f"cryomove state is {self.cryoicepap.state}")
                    else:
                        #print("Ask cryoshort up")
                        self.cryoicepap.sync_hard()
                        # print("cryoicepap velocity: %s"%(self.cryomove.cryoicepap.velocity))
                        try:
                            self.cryomove.shortup()
                        except:
                            print ("Motor cryomove.shortup busy")
                else:
                    if ("READY" not in self.cryoicepap.state.current_states() or self.cryoicepap.is_moving):
                        print(f"cryomove state is {self.cryoicepap.state}")
                    else:
                        #print("Ask cryoshort down")
                        self.cryoicepap.sync_hard()
                        try:
                            self.cryomove.down()
                        except:
                            print ("Motor cryomove.down busy")
                self.movecryoreq = False
            gevent.sleep(0.2)

    def beam_align_using_yag1(self):
        print("doing quick beam alignement")
        beam_align_using_yag()

