from bliss.controllers.wago.wago import Wago as Wago_bliss
from bliss.controllers.wago.wago import MODULES_CONFIG, ModConf


MODULES_CONFIG.update({"750-881": [0, 0, 0, 0, 2, 2, "cpu", "Wago PLC Ethernet/IP"]})
MODULES_CONFIG.update({"750-362": [0, 0, 0, 0, 2, 2, "cpu", "Wago PLC Ethernet/IP"]})
MODULES_CONFIG.update({"750-852": [0, 0, 0, 0, 2, 2, "cpu", "Wago PLC Ethernet/IP"]})  #ce qui est lu pour le 5109-8888
MODULES_CONFIG.update({"750-458": [0, 0, 8, 0, 2, 2, "thc", "8 Channel Thermocouple Input"],})
MODULES_CONFIG.update({"750-511": [0, 0, 0, 2, 2, 2, "fs10", "2 PWM output"],})
MODULES_CONFIG["750-458"] = ModConf(*MODULES_CONFIG["750-458"], {}, {})
MODULES_CONFIG["750-511"] = ModConf(*MODULES_CONFIG["750-511"], {}, {})


class Wago(Wago_bliss):
    def __init__(self, name, config_tree):
        Wago_bliss.__init__(self, name, config_tree)
