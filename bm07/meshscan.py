import numpy as np
from bliss.physics import trajectory
from bliss.common import axis
from bliss.common.motor_group import TrajectoryGroup
from bliss.setup_globals import *
import matplotlib.pyplot as plt

class md2_mesh(object):
    def __init__(self, name, config, config_objects=None):
        self.x_traj=None
        self.y_traj=None
        self.group=None
        self.x_mot = config.get("x_mot")
        self.y_mot = config.get("y_mot")



    def create_traj(self, p_start, dx, dy, nb_line, nb_col, total_time):

        col_points = np.linspace(0, nb_col-1, nb_col)
        line_points = np.linspace(0, nb_line-1, nb_line)

        x_points, y_points = np.meshgrid(col_points, line_points)
        for i in range(1, len(line_points), 2):
            x_points[i]=np.flip(x_points[i])

        t_points = np.linspace(0, total_time, nb_line*nb_col)
        x_points = x_points.flatten()
        y_points = y_points.flatten()

        
        sec_mm = total_time/(dx*(nb_col-1)*nb_line+dy*(nb_line-1))

        x_finals = [x_points[0]-0.1, x_points[0]]
        y_finals = [y_points[0], y_points[0]]
        t_finals = [-0.2*sec_mm, 0]

        for i in range(1, len(x_points)-1):
            deltax = x_points[i] - x_points[i-1]
            if deltax==0:
                x_finals.append(x_finals[-1])
                y_finals.append(y_points[i])
                t_finals.append(t_finals[-1]+0.1*sec_mm)

            x_finals.append(x_points[i])
            y_finals.append(y_points[i])
            t_finals.append(t_points[i])

            if deltax!=0 and y_points[i+1]!=y_points[i]:
                x_finals.append(x_points[i]+0.2)
                y_finals.append(y_points[i])
                t_finals.append(t_finals[-1]+0.1*sec_mm)

        x_finals.append(x_points[-1])
        y_finals.append(y_points[-1])
        t_finals.append(t_points[-1])

        if nb_col%2==0:
            x_finals.append(x_points[-1]-0.2)
        else:
            x_finals.append(x_points[-1]+0.2)
        y_finals.append(y_points[-1]) 
        t_finals.append(t_points[-1]+0.2*sec_mm)
        print(y_finals)

        x_points = [p_start[0]+el*dx for el in x_finals]
        y_points = [p_start[1]-el*dy for el in y_finals]
        t_points = [el-t_finals[0] for el in t_finals]

        for i in range(0,len(x_points)):
            print("(%.4f, %.4f) t="%(x_points[i],y_points[i]), t_points[i])
  


        pt = trajectory.PointTrajectory()
        pt.build(t_points,{self.x_mot.name:x_points, self.y_mot.name:y_points})
        pvt = pt.pvt()

        self.x_traj = axis.Trajectory(self.x_mot, pvt[self.x_mot.name])
        self.y_traj = axis.Trajectory(self.y_mot, pvt[self.y_mot.name])

    def prepare_move(self):
        self.group = TrajectoryGroup(self.x_traj, self.y_traj)
        self.group.prepare()
        self.group.move_to_start()

    def move(self):
        self.group.move_to_end()

        