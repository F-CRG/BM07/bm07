"""
Procedure to find the max attenuation on a sample, while the signal from
the flourescence detector is within specified min and max integrated counts,
for a ROI between a configurable min and max energy, for a given counting time.

yml configuration example:
name: find_max_attenuation
plugin: default
diffractometer: $diffractometer
mca: $mca
transmission: $transmission
safshut: $safshut
integrated_counts: [2000, 50000]
transmission_table: [0.1, 0.2, 0.3, 0.9, 1.3, 1.9, 2.6, 4.3, 6, 8, 12, 24, 36, 50, 71]
"""
import logging
import gevent
from bliss.config import static
from bliss.common.cleanup import cleanup, error_cleanup
from bliss.setup_globals import *

class FindMaxAttenuation:
    """ Find maximum attenuation """

    def __init__(self):
        #cfg = static.get_config()
        #config = cfg.get("find_max_attenuation")
        #self.safshut = config.get("safshut")
        #self.fastshutmot = config.get("fastshutmot")
        #self.mca = config.get("mca")
        #self.mca_in_out = config.get("mca_in_out")
        #self.bstop = config.get("bstop")
        #self.transmission = config.get("transmission")
        #self.transmission_table = config.get("transmission_table", [])
        self.transmission_table = [0.1, 0.2, 0.3, 0.9, 1.3, 1.9, 2.6, 4.3, 6, 8, 12, 24, 36, 50, 71, 100]
        self.t_table = []
        #self.integrated_counts = config.get("integrated_counts", [])
        self.integrated_counts = [50000, 150000]  #[10000, 50000] #[1000, 5000]
        self.roi = []
        self.datafile = None
        self.initial_transmission = 0
        self.ctime = 3  # default count time [s]
        self.misc_roi_params = ["Se", 34]  # default roi element

    def init(self, **kwargs):
        """Initialise the procedure
        Kwargs:
            ctime(float): count time [s]
            integrated_counts (list): minimum and maximum mca counts value
            roi (list): mca ROI minimum and maximum energy [keV]
            datafile (string): file to keep the data - full path
            transmission_table (list): list of transmission values to check
            misc_roi_params (list): some roi parameters
        """

        if not safshut:
            logging.getLogger().debug("Safety shutter will not be handled")

        # count time in s
        self.ctime = kwargs.get("ctime", 3.0)

        # filename if saving the raw data wanted
        self.datafile = kwargs.get("datafile")

        # get the mca roi and set the values in keV
        self.roi = []
        roi = kwargs.get("roi", [2.0, 15.0])
        for i in roi:
            if i > 1000:
                i /= 1000.0
            self.roi.append(i)
        self.roi.sort()

        self.misc_roi_params = kwargs.get("misc_roi_params", ["Se", 34])

        integrated_counts = kwargs.get("integrated_counts")
        if integrated_counts:
            self.integrated_counts = integrated_counts

        if not self.integrated_counts:
            msg = "Minimum and/or maximum integrates counts value not set"
            logging.getLogger("user_level_log").exception(msg)
            raise RuntimeError(msg)
        self.integrated_counts.sort()

        self.t_table = kwargs.get("transmission_table", self.transmission_table)

    def _prepare(self, ctime=None, fname=None, roi=None):
        if ctime:
            self.ctime = ctime
        if roi:
            self.roi[0]=roi[0]
            self.roi[1]=roi[1]
        if fname:
            self.datafile=fname
        # set the mca
        mca.set_roi(
            self.roi[0],
            self.roi[1],
            channel=1,
            element=self.misc_roi_params[0],
            atomic_nb=self.misc_roi_params[1],
        )
        mca.set_presets(erange=1, ctime=self.ctime, fname=self.datafile)

        # open safety shutter (just in case)
        if safshut:
           safshut.open()

        # put the fluorescent detector close to the sample
        mca_in_out.insert()
        gevent.sleep(1)

    def _cleanup(self):
        fastshutmot.open_manual()

    def _error_cleanup(self):
        self._cleanup()
        mca_in_out.remove()
        if safshut:
            fastshutmot.close_manual()

    def _procedure(self, ctime=None, fname=None, roi=None):
        """ The procedure itself """
        if ctime:
            self.ctime = ctime
        if roi:
            self.roi[0]=roi[0]
            self.roi[1]=roi[1]
        if fname:
            self.datafile=fname
        def restore_transmission(old_transmission=transmission.get()):
            transmission.set(old_transmission)

        with cleanup(self._cleanup):
            # set the maximum attenuation
            transmission.set(0)
            logging.getLogger("user_level_log").info(
                "Choosing maximum attenuation, please wait"
            )

            # open the fast shutter
            fastshutmot.open_manual()

            with error_cleanup(self._error_cleanup, restore_transmission):
                # start the acquisition
                mca.start_acq(30)
                gevent.sleep(1.5)
                # save data if needed
                save_data = False
                if self.datafile:
                    save_data = True
                    logging.getLogger("user_level_log").info(
                        "Raw data in " + self.datafile
                    )

                # read the integrated counts
                #i_c = sum(mca.read_roi_data(save_data)) // self.ctime
                i_c = mca.read_chrate(1)
                logging.getLogger("user_level_log").debug(
                    "Integrated counts/s: %g" % i_c
                )
                if i_c > self.integrated_counts[1]:
                    fastshutmot.close_manual()
                    msg = "The detector is saturated, giving up."
                    logging.getLogger("user_level_log").exception(msg)
                    raise RuntimeError(msg)

                for t_m in self.t_table:
                    mca.clear_spectrum()
                    logging.getLogger("user_level_log").debug(
                        "Setting transmission to %f" % t_m
                    )
                    transmission.set(t_m)
                    gevent.sleep(1.5)
                    #i_c = sum(mca.read_roi_data()) // self.ctime
                    i_c = mca.read_chrate(1)
                    #if t_m >0.2:
                    #    i_c = 500
                    print("Integrated counts/s: %d" % i_c)
                    logging.getLogger("user_level_log").debug(
                        "Integrated counts/s: %g" % i_c
                    )
                    if i_c > self.integrated_counts[0] or t_m == self.t_table[-1]:
                        t_m = transmission.get()
                        logging.getLogger("user_level_log").info(
                            "Transmission chosen: %.3f (%d counts/s)" % (t_m, i_c)
                        )
                        mca.stop_acq()
                        mca.clear_spectrum()
                        mca.start_acq(self.ctime+1)
                        gevent.sleep(self.ctime+1)
                        #mca.stop_acq()
                        fastshutmot.close_manual()
                        
                        return t_m

                #if i_c < self.integrated_counts[0]:
                #    msg = "Could not find satisfactory attenuation (is the mca properly set up?), giving up."
                #    logging.getLogger("user_level_log").exception(msg)
                #    raise RuntimeError(msg)
        return False

    def find_max_attenuation(self, **kwargs):
        """ The whole sequence of finding maximum attenuation """
        self.init(**kwargs)
        self._prepare(**kwargs)
        return self._procedure(**kwargs)


find_max_attenuation = FindMaxAttenuation().find_max_attenuation