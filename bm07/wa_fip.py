from bliss.shell.standard import _MAX_COLS, _ERR, _tabulate, _print_errors_with_traceback
from bliss.common.utils import ErrorWithTraceback
from bliss import global_map, global_log

def wa(**kwargs):
    """
    Displays all positions (Where All) in both user and dial units
    """
    max_cols = kwargs.get("max_cols", _MAX_COLS)
    err = kwargs.get("err", _ERR)

    print("Current Positions: user")
    print("                   dial")
    header, pos, dial, inclino = [], [], [], []
    tables = [(header, pos, dial, inclino)]
    errors = []
    try:
        inclino_m1 = None
        inclino_m2 = None
        for counter in global_map.get_counters_iter():
            if counter.name == "inclino_m1":
                inclino_m1 = counter
            elif counter.name == "inclino_m2":
                inclino_m2 = counter
        for (
            axis_name,
            position,
            dial_position,
            axis_unit,
        ) in global_map.get_axes_positions_iter(
            on_error=ErrorWithTraceback(error_txt=err)
        ):
            if len(header) == max_cols:
                header, pos, dial, inclino = [], [], [], []
                tables.append((header, pos, dial, inclino))
            axis_label = axis_name
            if axis_unit:
                axis_label += "[{0}]".format(axis_unit)
            header.append(axis_label)

            pos.append(position)
            dial.append(dial_position)
            
            if axis_name == "alpha1" and inclino_m1 is not None:
                inclino.append("inc="+str(inclino_m1.value))
            elif axis_name == "alpha2" and inclino_m2 is not None:
                inclino.append("inc="+str(inclino_m2.value))
            else:
                inclino.append("")

            if _ERR in [str(position), str(dial_position)]:
                errors.append((axis_label, dial_position))

        for table in tables:
            print("")
            print(_tabulate(table))

        _print_errors_with_traceback(errors, device_type="motor")

    finally:
        errors.clear()

def apply_config_all_motors():
    for mot in global_map.get_axes_iter():
        if hasattr(mot, 'apply_config'):
            print("Reload config for mot =",mot.name)
            try:
                mot.apply_config(reload="True")
            except Exception as e:
                print(str(e))