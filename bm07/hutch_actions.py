import logging
from pickle import loads
from base64 import b64decode
from gevent import Timeout, sleep
from bliss.config import static
from bliss.setup_globals import *
import time

def diffractometer_wait_ready(timeout):
    start = time.time()
    while time.time()-start < timeout:
        if md2.proxy.md2_motor_busy == False:
            break
        time.sleep(1)


def hutch_actions(enter=True, **kwargs):
        """Actions to execute when enter/exit hutch or load/unload sample
        Args:
            enter (bool): Enter (True) or interlock (False) the hutch.
        Kwargs:
            release_interlock (bool): Release the hutch interlock. Default False
            hutch_trigger (bool): state of the hutch trigger. Default False
            sc_loading (bool): Sample changed to load. Default False
            phase (str): Diffractometer action ("centring", "loading").
        """
        release_interlock = kwargs.get("release_interlock", False)
        hutch_trigger = kwargs.get("hutch_trigger", False)
        sc_loading = kwargs.get("sc_loading", False)
        phase = kwargs.get("phase", "centring")

        if release_interlock:
            logging.getLogger("user_level_log").info("release_interlock")
            # self.detector_motor.on()
            return

        if enter:
            if hutch_trigger:
                logging.getLogger("user_level_log").info(
                    "Prepare diffractometer for loading"
                )
            #    self.diffractometer.set_phase("Transfer", timeout=0)

            # self.protect_detector(40)
            # self.diffractometer.wait_ready(40)
        else:
            if hutch_trigger:
                #self.diffractometer.wait_ready(1200)
                diffractometer_wait_ready(60)

                if phase == "loading":
                    logging.getLogger("user_level_log").info(
                        "Prepare diffractometer for loading a sample"
                    )
                    #self.diffractometer.set_phase("Transfer")
                elif phase == "centring":
                    logging.getLogger("user_level_log").info(
                        "Prepare diffractometer for centring"
                    )
                    #self.diffractometer.set_phase("Centring", timeout=0)

            if sc_loading or (hutch_trigger and phase == "centring"):
                # self.detector_motor.off()
                with Timeout(10, RuntimeError("Sample Changer not in safe position")):
                    #if not self.robot_is_safe():
                    if not wagoflex.get("RobotParked"):
                        logging.getLogger("user_level_log").info("Flex not ready")
                        sleep(0.2)
                # self.detector_motor.on()
                #self.prepare_detector()
            if hutch_trigger:
                logging.getLogger("user_level_log").info("hutch_trigger only")
                #self.diffractometer.wait_ready(40)
                diffractometer_wait_ready(60)