from bliss.setup_globals import *
import time

def acq_externalSingle(expo_s, step_deg, start_angle_deg, end_angle_deg):
    readout = pilatus_6m.proxy.latency_time

    velocity = step_deg/(expo_s+readout)
    nb_img = int((end_angle_deg-start_angle_deg)/step_deg)

    omega.sync_hard()
    #bac24.init()

    pilatus_6m.proxy.reset()
    #pilatus_6m.camera.threshold=energy.position
    pilatus_6m.camera.fill_mode = 'ON'
    pilatus_6m.acquisition.trigger_mode = "EXTERNAL_TRIGGER"
    pilatus_6m.proxy.saving_mode = "AUTO_FRAME"
    pilatus_6m.acquisition.nb_frames = nb_img
    pilatus_6m.proxy.acq_expo_time = expo_s
    pilatus_6m.proxy.saving_overwrite_policy = "OVERWRITE"


    pilatus_6m.proxy.saving_directory="/lima_data/pics"
    pilatus_6m.proxy.saving_prefix="det%d_"%round(detfwd.position)#pilatus_6m_"
    pilatus_6m.proxy.saving_suffix=".cbf"
    pilatus_6m.proxy.saving_index_format="%04d"
    pilatus_6m.proxy.saving_format="CBF"
    pilatus_6m.proxy.saving_header_delimiter=["|", ";", ":"]
    pilatus_6m.proxy.saving_next_number =0

    headers = list()
    start_angles = list()
    headercommon = {}

    headercommon["file_comments"] = ""
    headercommon["N_oscillations"] = nb_img
    headercommon["Oscillation_axis"] = "omega"
    headercommon["Beam_xy"] = "(%.2f, %.2f) pixels" % (pilatus_6m.image.width/2, pilatus_6m.image.height/2)
    headercommon["Detector_distance"] = "%f m" % ( detfwd.position / 1000.0)
    headercommon["Wavelength"] = "%f A" % energy.controller._tagged["wavelength"][0].position
    headercommon["Threshold_setting"] = "%d eV" % pilatus_6m.camera.threshold    
    headercommon["Count_cutoff"] = "1048500"
    headercommon["Tau"] = "= 0 s"
    headercommon["Exposure_period"] = "%f s" % (expo_s + readout)
    headercommon["Exposure_time"] = "%f s" % expo_s


    for i in range(nb_img):
        start_angles.append("%0.4f deg." % (start_angle_deg + step_deg * i))
    for i, start_angle in enumerate(start_angles):
        header = "\nPILATUS 6M, S/N 60-0106, Soleil\n" 
        header += "# %s\n" % time.strftime("%Y/%b/%d %T")
        header += "\nSilicon sensor, thickness 0.000320 m\n"
        header += "\nPixel_size 172e-6 m x 172e-6 m\n"
        headercommon["Start_angle"] = start_angle

        for key, value in headercommon.items():
            header += "# %s %s\n" % (key, value)

        headers.append("%d : array_data/header_contents|%s;" % (i, header))
    pilatus_6m.proxy.setImageHeader(headers)



    omega.velocity = 20
    umv(omega,start_angle_deg-1)
    omega.velocity = velocity

    bac24.ramp(start_angle_deg,end_angle_deg)

    pilatus_6m.stopAcq()
    pilatus_6m.prepareAcq()
    pilatus_6m.startAcq()

    time.sleep(0.5)
    mv(omega, end_angle_deg+1)
    bac24.zero()
    omega.velocity=20
    umv(omega,0)
    

def pico_moyenne(seconde):
    import tango
    pico = tango.DeviceProxy('bm07/pico/oh')
    pico.StartT(seconde*1000)
    time.sleep(seconde+0.5)
    dstopMoyenne = pico.ReadIBuffer()[4]/pico.ReadIBuffer()[1]
    dMono1Moyenne = pico.ReadIBuffer()[2]/pico.ReadIBuffer()[1]
    dMono2Moyenne = pico.ReadIBuffer()[3]/pico.ReadIBuffer()[1]
    return [dstopMoyenne, 0.5*(dMono1Moyenne+dMono2Moyenne)]

def nrjcal_vYo(eMin, eMax, eStep, tMoy):
    import numpy as np
    txt="energy beta dstop dmono\n"
    for e in np.arange(eMin,eMax,eStep):
        energy.move(e)
        counts = pico_moyenne(tMoy)
        txt+="%f %f %f %f\n"%(energy.position, beta.position, counts[0], counts[1])
        print(txt)


def read_mca_p0():
    mca.mca.times['real_t_preset']=3
    mca.start_acq()
    sleep(1)
    resp = mca.mca.sl.write_readline(b"$GR 1\r")
    return int(resp[3:])



def nrjcal_vYo2(eMin, eMax, eStep, tMoy):
    import numpy as np
    mca.mca.times['real_t_preset']=tMoy
    mca.set_roi(2.0,15.0,channel=1,element="Se",atomic_nb=34)
    txt="energy beta dstop dmono\n"
    for e in np.arange(eMin,eMax,eStep):
        energy.move(e)
        mca.start_acq()
        counts = pico_moyenne(tMoy)
        resp = mca.mca.sl.write_readline(b"$GR 1\r")
        p0 = int(resp[3:])
        txt+="%f %f %f %f %f\n"%(energy.position, beta.position, p0,counts[0], counts[1])
        print(txt)