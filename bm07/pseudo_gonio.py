#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bliss.controllers.motor import CalcController,Controller
from bliss.config.static import get_config
from bliss.common import event
#from bm07 import grob_FIP
import math

class pseudo_gonio(CalcController):
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.omega = None
        self.x = None
        self.z = None
        self.omega = None
        self._pos_dict = {}

    def initialize_axis(self, axis):
        CalcController.initialize_axis(self, axis)
        self.omega = self._tagged.get("omega", [None])[0]
        self.x = self._tagged.get("x", [None])[0]
        self.z = self._tagged.get("z", [None])[0]
        axis.no_offset = True

    def calc_from_real(self, reals_dict):
        self._pos_dict = reals_dict  # used to store ALL motors positions
        pseudos_dict = dict()
        omegarad = -math.radians(reals_dict["omega"])
        #print("omega=",reals_dict["omega"], self.omega.position)
        horizontalpos = reals_dict["x"] * math.cos(omegarad) + reals_dict["z"] * math.sin(omegarad)
        verticalpos = -reals_dict["x"] * math.sin(omegarad) + reals_dict["z"] * math.cos(omegarad)
        pseudos_dict.update({"sample_vertical": (verticalpos)})
        pseudos_dict.update({"sample_horizontal": (horizontalpos)})
        self._pos_dict.update(pseudos_dict)
        return pseudos_dict

    def calc_to_real(self, pseudos_dict2):
        reals_dict = dict()

        omegarad = -math.radians(self.omega.position)
        verticalpos = pseudos_dict2["sample_vertical"]
        horizontalpos = pseudos_dict2["sample_horizontal"]
        #verticalpos = self._pos_dict["sample_vertical"]
        #horizontalpos = self._pos_dict["sample_horizontal"]
        
        print("verticalpos=",verticalpos,"horizontalpos=", horizontalpos)
        print(self._pos_dict)
        print("verticalpos=",verticalpos,"horizontalpos=", horizontalpos, "cos=",math.cos(-omegarad), "sin=", math.sin(-omegarad))
        x = horizontalpos * math.cos(omegarad) - verticalpos * math.sin(omegarad)
        z = horizontalpos * math.sin(omegarad) + verticalpos * math.cos(omegarad)

        reals_dict.update({"x": x, "z": z})

        return reals_dict
