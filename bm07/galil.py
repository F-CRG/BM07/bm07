
from bliss.controllers.motors.galildmc213 import *
# from bliss.controllers.motors.galil.galildmc import *

# MODEL_DICT["DMC2112"]="MODEL_2000"


# class Galil(GalilDMC):
#     def __init__(self, *args, **kwargs):
#         GalilDMC.__init__(self, *args, **kwargs)
class Galil(GalilDMC213):
    def __init__(self, *args, **kwargs):
        GalilDMC213.__init__(self, *args, **kwargs)

    def initialize_hardware(self):
        # perform hw reset
        # YO do NOT reset controller each time you launch BLISS
        #self._galil_query("RS")
        # set default sample time
        self._galil_query("TM 1000")
        # set vector time constant (motion smoothing)
        self._galil_query("VT 1.0")
        # configure switches and latch polarity
        self._galil_query("CN %d,%d,%d" % (LOW_LEVEL, HIGH_LEVEL, HIGH_LEVEL))

    #def initialize_hardware_axis(self, axis):
    #    if self._galil_query("TSA") != "13" :
    #        super().initialize_hardware_axis(axis)


    def home_search(self, axis, switch):
        print("home_search version Yo")
        """
        start home search.
        """
        self._galil_query("OE%s=0" % axis.channel)
        self._galil_query("SH%s" % axis.channel)
        #YO start
        self._galil_query("JG 500")
        #YO end
        self._galil_query("FI%s" % axis.channel)
        self._galil_query("BG%s" % axis.channel)

