"""
Rontec MCA, acessible via serial line

yml configuration example:

.. code-block::

    name: mca
    class: rontec
    serial:
       url: "rfc2217://lid30b2:28010"       #serial line name
    calib_c: [-0.40290063, 0.0050059618, 0]  #calibration coefficients
    or
    calib_file: rontec calibration file, formated as:
                # calibration coefficients
                0 0.00410435 1.2e-9
"""
import math
import time
import numpy
import warnings
import enum
from bliss.comm.serial import Serial

from bliss.common import tango


@enum.unique
class MCA_STATE(enum.Enum):
    """ MCA States enumeration """

    UNKNOWN = 0
    RUNNING = 1
    STOP = 2


class Rontec(object):

    ERANGE = {0: "10keV", 1: "20keV", 2: "40keV", 3: "80keV"}
    MCA_DEFAULTS = {"chmin": 0, "chmax": 4095}
    MCA_ERROR = {
        0: "General error or buffer overflow",
        1: "Unknown command",
        2: "Numeric parameter expected",
        4: "Boolean parameter expected",
        5: "Additional parameter expected",
        6: "Unexpected parameter or character",
        7: "Illegal numeric value",
        8: "Unknown subcommand",
        9: "Function not implemented or no hardware support",
        13: "Hardware error",
        14: "Illegal baud rate",
    }

    def __init__(self, name, config):
        self.proxy = tango.DeviceProxy(config.get("tango"))
        self.proxy.set_timeout_millis(10000)
        calib_c = config.get("calib_c")
        calib = [float(item) for item in calib_c.split()]

        self.preset_erange = None
        self.live_t = False
        self.roi_channel = None
        self.type = None
        self.times = {}
        self.debug = False
        self.roi_dict = {}
        self.calib_c = calib
        self.reset()


    def _calib_getch(self, energy):
        if not self.calib_c[2]:
            if self.calib_c[1]:
                return int((energy - self.calib_c[0]) / self.calib_c[1])
            else:
                return 0
        cc = 1
        if self.calib_c[1] > 0:
            cc = -1
        res = int(
            (
                self.calib_c[1]
                - cc
                * math.sqrt(
                    math.pow(self.calib_c[1], 2)
                    - 4 * (self.calib_c[0] - energy) * self.calib_c[2]
                )
            )
            / (2 * self.calib_c[2])
        )
        return res

    def _calib_getE(self, chan):
        res = (
            self.calib_c[0]
            + self.calib_c[1] * chan
            + self.calib_c[2] * math.pow(chan, 2)
        )
        return res

    def set_presets(self, **kwargs):
        """Set presets parameters.

           Keyword Args:
              ctime (float): real time [s]
              erange (int): the energy range. Valid entries:
                             0: 10 keV, 1: 20 keV, 2: 40 keV, 3: 80 keV
              fname (str): file name (full path) to save the raw data
        """
        if "ctime" in kwargs:
            # we want ms or cps - this is the IC/OC counters time(gate)
            ms_time = float(kwargs["ctime"]) * 1000
            if ms_time < 0.001 or ms_time > 2000:
                gate_time = 1000  # 1s - we want ICR and OCR in cps
            else:
                gate_time = ms_time
            #asw = self.sl.write_readline(b"$CT %u\r" % gate_time)
            #self._check_answer(asw, b"set_presets: ctime")

            self.times["real_t_preset"] = kwargs["ctime"]
            self.times["cycle_t_preset"] = gate_time / 1000

        if "erange" in kwargs:
            # set the energy range
            erange = int(kwargs["erange"])
            if erange in Rontec.ERANGE.keys():
                #asw = self.sl.write_readline(b"$SE %d\r" % erange)
                #self._check_answer(asw, b"set_presets: erange")
                self.preset_erange = erange

        if "fname" in kwargs:
            self.fname = kwargs["fname"]


    def get_calibration(self):
        """Get the calibration coefficients list.

        Returns:
            calib (list): Calibration coefficients.
        """
        return self.calib_c

    def reset(self):
        """Reset the controller and sets calibration.

        Kwargs:
            calib (str): optional filename with the calibration coefficients
            or
            calib(list): optional list of calibration coefficients

        Raises:
            RuntimeError
        """
        self.proxy.mcareset()
        self.chmin = Rontec.MCA_DEFAULTS["chmin"]
        self.chmax = Rontec.MCA_DEFAULTS["chmax"]

        self.emin = self._calib_getE(self.chmin)
        self.emax = self._calib_getE(self.chmax)
        self.stop_acq()

    def clear_spectrum(self):
        """Clear the acquired spectrum"""
        self.proxy.mcaclearmemory()

    def stop_acq(self):
        """Stop the running acquisition"""
        self.proxy.mcastopacq()

    def start_acq(self, ctime=None):
        """Starts new acquisition.

        If cnt_time is not specified, counts for the preset real time.

        Keyword Args:
            cnt_time (float): count time in seconds; 0 to count indefinitely
        """
        self.proxy.mcastartacq()

    def set_roi(self, emin, emax, **kwargs):
        """Configure a ROI
        Args:
            emin (float): energy [keV] or channel number
            emax (float): energy [keV] or channel number
        Keyword Args:
            channel (int): output connector channel number (1-8)
            element (str): element name as in periodic table
            atomic_nb (int): element atomic number
        Raises:
            KeyError
        """
        if emin > 1000:
            emin /= 1000.0
        if emax > 1000:
            emax /= 1000.0

        # check if input is energy [keV] or channels
        if emax > 80:
            self.chmin = emin
            self.chmax = emax
            self.emin = self._calib_getE(self.chmin)
            self.emax = self._calib_getE(self.chmax)
        else:
            self.emin = emin
            self.emax = emax
            self.chmin = self._calib_getch(self.emin)
            self.chmax = self._calib_getch(self.emax)
        if "channel" in kwargs:
            roi_channel = int(kwargs["channel"])
            # test if channel is between 1 and 8
            if roi_channel < 1 or roi_channel > 8:
                raise KeyError("Channel number is should be between 1 and 8")
            self.roi_dict[roi_channel] = "%2.4f(%d) %2.4f(%d)" % (
                self.emin,
                self.chmin,
                self.emax,
                self.chmax,
            )
            roi_str = "%d %d %s %d %d" % (
                roi_channel,
                int(kwargs.get("atomic_nb", 34)),
                kwargs.get("element", "Se"),
                emin * 1000,
                emax * 1000,
            )
            print(roi_str)
            self.proxy.mcasetroi(roi_str)

    def set_p0(self, low, high):
        self.proxy.mcasetroi("1 1 p0 %d %d"%(low,high))

    def set_ps(self, low, high):
        self.proxy.mcasetroi("2 2 ps %d %d"%(low,high))


    def read_p0(self):
        return self.proxy.mcareadchrate(1)

    def read_ps(self):
        return self.proxy.mcareadchrate(2)


    def get_roi(self, **kwargs):
        """Get ROI settings
        Keyword Args:
            channel (int): output connector channel number (1-8)
        Returns:
            params (dict): ROI settingsdictionary.
        """
        argout = {"chmin": self.chmin, "chmax": self.chmax}
        if "channel" in kwargs:
            roi_channel = int(kwargs["channel"])
            # test if channel is between 1 and 8
            if roi_channel < 1 or roi_channel > 8:
                return argout
        else:
            roi_channel = 0

        if roi_channel:
            asw = self.proxy.mcagetroi(roi_channel)
            if self.debug:
                print(asw)
            asw = asw[4:]
            argout["ext_roi"] = asw
            _, _, self.emin, self.emax = asw.split()
            self.emin = float(self.emin) / 1000
            self.emax = float(self.emax) / 1000
            self.roi_dict[roi_channel] = "%2.4f(%d) %2.4f(%d)" % (
                self.emin,
                self.chmin,
                self.emax,
                self.chmax,
            )
        else:
            try:
                argout.pop("ext_roi")
            except KeyError:
                pass

        argout.update({"chmin": self._calib_getch(self.emin)})
        argout.update({"chmax": self._calib_getch(self.emax)})
        
        argout.update({"emin": self.emin, "emax": self.emax})
        return argout

    def read_roi_data(self, save_data=False):
        """Read ROI data
        Keyword Args:
            save_data (bool): save data in the file or not, defaults to False
        Returns:
            data (list): Raw data for the predefined ROI channels.
        """
        return self.read_raw_data(self.chmin, self.chmax, save_data)

    def read_data(self, chmin=0, chmax=4095, calib=False, save_data=False):
        """Reads the data
        Keyword Args:
            chmin (float): channel number or energy [keV], defaults to 0
            chmax (float): channel number or energy [keV], defaults to 4095
            calib (bool): use calibration, defaults to False
            save_data (bool): save data in the file or not, defaults to False
        Returns:
            data (numpy.array): x-channels or energy (if calib=True), y-data
        """
        # the input is energy and the calibration is done
        if chmax < 30 :
            chmin = self._calib_getch(chmin)
            chmax = self._calib_getch(chmax)
        y = self.read_raw_data(chmin, chmax, save_data)
        x = numpy.linspace(chmin, chmax, len(y), endpoint=False)
        if calib:
            x = self.calib_c[0] + self.calib_c[1] * x + self.calib_c[2] * x ** 2

        return numpy.array([x, y], dtype=float)

    def read_raw_data(self, chmin=0, chmax=4095, save_data=False):
        """Reads raw data
        Keyword Args:
            chmin (int): channel number, defaults to 0
            chmax (int): channel number, defaults to 4095
            save_data (bool): save data in the file or not, defaults to False
        Returns:
            data (list): Raw data for the specified roi
        """
        size = int(chmax - chmin + 1)
        #step=800
        #raw_data=[]
        #for beg in range(chmin, chmax+2, step):
        #    end=min(beg+step-1,chmax)
        #    # read only what is asked
        #    #asw = self.sl.write_readline(b"$SS %d,1,1,%d\r" % (chmin, size))
        #    #self._check_answer(asw, b"read_raw_data: handshake answer reading")
        #    # read again to get the data
        #    #raw_data = self.sl.read(size=size * 4, timeout=10)
        #    print([beg,end])
        #   temp= self.proxy.mcareaddata([beg,end])
        #   raw_data.extend(temp)
        #   print("temp=")
        #    print(temp)
        raw_data= self.proxy.mcareaddata([chmin,chmax])
        data = numpy.array(raw_data, dtype=">i4")
        print(data)
        if self.debug:
            print("read %d characters" % len(data))

        if save_data:
            with open(self.fname, "ab") as fd:
                fd.write(b"#\n#S 1  mcaacq %d\n" % self.times["real_t_preset"])
                fd.write(
                    b"#@CALIB %g %g %g\n@A"
                    % (self.calib_c[0], self.calib_c[1], self.calib_c[2])
                )
                fd.write(b" ".join(b"%d" % x for x in data) + b"\n")
                fd.close()
        return data

    def test(self):
        from bliss.common import tango
        proxy = tango.DeviceProxy("bm07/serial/06")
        proxy.devserwritestring("$SS 0,1,1\r")
        time.sleep(0.2)
        print(proxy.devserreadstring(2))
        time.sleep(5)
        a=proxy.devserreadchar(10000)
        b= proxy.devserreadchar(10000)
        c=proxy.devserreadchar(10000)
        d= proxy.devserreadchar(10000)
        data = numpy.concatenate((a, b, c, d), axis=None)
        dty = numpy.dtype("uint32")
        dty = dty.newbyteorder('>')
        data2= numpy.frombuffer(data,dtype=dty)
        with open(self.fname, "ab") as fd:
            fd.write(b"#\n#S 1  mcaacq %d\n" % self.times["real_t_preset"])
            fd.write(
                b"#@CALIB %g %g %g\n@A"
                % (self.calib_c[0], self.calib_c[1], self.calib_c[2])
            )
            fd.write(b" ".join(b"%d" % x for x in data2) + b"\n")
            fd.close()

class rontec:
    def __init__(self, name, config):
        try:
            port = config["serial"]["url"]
        except KeyError:
            port = config["SLdevice"]
            warnings.warn(
                "'SLdevice' is deprecated. Use serial instead", DeprecationWarning
            )
        calib_c = config.get("calib_c", [0, 1, 0])
        calib_file = config.get("calib_file")
        self.mca = Rontec(port, calib_file=calib_file, calib_c=calib_c)

    def read_raw_data(self, chmin=0, chmax=4095, save_data=False):
        return self.mca.read_raw_data(chmin, chmax, save_data)

    def read_roi_data(self, save_data=False):
        return self.mca.read_roi_data(save_data)

    def read_data(self, chmin=0, chmax=4095, calib=False, save_data=False):
        return self.mca.read_data(chmin, chmax, calib, save_data)

    def set_calibration(self, calib):
        return self.mca.set_calibration(calib)

    def get_calibration(self):
        return self.mca.get_calibration()

    def set_roi(self, emin, emax, **kwargs):
        self.mca.set_roi(emin, emax, **kwargs)

    def get_roi(self, **kwargs):
        return self.mca.get_roi(**kwargs)

    def clear_roi(self, **kwargs):
        self.mca.clear_roi(**kwargs)

    def get_times(self):
        return self.mca.get_times()

    def get_presets(self, **kwargs):
        return self.mca.get_presets(**kwargs)

    def set_presets(self, **kwargs):
        self.mca.set_presets(**kwargs)

    def start_acq(self, cnt_time=None):
        self.mca.start_acq(cnt_time)

    def stop_acq(self):
        self.mca.stop_acq()

    def clear_spectrum(self):
        self.mca.clear_spectrum()


if __name__ == "__main__":
    """
    det = Rontec('/dev/ttyR8')
    det = Rontec('ser2net://lid30b2:8800/dev/ttyR8')
    """
    # port number found in ser2net.conf of lid30b2
    det = Rontec("rfc2217://lid30b2:28010")

    det.clear_spectrum()
    det.set_calibration(calib_c=[-0.40290063, 0.0050059618, 0])

    det.set_roi(2, 15, channel=1)
    cd = det.get_roi(channel=1)
    print(cd)
    det.set_presets(erange=1, ctime=5, fname="/tmp/newdata.mca")
    print("erange ", det.get_presets(erange="erange"))
    print("ctime", det.get_presets(ctime="ctime"))
    det.start_acq()
    bbb = det.get_times()
    print(bbb)
    time.sleep(5)
    cc = det.get_times()
    print(cc)
    aaa = det.read_data(0, 4095)
    print(aaa)
    # bbb = det.read_raw_data(0, 100)
    bbb = det.read_raw_data(save_data=True)
    print(sum(bbb) / cc["real_t_elapsed"])